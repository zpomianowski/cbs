# -*- coding: utf-8 -*-

#########################################################################
# # This scaffolding model makes your app work on Google App Engine too
# # File is released under public domain and you can use without limitations
#########################################################################

# # if SSL/HTTPS is properly configured and you want all HTTP requests to
# # be redirected to HTTPS, uncomment the line below:
# request.requires_https()
from gluon.contrib.appconfig import AppConfig
myconf = AppConfig(reload=True)
tag = myconf.take('main.config')
if not request.env.web2py_runtime_gae:
    #if (request.env.http_host == 'saut.test.misc.polsatc') or (request.env.http_host == 'saut-test.polsatc'):
    #    db = DAL(S_SAUTIS_DATABASEURL,migrate=True, fake_migrate=False) #, migrate=False, fake_migrate=False
    #else:
        # # if NOT running on Google App Engine use SQLite or other DB
    db = DAL(myconf.take(tag + '.database_uri'), migrate=False, fake_migrate=False)
else:
    # # connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    # # store sessions and tickets there
    session.connect(request, response, db=db)
    # # or store session in Memcache, Redis, etc.
    # # from gluon.contrib.memdb import MEMDB
    # # from google.appengine.api.memcache import Client
    # # session.connect(request, response, db = MEMDB(Client()))

response.delimiters = ('{!', '!}')
# # by default give a view/generic.extension to all actions from localhost
# # none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
# # (optional) optimize handling of static files # response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
# # (optional) static assets folder versioning # response.static_version = '0.0.0'
#########################################################################
# # Here is sample code if you need for
# # - email capabilities
# # - authentication (registration, login, logout, ... )
# # - authorization (role based authorization)
# # - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# # - old style crud actions # # (more options discussed in gluon/tools.py)
#########################################################################
import datetime
from gluon.tools import Auth, Crud, Service, PluginManager, prettydate, Mail
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

myconf = AppConfig(reload=True)

auth.settings.expiration = 3600*24
auth.settings.long_expiration = 3600*24*30  # 30 dni
auth.settings.remember_me_form = True

auth.settings.table_user_name = 'cbs_auth_user'
auth.settings.table_group_name = 'cbs_auth_group'
auth.settings.table_membership_name = 'cbs_auth_membership'
auth.settings.table_permission_name = 'cbs_auth_permission'
auth.settings.table_event_name = 'cbs_auth_event'
auth.settings.table_cas_name = 'cbs_auth_cas'
auth.define_signature()

auth.settings.extra_fields[auth.settings.table_user_name] = [
    Field('visible', 'boolean', default=True)
]

# # create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False,migrate=False, fake_migrate=True)

# # configure email
mail = auth.settings.mailer
mail.settings.server = myconf.take(tag + '.email_server')
mail.settings.sender = myconf.take(tag + '.email_sender')
mail.settings.login = myconf.take(tag + '.email_login')


auth.settings.actions_disabled.append('register')

# # configure auth policy
# auth.settings.everybody_group_id = 5 wszyscy nowi użytkownicy zostają przypisani do grupy 5
auth.settings.create_user_groups = False
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True


auth.messages.label_remember_me = "Remember password"


#auth.settings.login_url = URL('index', args='login')

#########################################################################
# # Login menu
#########################################################################

auth.navbar(mode='bare')
response.logged_user = auth.bar['user']


#auth.settings.login_url = URL('user', args='login')
#auth.settings.logged_url = URL('index', args='dashboard')
#auth.settings.login_next = URL('index' , args='dashboard')


# # if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
# # register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

def atms_validation(form=None):
    user = db(db.atms_auth_user.email == form.vars.email).select().first()
    # atms email and password validation
    if user is None or user.password != form.vars.password:
        form.vars.password = 'blad'
        return False
    else:
        # get atms permissions of user
        permissions = []
        for row in db(db.atms_auth_membership.user_id == user.id).select(db.atms_auth_membership.group_id):
            permissions.append(row.group_id)
        # if user is a tester - 7 or manager - 8 add user to cbs_auth user and so on
        if 7 in permissions or 8 in permissions:
            db.cbs_auth_user.update_or_insert(db.cbs_auth_user.email == user.email,
            email = user.email,
            first_name = user.first_name,
            last_name = user.last_name,
            password = user.password,
            visible = user.is_active)

            cbsUser = db(db.cbs_auth_user.email == form.vars.email).select().first()

            # if user has no permission group we give him developer permissions
            if not db(db.cbs_auth_membership.user_id == cbsUser.id).select().first():
                db.cbs_auth_membership.insert(user_id = cbsUser.id, group_id = 3)

            db.cbs_client.update_or_insert(db.cbs_client.email == user.email,
            user_id = cbsUser.id,
            firstname = user.first_name,
            lastname = user.last_name,
            company_id = 1,
            email = user.email,
            visible = user.is_active,
            is_active = user.is_active)
        else:
            form.vars.password = 'blad'
            return False

auth.settings.login_onvalidation = atms_validation

#########################################################################
# # Define your tables below (or better in another model file) for example
# # # # >>> db.define_table('mytable',Field('myfield','string'))
##
# # Fields can be 'string','text','password','integer','double','boolean'
# #       'date','time','datetime','blob','upload', 'reference TABLENAME'
# # There is an implicit 'id integer autoincrement' field
# # Consult manual for more options, validators, etc.
##
# # More API examples for controllers:
##
# # >>> db.mytable.insert(myfield='value')
# # >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
# # >>> for row in rows: print row.id, row.myfield
#########################################################################
db.define_table(
    'atms_auth_user',
    Field('first_name', 'string'),
    Field('last_name', 'string'),
    Field('email', 'string'),
    Field('password', 'password'),
    Field('is_active', 'boolean'),
    format='(%(id)s)', migrate=False
    )

db.define_table(
    'atms_auth_membership',
    Field('user_id', 'integer'),
    Field('group_id', 'integer'),
    format='(%(id)s)', migrate=False
    )

db._common_fields.append(auth.signature)


db.define_table(    # lista firm
    'cbs_company',
    Field('name', 'string', length=255),
    Field('description', 'text'),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(name)s'
    )

db.define_table(    # typy urządzeń
    'cbs_device_type',
    Field('name', 'string', length=255),
    Field('description', 'text'),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(name)s',migrate=False, fake_migrate=True
    )

db.define_table(    # modele urządzeń
    'cbs_device_model',
    Field('name', 'string', length=255),
    Field('description', 'text'),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(name)s'
    )
db.cbs_device_model._enable_record_versioning()

db.define_table(    # stan urządenia i kart SIM
    'cbs_device_state',
    Field('name', 'string', length=255),
    Field('description', 'text'),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(name)s'
    )


db.define_table(
    'cbs_client',
    Field('user_id',
          requires=IS_EMPTY_OR(IS_IN_DB(
                               db, db.cbs_auth_user.id,
                               '(%(id)s) %(first_name)s %(last_name)s')),
          default=None,
          ondelete='NO ACTION'),    # id user or None
    Field('firstname', 'string', length=255),
    Field('lastname', 'string',  length=255),
    Field('company_id', 'reference cbs_company',
          ondelete='NO ACTION'),
    Field('email', 'string', length=255),
    Field('phone', 'string', length=255),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(firstname)s %(lastname)s'
    )
db.cbs_client._enable_record_versioning()


if db((db.cbs_client.firstname == 'LAB') &
      (db.cbs_client.lastname == 'LAB')).select():
    response.LAB = db((db.cbs_client.firstname == 'LAB') &
                      (db.cbs_client.lastname == 'LAB')).select().first().id
else:
    response.LAB = 0

db.define_table(    # wszystkei urządzenia oprócz kart SIM
    'cbs_device',
    Field('model_id', 'reference cbs_device_model',
          ondelete='NO ACTION'),
    Field('company_id', 'reference cbs_company',
          ondelete='NO ACTION'),
    Field('type_id', 'reference cbs_device_type',
          ondelete='NO ACTION'),
    Field('sn', default=None),
    Field('imei', default=None),
    Field('mac', default=None),
    Field('holder_id', 'reference cbs_client',
          default=response.LAB,
          ondelete='NO ACTION'),
    Field('owner_id', 'reference cbs_client',
          default=response.LAB,
          ondelete='NO ACTION'),
    Field('state_id', 'reference cbs_device_state',
          ondelete='NO ACTION'),
    Field('description', 'text', default=None),
    Field('visible', 'boolean', default=True),
    Field('cbs_id', default=None)#,
    #format='(%(id)s)', migrate=False, fake_migrate=False
    )
db.cbs_device._enable_record_versioning()


db.define_table(
    'cbs_sim',
    Field('company_id', 'reference cbs_company',
          ondelete='NO ACTION'),
    Field('pin', 'integer'),
    Field('pin2', 'integer', default=None),
    Field('puk', 'integer'),
    Field('puk2', 'integer', default=None),
    Field('msisdn'),
    Field('imsi'),
    Field('iccid'),
    Field('holder_id', 'reference cbs_client',
          default=response.LAB,
          ondelete='NO ACTION'),
    Field('owner_id', 'reference cbs_client',
          default=response.LAB,
          ondelete='NO ACTION'),
    Field('state_id', 'reference cbs_device_state',
          ondelete='NO ACTION'),
    Field('description', 'text', default=None),
    Field('visible', 'boolean', default=True),
    Field('cbs_id', default=None),
    format='(%(id)s) %(msisdn)s %(iccid)s'#,
    #migrate=False, fake_migrate=True
    )
db.cbs_sim._enable_record_versioning()


db.define_table(
    'cbs_transfer',
    Field('sim_id', 'reference cbs_sim', ondelete='NO ACTION'),
    Field('year_id', 'integer'),
    Field('month_id', 'integer'),
    Field('total'),
    Field('uplink'),
    Field('downlink'),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(sim_id.msisdn)s %(sim_id.iccid)s'
    )
db.cbs_sim._enable_record_versioning()


# ############# TRANSACTION

db.define_table(    # operacja przeprowadzana w transakcji
    'cbs_operation',
    Field('name', 'string', length=255),
    Field('description', 'text'),
    Field('visible', 'boolean', default=True),
    format='(%(id)s) %(name)s'
    )

db.define_table(
    'cbs_attachments',
    Field('title', required=True),
    Field('filetype'),
    Field('filesize', 'integer'),
    Field('annex', 'upload', autodelete=True)
    )

db.define_table(
    'cbs_transaction',
    Field('from_id', 'reference cbs_client', ondelete='NO ACTION'),  # odkogo
    Field('to_id', 'reference cbs_client', ondelete='NO ACTION'),  # do kogo
    Field('operation_id', 'reference cbs_operation', ondelete='NO ACTION'),
    Field('beginning', 'date'),  # data wypożyczenia
    Field('ending', 'date'),    # data zwrotu
    Field('is_open', 'boolean', default=True),  # czy transakcje jest otwarta
    Field('report', 'reference cbs_attachments', ondelete='SET NULL'),
    Field('description', 'text'),
    Field('visible', 'boolean', default=True),
    Field('notification_email', 'boolean', default=False),  # czy wysyłane są maile z powiadomieniami
    Field('changing_email', 'boolean', default=False),  # maile przy zmianie statusu transakcji
    format='%(id)s'
    )
db.cbs_transaction._enable_record_versioning()

db.define_table(
    'cbs_subtransaction_device',
    Field('transaction_id', 'reference cbs_transaction', ondelete='NO ACTION'),
    Field('device_id', 'reference cbs_device', ondelete='NO ACTION'),
    Field('returned', 'boolean', default=False),
    Field('return_date', 'date')
    )

db.define_table(
    'cbs_subtransaction_sim',
    Field('transaction_id', 'reference cbs_transaction', ondelete='NO ACTION'),
    Field('sim_id', 'reference cbs_sim', ondelete='NO ACTION'),
    Field('returned', 'boolean', default=False),
    Field('return_date', 'date')
    )


db.define_table(
    'cbs_comment',
    Field('transaction_id', 'reference cbs_transaction', ondelete='NO ACTION'),
    Field('author_id', 'reference cbs_client', ondelete='NO ACTION'),
    Field('posted', 'datetime'),
    Field('description', 'text')
    )


db.define_table(
    'cbs_datasheet',
    Field('cat_3g_uplink', 'string', length=255, default=None),  # kategoria
    Field('cat_3g_downlink', 'string', length=255, default=None),
    Field('cat_lte', 'string', length=255, default=None),

    Field('chipset_prvider_id', default=None,
          requires=IS_EMPTY_OR(IS_IN_DB(
                               db, db.cbs_company.id,
                               '(%(id)s) %(name)s')),
          ondelete='NO ACTION'),
    Field('chipset_model', 'string', length=255, default=None),
    Field('chipset_baseline', 'string', length=255, default=None),

    Field('tgpp', 'string', length=255, default=None),  # 3GPP

    Field('ue_usage_setting', 'string', length=255, default=None),
    Field('voice_domain', 'string', length=255, default=None),

    Field('system_gsm', 'boolean', default=False),
    Field('system_gprs', 'boolean', default=False),
    Field('system_edge', 'boolean', default=False),
    Field('system_umts', 'boolean', default=False),
    Field('system_hspa', 'boolean', default=False),
    Field('system_hspa_p', 'boolean', default=False),
    Field('system_lte', 'boolean', default=False),
    Field('system_lte_a', 'boolean', default=False),
    Field('system_wifi', 'boolean', default=False),

    Field('band_lte', 'string', length=255),    # B1_B2_B3...
    Field('band_3g', 'string', length=255),     # B1_B2_B3...
    Field('band_2g', 'string', length=255),     # B1_B2_B3...
    Field('band_wifi', 'string', length=255),

    Field('standard_wifi', 'string', length=255),

    Field('csfb', 'boolean', default=False),
    Field('sms_sgs', 'boolean', default=False),
    Field('sms_ims', 'boolean', default=False),
    Field('volte', 'boolean', default=False),

    Field('drx', 'string', length=255, default=None),
    Field('dual_carrier', 'boolean', default=False),

    Field('mimo', 'string', length=255, default=None),
    Field('duplex', 'string', length=255, default=None),

    Field('rohc', 'string', length=255, default=None),

    Field('intra_band_c_CA', 'string', length=255),
    Field('intra_band_nc_CA', 'string', length=255),
    Field('inter_band_CA', 'string', length=255),

    Field('lte_3g_handover', 'boolean', default=False),
    Field('lte_2g_handover', 'boolean', default=False),
    Field('lte_2g_nacc', 'boolean', default=False),
    Field('tg_lte_handover', 'boolean', default=False),
    Field('dg_lte_handover', 'boolean', default=False),
    Field('inter_frequency_handover', 'boolean', default=False),
    Field('fdd_tdd_handover', 'boolean', default=False)
    )


db.define_table(
    'cbs_specification',
    Field('model_id', 'reference cbs_device_model', ondelete='NO ACTION'),
    Field('company_id', 'reference cbs_company', ondelete='NO ACTION'),
    Field('operating_system', 'string', length=255),
    Field('system_version', 'string', length=255),
    Field('system_version_number', 'string', length=255),

    Field('firmware', 'string', length=255),
    Field('hardware', 'string', length=255),

    Field('datasheet_id',
          requires=IS_EMPTY_OR(IS_IN_DB(
                               db, db.cbs_datasheet.id,
                               '%(id)s')),
          default=None, ondelete='NO ACTION'),
    Field('ipla_id', default=None, ondelete='NO ACTION'),
    Field('adddate', "date", default=datetime.date.today()),
    Field('description', 'text'),
    Field('annex', 'reference cbs_attachments', ondelete='SET NULL'),
    Field('visible', 'boolean', default=True),
    )

db.define_table(
    'cbs_throughput',
    Field('specification_id', 'reference cbs_specification',
          ondelete='NO ACTION'),
    Field('category', 'string', length=255),
    Field('uplink', 'integer'),
    Field('downlink', 'integer'),
    Field('band', 'string', length=255),
    Field('frequency', 'string', length=255),
    Field('adddate', "date", default=datetime.date.today()),
    )

# # after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
