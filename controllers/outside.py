# -*- coding: utf-8 -*-

import os
import json
import gluon.contrib.simplejson

def index():
	return dict(message=T('Hello World'))

def resources():
	return dict()

def specifications():
	return dict()


# POBIERA LISTĘ KLIENTÓW
def getClients():
    data = []
    for i in db(db.cbs_client).select():
        data.append({"id": i.id, "firstname": i.firstname, "lastname": i.lastname,
                     "email": i.email, "phone": i.phone, "visible": i.visible,
                     "user_id": i.user_id})
    return json.dumps(data)

# POBIERA LISTĘ FIRM
def getCompany():
    data = []
    for i in db(db.cbs_company).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                     "visible": i.visible})
    return json.dumps(data)

# POBIERA LISTĘ TYPÓW URZĄDZEŃ
def getDevicesType():
    data = []
    for i in db(db.cbs_device_type.id.belongs([2,3])).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                    "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ MODELÓW URZĄDZEŃ
def getDevicesModel():
    data = []
    for i in db(db.cbs_device_model).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                    "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ MODELÓW URZĄDZEŃ
def getDevicesState():
    data = []
    for i in db(db.cbs_device_state).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                    "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ URZĄDZEŃ
def getDevices():
    '''
        types - tablica id dostępnych typów urządzeń
    '''
    data = []
    for i in db(db.cbs_device.type_id.belongs([2,3])).select():
        data.append({"id": i.id, "model_id": i.model_id,
                     "company_id": i.company_id, "imei": i.imei,
                     "type_id": i.type_id, "sn": i.sn, "sn": i.sn,
                     "mac": i.mac, "state_id": i.state_id,
                     "holder_id": i.holder_id, "owner_id": i.owner_id,
                     "description": i.description, "visible": i.visible,
                     "cbs_id": i.cbs_id,
                     "created_on": str(i.created_on),
                     "created_by": i.created_by,
                     "modified_on": str(i.modified_on),
                     "modified_by": i.modified_by})
    return json.dumps(data)

 

def getSubTransactionDevice():
    data = []
    for i in db(db.cbs_subtransaction_device.returned == False).select():
        data.append({"id": i.id,"device_id": i.device_id, "returned": i.returned})            
    return json.dumps(data)