# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
# # This is a sample controller
# # - index is the default action of any application
# # - user is required for authentication and authorization
# # - download is for downloading files uploaded in the db (does streaming)
# # - call exposes all registered services (none by default)
#########################################################################
import os
import json
import gluon.contrib.simplejson


@auth.requires_login()
def index():

    if request.function not in request.url:
        redirect('/cbs/default/index')
    return dict(message=T('Hello World'))

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """

    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """

    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())


@auth.requires_login()
def raport():
    return dict()

#  --- View ---
@auth.requires_login()
def dashboard():
    return dict()


def newTransaction():
    return dict()


def browseTransaction():
    return dict()


def newResources():
    return dict()


def browseResources():
    return dict()


def users():
    return dict()


def clients():
    return dict()


def transferSim():
    return dict()


def newSpecification():
    return dict()


def browseSpecification():
    return dict()

#  def editRow():
#  return dict()

#  ---END View ---


# ZWRACA DANE AKTUALNIE ZALOGOWANEGO UŻYTKOWNIKA
def getUserInfo():
    data = False
    if auth.user:
        client = db(db.cbs_client.user_id == auth.user.id).select().first()
        role = db((db.cbs_auth_group.id == db.cbs_auth_membership.group_id) &
                  (db.cbs_auth_membership.user_id == auth.user.id)
                  ).select().first()
        lab = db((db.cbs_client.firstname == 'LAB') & (db.cbs_client.lastname == 'LAB')).select().first()
        if lab:
            lab = lab.id
        data = {
            'id': auth.user.id,
            'firstname': auth.user.first_name,
            'email': auth.user.email,
            'lastname':  auth.user.last_name,
            #'username': auth.user.username,
            'client_id': client.id,
            'phone': client.phone,
            'company_id': client.company_id,
            'privileges': {'name':role.cbs_auth_group.role, 'id':role.cbs_auth_group.id},
            'LAB': lab
        }
    return json.dumps(data)


# POBIERA LISTĘ UŻYTKOWNIKÓW
@auth.requires_login()
def getUsers():
    data = []
    for i in db(db.cbs_auth_user).select():
        client = db(db.cbs_client.user_id == i.id).select().first()
        privileges = role = db((db.cbs_auth_group.id == db.cbs_auth_membership.group_id) & (
            db.cbs_auth_membership.user_id == i.id)).select().first()
        if not privileges:
            continue
        data.append({"id": i.id,
                     "firstname": i.first_name,
                     "lastname": i.last_name,
                     #"username": i.username,
                     "email": i.email,
                     "visible": i.visible,
                     "client_id": client.id,
                     'permission': {"id": privileges.cbs_auth_group.id,
                                    "role": privileges.cbs_auth_group.role},
                    'member_id': role.cbs_auth_membership.id})
    return json.dumps(data)



# POBIERA LISTĘ KLIENTÓW
@auth.requires_login()
def getClients():
    data = []
    for i in db(db.cbs_client).select():
        company = db(db.cbs_company.id == i.company_id).select().first()
        data.append({"id": i.id,
                     "user_id": i.user_id,
                     "firstname": i.firstname,
                     "lastname": i.lastname,
                     "company": {"id": company.id,
                                 "name": company.name,
                                 "description": company.description,
                                 "visible": company.visible},
                     "email": i.email,
                     "phone": i.phone,
                     "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ FIRM
@auth.requires_login()
def getCompany():
    data = []
    for i in db(db.cbs_company).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                     "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ MOŻLIWYCH OPERACJI
@auth.requires_login()
def getOperations():
    data = []
    for i in db(db.cbs_operation).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                     "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ URZĄDZEŃ
@auth.requires_login()
def getDevices():
    data = []
    for i in db(db.cbs_device).select():
        data.append({"id": i.id, "model_id": i.model_id,
                     "company_id": i.company_id, "imei": i.imei,
                     "type_id": i.type_id, "sn": i.sn, "sn": i.sn,
                     "mac": i.mac, "state_id": i.state_id,
                     "holder_id": i.holder_id, "owner_id": i.owner_id,
                     "description": i.description, "visible": i.visible,
                     "cbs_id": i.cbs_id,
                     "created_on": str(i.created_on),
                     "created_by": i.created_by,
                     "modified_on": str(i.modified_on),
                     "modified_by": i.modified_by})
    return json.dumps(data)


# POBIERA HISTORIE URZĄDZEŃ
@auth.requires_login()
def getDeviceArchive():
    data = []
    for i in db(db.cbs_device_archive.current_record == request.vars.device_id).select():
        data.append({"id": i.id, "current_record": i.current_record,
                     "model_id": i.model_id, "company_id": i.company_id,
                     "type_id": i.type_id, "sn": i.sn, "imei": i.imei,
                     "sn": i.sn, "mac": i.mac,
                     "holder_id": i.holder_id, "owner_id": i.owner_id,
                     "state_id": i.state_id,
                     "description": i.description,
                     "modified_on": str(i.modified_on),
                     "modified_by": i.modified_by
                     })
    return json.dumps(data)


# POBIERA LISTĘ TYPÓW URZĄDZEŃ
@auth.requires_login()
def getDevicesType():
    data = []
    for i in db(db.cbs_device_type).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                    "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ MODELÓW URZĄDZEŃ
@auth.requires_login()
def getDevicesModel():
    data = []
    for i in db(db.cbs_device_model).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                    "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ MODELÓW URZĄDZEŃ
@auth.requires_login()
def getDevicesState():
    data = []
    for i in db(db.cbs_device_state).select():
        data.append({"id": i.id, "name": i.name, "description": i.description,
                    "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ KART SIM
@auth.requires_login()
def getSim():
    data = []
    for i in db(db.cbs_sim).select():
        data.append({"id": i.id, "company_id": i.company_id, "pin": i.pin,
                     "pin2": i.pin2, "puk": i.puk, "puk2": i.puk2,
                     "msisdn": i.msisdn, "imsi": i.imsi, "iccid": i.iccid,
                     "holder_id": i.holder_id, "owner_id": i.owner_id,
                     "state_id": i.state_id, "description": i.description,
                     "visible": i.visible,
                     "created_on": str(i.created_on),
                     "created_by": i.created_by,
                     "modified_on": str(i.modified_on),
                     "modified_by": i.modified_by})

    return json.dumps(data)


# POBIERA HISTORIE URZĄDZEŃ
@auth.requires_login()
def getSimArchive():
    data = []
    for i in db(db.cbs_sim_archive.current_record == request.vars.sim_id).select():
        data.append({"id": i.id, "current_record": i.current_record,
                     "company_id": i.company_id, "pin": i.pin,
                     "pin2": i.pin2, "puk": i.puk, "puk2": i.puk2,
                     "msisdn": i.msisdn, "imsi": i.imsi, "iccid": i.iccid,
                     "holder_id": i.holder_id, "owner_id": i.owner_id,
                     "state_id": i.state_id, "description": i.description,

                     "modified_on": str(i.modified_on),
                     "modified_by": i.modified_by
                     })
    return json.dumps(data)


# POBIERA LISTĘ TRANSAKCJI ###############
@auth.requires_login()
def getTransactions():
    data = []
    query = db.cbs_transaction
    for i in db(query).select():
        #operation = db(db.cbs_operation.id == i.operation_id).select().first()
        #fro = db(db.cbs_client.id == i.from_id).select().first()
        #to = db(db.cbs_client.id == i.to_id).select().first()

        data.append({"id": i.id, "from_id": i.from_id, "to_id": i.to_id,
                     #"from": str(fro.firstname + " " + fro.lastname),
                     #"to": str(to.firstname + " " + to.lastname),"operation": operation.name,
                     "operation_id":  i.operation_id,
                     "beginning": str(i.beginning),
                     'notification_email': i.notification_email,
                     "ending": str(i.ending), "is_open": i.is_open,
                     "report": i.report, "description": i.description,
                     "visible": i.visible})
    return json.dumps(data)


# POBIERA LISTĘ POD TRANSAKCJI URZĄDZEŃ
@auth.requires_login()
def getSubTransactionDevice():
    data = []
    for i in db(db.cbs_subtransaction_device).select():
        data.append({"id": i.id, "transaction_id": i.transaction_id,
                     "device_id": i.device_id, "returned": i.returned,
                     "return_date": str(i.return_date)})
    return json.dumps(data)


# POBIERA LISTĘ POD TRANSAKCJI URZĄDZEŃ
@auth.requires_login()
def getSubTransactionSim():
    data = []
    for i in db(db.cbs_subtransaction_sim).select():
        data.append({"id": i.id, "transaction_id": i.transaction_id,
                     "sim_id": i.sim_id, "returned": i.returned,
                     "return_date": str(i.return_date)})
    return json.dumps(data)


# POBIERA KOMENTARZE DO TRANSAKCJI
@auth.requires_login()
def getComments():
    data = []
    for i in db(db.cbs_comment).select():
        data.append({"id": i.id, "transaction_id": i.transaction_id,
                     "author_id": i.author_id, "posted": str(i.posted),
                     "description": i.description})
    return json.dumps(data)


# POBIERA HISTORIE TRANSAKCJI
@auth.requires_login()
def getTransactionArchive():
    data = []
    for i in db(db.cbs_transaction_archive.current_record == request.vars.transaction_id).select():
        data.append({"id": i.id, "current_record": i.current_record,
                     "ending": str(i.ending), "is_open": i.is_open,
                     "report": i.report, 'description': i.description,
                     "modified_on": str(i.modified_on),
                     "modified_by": i.modified_by,
                     "notification_email": i.notification_email
                     })
    return json.dumps(data)


# POBIERA TRANSFERY KART SIM
@auth.requires_login()
def getSimTransfer():
    data = []
    for i in db(db.cbs_transfer).select():
        data.append({"id": i.id, "sim_id": i.sim_id, "year": i.year_id,
                     "month": i.month_id, "total": float(i.total), "uplink": float(i.uplink),
                     "downlink": float(i.downlink), "visible": i.visible})
    return json.dumps(data)


@auth.requires_login()
def getSpecification():
    '''
     Pobiera specyfikacje
    '''
    data = []
    for i in db(db.cbs_specification).select():
        data.append({"id": i.id, "model_id": i.model_id,
                     "company_id": i.company_id,
                     "operating_system": i.operating_system,
                     "system_version": i.system_version,
                     "system_version_number": i.system_version_number,
                     "firmware": i.firmware,
                     "hardware": i.hardware,
                     "datasheet_id": i.datasheet_id,
                     "ipla_id": i.ipla_id,
                     "adddate": str(i.adddate),
                     "description": i.description,
                     "visible": i.visible})
    return json.dumps(data)


@auth.requires_login()
def getDatsheet():
    '''
     Pobiera datasheet
    '''
    data = []
    if len(request.args) > 0:
        query = db.cbs_datasheet.id == request.args[0]
        get_all = False
    else:
        query = db.cbs_datasheet.id > 0
        get_all = True

    for i in db(query).select():
        temp = {"id": i.id, "cat_3g_uplink": i.cat_3g_uplink,
                "cat_3g_downlink": i.cat_3g_downlink, "cat_lte": i.cat_lte,
                "chipset_prvider_id": i.chipset_prvider_id,
                "chipset_model": i.chipset_model,
                "chipset_baseline": i.chipset_baseline, "tgpp": i.tgpp,
                "ue_usage_setting": i.ue_usage_setting,
                "voice_domain": i.voice_domain, "system_gsm": i.system_gsm,
                "system_gprs": i.system_gprs, "system_edge": i.system_edge,
                "system_umts": i.system_umts, "system_hspa": i.system_hspa,
                "system_hspa_p": i.system_hspa_p,
                "system_lte": i.system_lte, "system_lte_a": i.system_lte_a,
                "system_wifi": i.system_wifi, "band_lte": i.band_lte,
                "band_3g": i.band_3g, "band_2g": i.band_2g,
                "band_wifi": i.band_wifi, "standard_wifi": i.standard_wifi,
                "csfb": i.csfb, "sms_sgs": i.sms_sgs, "sms_ims": i.sms_ims,
                "volte": i.volte, "drx": i.drx, "dual_carrier": i.dual_carrier,
                "mimo": i.mimo, "duplex": i.duplex, "rohc": i.rohc,
                "intra_band_c_CA": i.intra_band_c_CA,
                "intra_band_nc_CA": i.intra_band_nc_CA,
                "inter_band_CA": i.inter_band_CA,
                "lte_3g_handover": i.lte_3g_handover,
                "lte_2g_handover": i.lte_2g_handover,
                "lte_2g_nacc": i.lte_2g_nacc,
                "tg_lte_handover": i.tg_lte_handover,
                "dg_lte_handover": i.dg_lte_handover,
                "inter_frequency_handover": i.inter_frequency_handover,
                "fdd_tdd_handover": i.fdd_tdd_handover
                }
        if get_all:
            data.append(temp)
        else:
            data = temp
    return json.dumps(data)


# EDYTUJ ISTNIEJĄCY REKORD W BAZIE
# id    - numer rekordu do modyfikacji
# table - tabela w której modyfikujemy
# data  - nowe dane (objekt json o taki jak pola w db)
def editRecord():
    data = gluon.contrib.simplejson.loads(request.body.read())
    resp = db(db[data['table']].id == int(data['id'])).update(**data['data'])
    return resp


# DODAJ NOWY REKORD
# table - tabela w której dodajemy
# data  - nowe dane (objekt json o taki jak pola w db, bez id)
# resp  - id nowego rekordu
def addRecord():
    data = gluon.contrib.simplejson.loads(request.body.read())
    if data['table'] == 'cbs_device':
        prev = db(db.cbs_device).select().last()
        try:
            prev_cbs_id = int(prev.cbs_id) + 1
        except:
            prev_cbs_id = str(prev.cbs_id)+"_1"

        data['data']['cbs_id'] = prev_cbs_id
        resp = db[data['table']].insert(**data['data'])
        return str(resp)+"-"+str(prev_cbs_id)
    else:
        resp = db[data['table']].validate_and_insert(**data['data'])
        return int(resp["id"])


#  UKRYJ REKORD
#  table - tabela w której zmieniamy visibility
#  id - id rekordu ktry jest zmieniany
def hideRecord():
    resp = db(db[request.vars.table].id == request.vars.id).update(visible=False)
    return resp


#  USUŃ REKORD
#  table - tabela w której zmieniamy visibility
#  id - id rekordu ktry jest zmieniany
def delRecord():
    resp = db(db[request.vars.table].id == request.vars.id).delete()
    return resp


def addFile():
    value = request.vars.testfile
    new_name = db.cbs_attachments.annex.store(value.file, filename=str(value.filename))
    id = db.cbs_attachments.insert(annex=new_name, title=request.vars.testfile.filename, filesize=int(request.vars.filesize), filetype=request.vars.filetype)
    return id


def getFile():
    # data = gluon.contrib.simplejson.loads(request.body.read())
    data = []
    rows = db(db.cbs_attachments.id == request.vars.id).select()
    for i in rows:
        data.append({"id": i.id, "title": i.title,
                     "size": i.filesize, "type": i.filetype,
                     'link': i.annex
                     })
    return json.dumps(data)


def sendEmail():
    import copy
    """
      Wysyłanie maili
    """

    to_mail = []
    if '@' in str(request.vars.toemail):
        to_mail.append(str(request.vars.toemail))

    cc = []
    if request.vars.fromemail:
        if '@' in str(request.vars.fromemail):
            cc.append(str(request.vars.fromemail))

    adm = db((db.cbs_auth_user.id == db.cbs_auth_membership.user_id) & (db.cbs_auth_membership.group_id == 1)).select()

    for i in adm:
        cc.append(i.cbs_auth_user.email)

    if request.vars.msgtype:
        if str(request.vars.msgtype) == "newresource":
            to_mail = copy.copy(cc)

    cc = list(set(cc) - set(to_mail))

    logo = os.path.join(request.folder, 'static', "images", "logo_cbs.png")

    attachments = []
    attachments.append(mail.Attachment(logo, filename='logo.png',
                                       content_id='logo'))

    devindex = []
    devices = False
    if request.vars.dev:
        for k in str(request.vars.dev).split('_'):
            devindex.append(int(k))
        devices = db(db.cbs_device.id.belongs(devindex)).select()

    simindex = []
    sim = False
    if request.vars.sim:
        for k in str(request.vars.sim).split('_'):
            simindex.append(int(k))
        sim = db(db.cbs_sim.id.belongs(simindex)).select()

    if request.vars.expire:
        expire = bool(str(request.vars.expire))
    else:
        expire = False

    if request.vars.ending:
        ending = str(request.vars.ending)
    else:
        ending = ""
    context = dict(msgtype=str(request.vars.msgtype), devices=devices,
                   sim=sim, expire=expire, ending=ending)

    if request.vars.fromwho:
        context.update({"fromwho": request.vars.fromwho})
        context.update({"fromemail": request.vars.fromemail})
        context.update({"fromphone": request.vars.fromphone})
        context.update({"fromcompany": request.vars.fromcompany})

        context.update({"towho": request.vars.towho})
        context.update({"toemail": request.vars.toemail})
        context.update({"tocompany": request.vars.tocompany})
        context.update({"tophone": request.vars.tophone})
        context.update({"trid": request.vars.trid})
        pass
    if request.vars.beginning:
        context.update({"beginning": str(request.vars.beginning)})

    if request.vars.operation:
        context.update({"operation": str(request.vars.operation)})

    message = response.render('mail_message.html', context)

    return mail.send(to=to_mail, subject='SAUT_CBS', message=message,
                     attachments=attachments, cc=cc)


def sendChangedEmail():

    logo = os.path.join(request.folder, 'static', "images", "logo_cbs.png")

    attachments = []
    attachments.append(mail.Attachment(logo, filename='logo.png',
                                       content_id='logo'))

    adm = db((db.cbs_auth_user.id == db.cbs_auth_membership.user_id) & (db.cbs_auth_membership.group_id == 1)).select()

    to_mail = []
    for i in adm:
        to_mail.append(i.cbs_auth_user.email)

    device = False
    olddev = False
    if request.vars.dev:
        device = db(db.cbs_device.id == int(request.vars.dev)).select().first()
        olddev = db(db.cbs_device_archive.current_record == int(request.vars.dev)).select().last()

    sim = False
    oldsim = False
    if request.vars.sim:
        sim = db(db.cbs_sim.id == int(request.vars.sim)).select().first()
        oldsim = db(db.cbs_sim_archive.current_record == int(request.vars.sim)).select().last()

    context = dict(device=device, olddev=olddev, sim=sim, oldsim=oldsim)

    message = response.render('mail_message_change.html', context)

    return mail.send(to=to_mail, subject='SAUT_CBS', message=message,
                     attachments=attachments)
