
var app = angular.module('CBS', ["ui.router",'ngResource', 'ngCookies','ngTable','dialogs','ui.bootstrap','ngAnimate', 'summernote','ngSanitize']);


/********** PROVIDERS, FACTORIES, SERVICES and DIRECTIVE ***********/
/** MENU **/
app.factory("side_menu", function( USER_ROLES) {
    return {
        a_home:{
            title: 'Strona startowa', icon: 'fa fa-lg fa-fw fa-home', url: '#/', active :true,
            privileges: [USER_ROLES.all], id:"aa",
            span: { content: "-", classs: 'badge pull-right inbox-badge' },
        },

        b_transaction:{
            title: 'Transakcja', icon: 'fa fa-lg fa-fw fa-calculator',
            privileges: [USER_ROLES.all], id:"bb",
            children:{
                a_new : { title: "Nowa", url: '#/transakcja/nowa',  privileges: [USER_ROLES.admin, USER_ROLES.manager, USER_ROLES.developer], active :false },
                b_browse: { title: "Przeglądaj", url: '#/transakcja/przegladaj',  privileges: [USER_ROLES.all], active :false}
            }
        },

        c_equipment: {
            title: 'Zasoby szprzętowe', icon: 'fa fa-lg fa-fw fa-cubes',
            privileges: [USER_ROLES.all], id:"cc",
            children:{
                a_new : { title: "Dodaj", url: "#/zasoby/dodaj", privileges: [USER_ROLES.admin, USER_ROLES.manager, USER_ROLES.developer, USER_ROLES.updater ], active :false},
                browse: {title: "Przeglądaj", url: "#/zasoby/przegladaj", privileges: [USER_ROLES.all], active :false}
            },
        },

        d_specification: {
            title: 'Specyfikacje', icon: 'fa fa-lg fa-fw fa-pencil-square-o',
            privileges: [USER_ROLES.all], id:"dd",
            children:{
                a_new : {title: "Nowa", url: "#/specyfikacja/nowa", privileges: [USER_ROLES.admin, USER_ROLES.manager, USER_ROLES.developer, USER_ROLES.updater ], active :false},
                browse: { title: "Przeglądaj", url: "#/specyfikacja/przegladaj", privileges: [USER_ROLES.all], active :false}
            },
        },

        e_people: {
            title: 'Personel', icon: 'fa fa-lg fa-fw fa-group',
            privileges: [USER_ROLES.all], id:"ee",
            children:{
                a_users : {title: "Użytkownicy",url: "#/personel/uzytkownicy", privileges: [USER_ROLES.admin], active :false },
                b_clients: {title: "Klienci",url: "#/personel/klienci", privileges: [USER_ROLES.all], active :false}
            },
        },

        f_amin: {
            title: 'Opcje Administratora', icon: 'fa fa-lg fa-fw fa-gears',
            privileges: [USER_ROLES.admin], id:"ff",
            children:{
                a_users : {title: "Transfer kart SIM",url: "#/admin/sim", privileges: [USER_ROLES.admin], active :false },
            },
        }

     };
});


/** FLASH **/
app.factory('flash', function ($timeout) {
	var obj = {};

	obj.visible = false;
	obj.message = "test";
	obj.type = " ";
	obj.show = function(msg,typ){
		obj.message = msg;
		switch(typ){
			case 'success':
				obj.type = "myFlash flash-success"
				break;
			case 'error':
				obj.type = "myFlash flash-error"
				break
			case 'info':
				obj.type = "myFlash flash-info"
				break
			case 'warning':
				obj.type = "myFlash flash-warning"
				break
		}
		obj.visible = true;
  		$timeout(function(){
			obj.visible = false;
		}, 4000);
 	}
  	return obj;
})

app.directive('flashmessage', function(flash) {
    return {
		restrict: 'E',
        template: '<div class="{[flash.type]}"  ng-show="flash.visible"  ng-click="flash.visible=!flash.visible"> {[flash.message]} <span id="closeflash" class="pull-right"> × </span></div>' ,
    }
});


/** USERS **/
app.provider('Users', function () {
    this.users = null;

    this.$get = function($http, $rootScope) {
        return {
            users: this.users,
            getUsers: function (){
                return this.users;
            },
            addUsers: function(user){
            	this.users.push(user)
            },
            loadUsersData : function(){
                $http.get("getUsers.py")
                  .success(function(data) {
                    this.users = data;
                    $rootScope.$broadcast('usersDataReady',  data);
                })
            }
        }
    };
});


/** CLIENTS **/
app.provider('Clients', function () {
    this.clients = null;
    this.userindex = null;

    this.$get = function($http, $rootScope, Session) {
        return {
            userindex: this.userindex,
            clients: this.clients,
            getClients: function (){
                return this.clients;
            },
            addClient: function(client){
            	this.clients.push(client)
            },
            loadClientsData : function(){
                $http.get("getClients.py")
                  .success(function(data) {
                    this.clients = data;
                    if(Session.user){
                      for(var i =0; i<data.length; i++){
                        if(data[i].id == Session.user.client_id){
                          this.userindex = i;
                          Session.client=data[i]
                          break;
                        }
                      }
                    }
                    $rootScope.$broadcast('clientsDataReady',  data)

                })
            }
        }
    };

});


/** COMPANY **/
app.provider('Company', function () {
    this.company = null;

    this.$get = function($http, $rootScope) {
        return {
            company: this.company,
            getCompany: function (){
                return this.company;
            },
            loadCompanData : function(){
                $http.get("getCompany.py")
                  .success(function(data) {
                    this.company = data;
                    $rootScope.$broadcast('companyDataReady',  data)
                })
            }
        }
    };
});

/** DEVICES **/
app.provider('Devices', function () {
    this.devices = null;

    this.$get = function($http, $rootScope) {
        return {
            devices: this.devices,
            getDevices: function (){
                return this.devices;
            },
            loadDevicesData : function(){
                $http.get("getDevices.py")
                  .success(function(data) {

                    this.devices = data;
                    $rootScope.$broadcast('devicesDataReady',  data)
                    $rootScope.$broadcast('dDataReady',  data)
                })
            }
        }
    };
});

/** DEVICE ARCHIVE **/
app.provider('DeviceArchive', function () {
    this.devicearchive = null;
    this.$get = function($http, $rootScope) {
        return {
            devicearchive: this.devicearchive,
            getDeviceArchive: function (){
                return this.devicearchive;
            }
        }
    };
});

/** DEVICES TYPE **/
app.provider('DevicesTypes', function () {
    this.devicetypes = null;

    this.$get = function($http, $rootScope) {
        return {
            devicetypes: this.devicetypes,
            getDeviceTypes: function (){
                return this.devicetypes;
            },
            loadDeviceTypesData : function(){
                $http.get("getDevicesType.py")
                  .success(function(data) {
                    this.devicetypes = data;
                    $rootScope.$broadcast('deviceTypesDataReady',  data)
                })
            }
        }
    };
});

/** DEVICES MODELS **/
app.provider('DevicesModels', function () {
    this.devicemodels = null;

    this.$get = function($http, $rootScope) {
        return {
            devicemodels: this.devicemodels,
            getDeviceModels: function (){
                return this.devicemodels;
            },
            loadDeviceModelsData : function(){
                $http.get("getDevicesModel.py")
                  .success(function(data) {
                    this.devicemodels = data;
                    $rootScope.$broadcast('deviceModelsDataReady',  data)
                })
            }
        }
    };
});

/** DEVICES STATES **/
app.provider('DevicesStates', function () {
    this.devicestates = null;

    this.$get = function($http, $rootScope) {
        return {
            devicestates: this.devicestates,
            getDeviceState: function (){
                return this.devicestates;
            },
            loadDeviceStateData : function(){
                $http.get("getDevicesState.py")
                  .success(function(data) {
                    this.devicestates = data;
                    $rootScope.$broadcast('deviceStatesDataReady',  data)
                })
            }
        }
    };
});


/** SIM **/
app.provider('SIM', function () {
    this.sim = null;

    this.$get = function($http, $rootScope) {
        return {
            sim: this.sim,
            getSim: function (){
                return this.sim;
            },
            loadSimData : function(){
                $http.get("getSim.py")
                  .success(function(data) {
                    this.sim = data;
                    $rootScope.$broadcast('simsDataReady',  data)
                })
            }
        }
    };
});

/** SIM ARCHIVE **/
app.provider('SimArchive', function () {
    this.simarchive = null;
    this.$get = function($http, $rootScope) {
        return {
            simarchive: this.simarchive,
            getSimArchive: function (){
                return this.simarchive;
            }
        }
    };
});

/** TRANSFER SIM **/
app.provider('Transfer', function () {
    this.simtransfer = null;

    this.$get = function($http, $rootScope) {
        return {
            simtransfer: this.simtransfer,
            getSimTransfer: function (){
                return this.simtransfer;
            },
            loadSimTransferData : function(){
                $http.get("getSimTransfer.py")
                  .success(function(data) {
                    this.simtransfer = data;
                    $rootScope.$broadcast('simsTransfersDataReady',  data)
                })
            }
        }
    };
});


/** OPERATION **/
app.provider('Operations', function () {
    this.operations = null;
    this.$get = function($http, $rootScope) {
        return {
            operations: this.operations,
            getOperations: function (){
                return this.operations;
            },
            loadOperationsData : function(){
                $http.get("getOperations.py")
                  .success(function(data) {
                    this.operations = data;
                    $rootScope.$broadcast('operationsDataReady',  data)
                })
            }
        }
    };
});


/** TRANSACTION **/
app.provider('Transaction', function () {
    this.transactions;
    this.$get = function($http, $rootScope) {
        return {
            transactions: this.transactions,
            getTransactions: function (){
                return this.transactions;
            },
            loadTransactionsData : function(){
                $http.get("getTransactions.py")
                  .success(function(data) {
                    this.transactions = data;
                    $rootScope.$broadcast('transactionsDataReady',  data)
                })
            }
        }
    };
});



/** SUB TRANSACTION  DEVICE**/
app.provider('SubTransactionDevice', function () {
    this.subtransactionsdevice = null;
    this.$get = function($http, $rootScope) {
        return {
            subtransactionsdevice: this.subtransactionsdevice,
            getSubTransactionsDevice: function (){
                return this.subtransactionsdevice;
            },
            loadSubTransactionDeviceData : function(){
                $http.get("getSubTransactionDevice.py")
                  .success(function(data) {
                    this.subtransactionsdevice = data;
                    $rootScope.$broadcast('subTransactionDeviceDataReady',  data)
                })
            }
        }
    };
});

/** SUB TRANSACTION SIM**/
app.provider('SubTransactionSIM', function () {
    this.subtransactionssim = null;
    this.$get = function($http, $rootScope) {
        return {
            subtransactionssim: this.subtransactionssim,
            getSubTransactionsSim: function (){
                return this.subtransactionssim;
            },
            loadSubTransactionSimData : function(){
                $http.get("getSubTransactionSim.py")
                  .success(function(data) {
                    this.subtransactionssim = data;
                    $rootScope.$broadcast('subTransactionSimDataReady',  data)
                })
            }
        }
    };
});


/** COMMENTS **/
app.provider('Comments', function () {
    this.comments = null;
    this.$get = function($http, $rootScope) {
        return {
            comments: this.comments,
            getComments: function (){
                return this.comments;
            },
            loadCommentsData : function(){
                $http.get("getComments.py")
                  .success(function(data) {
                    this.comments = data;
                    $rootScope.$broadcast('commentsDataReady',  data)
                })
            }
        }
    };
});


/** TransactionArchive **/
app.provider('TransactionArchive', function () {
    this.transactionarchive = null;
    this.$get = function($http, $rootScope) {
        return {
            transactionarchive: this.transactionarchive,
            getTransactionArchive: function (){
                return this.transactionarchive;
            },
            loadTransactionArchiveData : function(){
                $http.get("getTransactionArchive.py")
                  .success(function(data) {
                    this.transactionarchive = data;
                    $rootScope.$broadcast('transactionarchiveDataReady',  data)
                })
            }
        }
    };
});


/** Specyfikacje **/
app.provider('Specification', function () {
    this.specifications = null;
    this.$get = function($http, $rootScope) {
        return {
            specifications: this.specifications,
            getSpecification: function (){
                return this.specifications;
            },
            loadSpecificationsData : function(){
                $http.get("getSpecification.py")
                  .success(function(data) {
                    this.specifications = data;
                    $rootScope.$broadcast('specificationsDataReady',  data)
                })
            }
        }
    };
});

/** Datasheet **/
app.provider('Datasheet', function () {
    this.datasheets = [];
    this.$get = function($http, $rootScope) {
        return {
            datasheets: this.datasheets,
            getDatasheets: function (){
                return this.datasheets;
            },
            loadDatasheetsData : function(){
                $http.get("getDatsheet.py")
                  .success(function(data) {
                    this.datasheets = data;
                    $rootScope.$broadcast('datasheetsDataReady',  data)
                })
            }
        }
    };
});


/************** ACCESS CONTROL ********************/
/************** ACCESS CONTROL ********************/
app.constant('USER_ROLES', {
  all: {name:'all', id :0},
  admin: {name:'Administrator',id :1},
  manager: {name:'Manager',id :2},
  developer: {name:'Developer',id :3},
  updater: {name:'Updater',id :4},
  viewer: {name:'Viewer',id :5}
})

app.service('Session', function () {
  this.create = function (person) {
    this.user = person;
  };
  this.destroy = function () {
    this.user = null;
  };
  return this;
})

app.factory('AuthService', function ($http, Session, $rootScope, USER_ROLES) {
  var authService = {};

  authService.login = function () {
    return $http.get("getUserInfo.py")
      .then(function (res) {
        Session.create(res.data);
        $rootScope.$broadcast('userLoggedIn',  res.data)
        return  res.data;
      });
  };

  authService.logout = function(){
 	Session.destroy();
  }

  authService.isAuthenticated = function () {
  	if(!Session.user){
  		return false;
  	}
    return !!Session.user.id;
  };

  authService.isAuthorized = function (authorizedRoles) {
    if (!angular.isArray(authorizedRoles)) {
      authorizedRoles = [authorizedRoles];
    }

    if(authService.isAuthenticated() && authorizedRoles.indexOf(USER_ROLES.all) !== -1){
    	return true
    }else{
      if(authService.isAuthenticated()){
        for (i = 0; i < authorizedRoles.length; i++) {
          if (angular.equals(authorizedRoles[i], Session.user.privileges)) {
              return true;
          }
        }
        return false;
      }else{
        return false;
      }
   		//return (authService.isAuthenticated() &&
       // 	authorizedRoles.indexOf(Session.user.privileges) !== -1);
    }

  };
 /* */
  return authService;
})





/************** PRELOADING and CONFIGURATION  ********************/
/************** PRELOADING and CONFIGURATION  ********************/

app.run(function($http, Clients, Company, $timeout, $rootScope, $location, AuthService){
	if($location.absUrl().indexOf('default/index')>0 && $location.absUrl().indexOf('user')==-1){ // run only when we are in default/index
		$location.path("/")
		AuthService.login()
		$rootScope.$on('$stateChangeStart', function (event, next) {
			if(AuthService.isAuthenticated()){
				var authorizedRoles = next.data.authorizedRoles;
			    if (!AuthService.isAuthorized(authorizedRoles)) {
			      event.preventDefault();
		    	}
			}

	 	});
	}

});




app.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {
  $stateProvider
    /*.state('home', {
        url : '',
        templateUrl: 'dashboard.html',
        data: {
	      authorizedRoles: [USER_ROLES.admin]
	    }
    })*/
    .state('home2', {
        url : '/',
        templateUrl: 'dashboard.html',
        data: {
	      authorizedRoles: [USER_ROLES.all]
	    }
    })
    .state('newTr', {
        url: '/transakcja/nowa',
        templateUrl: 'newTransaction.html',
        data: {
	      authorizedRoles: [USER_ROLES.admin, USER_ROLES.manager, USER_ROLES.developer]
	    }

    })
    .state('transaction', {
        url: '/transakcja/przegladaj',
        templateUrl: 'browseTransaction.html',
        data: {
	      authorizedRoles: [USER_ROLES.all]
	    }

    })
    .state('resource', {
        url: '/zasoby/dodaj',
        templateUrl: 'newResources.html',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.manager, USER_ROLES.developer, USER_ROLES.updater ]
        }

    })
    .state('browseRes', {
        url: '/zasoby/przegladaj',
        templateUrl: 'browseResources.html',
        data: {
          authorizedRoles: [USER_ROLES.all]
        }

    })
    .state('newSpce', {
        url: '/specyfikacja/nowa',
        templateUrl: 'newSpecification.html',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.manager, USER_ROLES.developer, USER_ROLES.updater ]
        }

    })
     .state('browseSpce', {
        url: '/specyfikacja/przegladaj',
        templateUrl: 'browseSpecification.html',
        data: {
          authorizedRoles: [USER_ROLES.all]
        }

    })

    .state('uzytkownicy', {
        url: '/personel/uzytkownicy',
        templateUrl: 'users.html',
        data: {
	      authorizedRoles: [USER_ROLES.admin]
	    }
    })
    .state('klienci', {
        url: '/personel/klienci',
        templateUrl: 'clients.html',
        data: {
	      authorizedRoles: [USER_ROLES.all]
	    }
    })
    .state('transferSim', {
        url: '/admin/sim',
        templateUrl: 'transferSim.html',
        data: {
          authorizedRoles: [USER_ROLES.admin]
        }
    })


})


/**   PAIR CLIENT USER */
app.filter('pairClientUser', function() {
  return function(input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (+input[i].id == +id) {
        return input[i];
      }
    }
    return null;
  }
})


/************** CONTROLLERS  ********************/
/************** CONTROLLERS  ********************/



/** MAIN CONTROLLER**/
app.controller("MainCtrl", function($scope,$rootScope, $modal, $location, side_menu, Clients, USER_ROLES, AuthService) {
	var scope = this;
	this.sideMenuMinimize = false
	this.sideMenuHidden = false
	this.loginPage = false


    this.items = side_menu;

	if($location.absUrl().indexOf('user')>0){
		this.loginPage = true;
		this.sideMenuMinimize = true;
		this.sideMenuHidden = true;
	}

 	this.currentUser = null;
 	this.userRoles = USER_ROLES;
 	this.isAuthorized = AuthService.isAuthorized;

 	this.setCurrentUser = function (user) {
    	this.currentUser = user;
  };


  $scope.$on('userLoggedIn', function (event, data) {
		scope.setCurrentUser(data)
	});

  this.clients = Clients.getClients();
	this.setClient = function(data){
 		this.clients = data;

 	}
    $scope.$on('clientsDataReady', function (event, data) {
		scope.setClient(data)
	});

    this.selectedTab = side_menu.a_home;
    this.parentTab = null;
    this.selectTab = function(tab, parent){
    	this.selectedTab = tab;
    	this.parentTab = parent;
    };


    this.selTab =  this.items.a_home;
    this.openTab =  null;

    this.switchOpen = function(item){
    	if(item.children){
    		this.openTab =  item;
    		$("nav li").each(function(){
		         $(this).find("ul").not("#sidemenuid"+item.id).slideUp(200)
		      })
    		var path = window.location.href;
    		$("#sidemenuid"+item.id).slideDown(200)

    	}else{
    		this.selTab =  item;
    		this.openTab =  null;
            $("nav li").each(function(){
                 $(this).find("ul").slideUp(200)
            })
    	}
    }

    this.switchActive = function(item){
     	this.selTab =  item
    }





    /** EDIT TRANSACTION MODAL **/
        this.openTransactionModal = function (tran) {
          var modalInstance = $modal.open({
            templateUrl: 'transactionModalContent.html',
            controller: 'transactionModalInstanceCtrl',
            size: 100,
            resolve: {
              transaction: function () {
                return tran
              }
             // transaction :transaction
            }
          });

          modalInstance.result.then(function (selectedItem) {
           this.selected = selectedItem;
          }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
          });
        };

    /** EDIT RESOURCE MODAL **/
        this.openResourceModal = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'resourceModalContent.html',
                controller: 'resourceModalInstanceCtrl',
                size: 100,
                resolve: {
                  res: function () {
                    return item
                  }
                }
            });
            modalInstance.result.then(function (){}, function (){} );
         };

    /** ADD/EDIT DEV OPTION MODAL **/
        this.openDevOptionModal = function (what) {
            var modalInstance = $modal.open({
                templateUrl: 'devOptionModalContent.html',
                controller: 'devOptionModalInstanceCtrl',
                size: 100,
                resolve: {
                  dest: function () {
                    return what
                  }
                }
            });
            modalInstance.result.then(function (){}, function (){} );
         };

   /** ADD/EDIT CLIENT MODAL **/
        this.openClientModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'clientModalContent.html',
                controller: 'clientModalInstanceCtrl',
                size: 100,
               // resolve: {
                //  dest: function () {
                //    return what
                //  }
               // }
            });
            modalInstance.result.then(function (){}, function (){} );
         };

   /** ADD NEW RESOURCE FROM FILR  MODAL **/
        this.openNewResFiletModal = function (file) {
            var modalInstance = $modal.open({
                templateUrl: 'newResourcesFileModalContent.html',
                controller: 'newResourcesFileModalInstanceCtrl',
                size: 100,
                resolve: {
                  file: function () {
                    return file
                  }
                }
            });
            modalInstance.result.then(function (){}, function (){} );
         };

    /** SPECIFICATION & DATASHEET  MODAL **/
        this.openSpecificationModal = function (spec) {
            var modalInstance = $modal.open({
                templateUrl: 'specificationModalContent.html',
                controller: 'specificationModalInstanceCtrl',
                size: 100,
                resolve: {
                  specification: function () {
                    return spec
                  }
                }
            });
            modalInstance.result.then(function (){}, function (){} );
         };




    return $scope.MainCtrl = this;
});


app.factory("XLSXReaderService", ['$q', '$rootScope',function($q, $rootScope) {
    var service = function(data) {
        angular.extend(this, data);
    }

    service.readFile = function(file, readCells, toJSON) {
        var deferred = $q.defer();

        XLSXReader(file, readCells, toJSON, function(data) {
            $rootScope.$apply(function() {
                deferred.resolve(data);
            });
        });

        return deferred.promise;
    }
    return service;
}]);



app.directive('pdfmaker', function(CPLOGO){
  return {
    scope: {
      transaction: "=trans",
      devices: "=dev" ,
      sim: "=sim",
      subtrDev : "=subtrdev",
      subtrSim : "=subtrsim"
    },
    controller: function($scope, $element, $attrs, $transclude) {
        $scope.today = function(){
            var now = new Date()
            var day = now.getDate()
            var month = now.getMonth()+1

            if(day<10){
                day = "0"+day.toString()
            }
            if(month<10){
                month = "0"+month.toString()
            }
            return  day+"-"+month+"-"+now.getFullYear()
        }

        var getItem =function(src, id){ // zwraca element o określonym id
            for(var i = 0; i<src.length;i++ ){
                if(src[i].id == id){
                  return src[i]
                }
            }
        }

        if($scope.transaction.from.phone && ($scope.transaction.from.phone!="")){
            $scope.fromPhone = $scope.transaction.from.phone;
        }else{
            $scope.fromPhone = "";
        }
        if($scope.transaction.to.phone && ($scope.transaction.to.phone!="")){
            $scope.toPhone = $scope.transaction.to.phone;
        }else{
            $scope.toPhone = "";
        }

        $scope.getEquipmentList = function(){
            var equipment = []
            for(var i =0; i< $scope.subtrDev.length;i++){
                dev = getItem($scope.devices, $scope.subtrDev[i].device_id)
                var params = "  "
                if(dev.sn && (dev.sn!="")){
                    params +="  SN: "+dev.sn
                }
                if(dev.mac && (dev.mac!="")){
                    params +="  MAC: "+dev.mac
                }
                if(dev.imei && (dev.imei!="")){
                    params +="  IMEI: "+dev.imei
                }
                equipment.push({
                  text: (dev.type.name+"  "+dev.model.name+"  "+dev.company.name+" - "+params), fontSize: 12
                })
            }

            for(var i =0; i< $scope.subtrSim.length;i++){
                sim = getItem($scope.sim, $scope.subtrSim[i].sim_id)
                var params = "  "
                if(sim.msisdn && (sim.msisdn!="")){
                    params +="  MSISDN: "+sim.msisdn
                }
                if(sim.imsi && (sim.imsi!="")){
                    params +="  IMSI: "+sim.imsi
                }
                if(sim.iccid && (sim.iccid!="")){
                    params +="  ICCID: "+sim.iccid
                }
                equipment.push({
                  text: ("SIM - "+params), fontSize: 12
                })
            }
            return equipment
        }

        if($scope.transaction.description && $scope.transaction.description.indexOf("<br>")>-1){
            $scope.description = $scope.transaction.description.slice(0,-4)
        }else{
            $scope.description = $scope.transaction.description
        }





    },
    restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment

    link: function(scope, elem, attrs, controller){
        elem.on('click',function(){
            console.log(scope.fromName);

            var docDefinition = {
                pageSize: 'A4',
                pageOrientation: 'portrait',
                pageMargins: [30, 25, 25, 25],
                fontSize: 12,
                content: [
                  {
                    columns: [
                      {
                        text: scope.today().toString(),  fontSize: 10, alignment: 'left'
                      },
                      {
                        image: 'cplogo', alignment: 'right',width: 90, height: 40
                      }
                    ]
                  },
                  {
                    text: "\n\nProtokół przekazania", alignment: 'center', fontSize: 14, bold: true
                  },
                  '___________________________________________________________________________________________________\n\n\n',
                  {
                    columns: [
                      {
                        text:[
                            { text:"Od kogo:\n", bold: true },
                            { text: scope.transaction.from.firstname +" "+scope.transaction.from.lastname , fontSize: 12 },"\n",
                            { text: scope.transaction.from.email, fontSize: 12 },"\n",
                            { text: scope.transaction.from.company.name, fontSize: 12 },"\n",
                            { text: scope.fromPhone  , fontSize: 12 }
                        ]
                      },
                      {
                        text:[
                            {text:"Do kogo:\n", bold: true },
                            { text: scope.transaction.to.firstname +" "+scope.transaction.to.lastname , fontSize: 12 },"\n",
                            { text: scope.transaction.to.email, fontSize: 12 },"\n",
                            { text: scope.transaction.to.company.name, fontSize: 12 }, "\n",
                            { text: scope.toPhone, fontSize: 12 }
                        ]
                      }
                    ]
                  },
                  {text:"\n\nDane transakcji:\n", bold: true, fontSize: 12 },
                  {text: "Numer transakcji: "+scope.transaction.id.toString() , fontSize: 12},
                  {text: "Operacja: "+scope.transaction.operation.name, fontSize: 12},
                  {text: "Data przekazania: "+scope.transaction.beginning.toString(), fontSize: 12},
                  {text:[
                     {text: "Termin zwrotu: ",fontSize: 12},
                     {text: scope.transaction.ending.toString(), decoration: 'underline',  decorationColor: 'red',fontSize: 12},
                    ]
                  }, "\n",
                  {text: scope.description, fontSize: 12},"\n","\n",
                  {
                    ol: scope.getEquipmentList()
                  },"\n","\n","\n",
                  {
                    columns: [
                        {
                            text: "Podpis wydajacego", alignment: 'center', fontSize: 12
                        },
                        {
                            text: "Podpis osoby odbierającej", alignment: 'center', fontSize: 12
                        }
                      ]
                  }
                ],

                images: {
                    cplogo:  CPLOGO
                }
            };

            pdfMake.createPdf(docDefinition).open();

        })
    }
  }
});


app.directive('pdfspecification', function(CPLOGO){
  return {
    scope: {
      specification: "=spec",
      datasheet: "=data" ,
      description: "=desc",
      description_lines: "=desclines",
      datasheet_view: "=view"
    },
    controller: function($scope, $element, $attrs, $transclude) {

        $scope.today = function(){
            var now = new Date()
            var day = now.getDate()
            var month = now.getMonth()+1

            if(day<10){
                day = "0"+day.toString()
            }
            if(month<10){
                month = "0"+month.toString()
            }

            return  day+"-"+month+"-"+now.getFullYear()
        }

        $scope.hexToRgb = function(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : {r:0, g:0, b:0};
        }

    },
    restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment

    link: function(scope, elem, attrs, controller){
        elem.on('click',function(){
            var imgData =  CPLOGO

            var margin = 25
            var page_width = 210
            var page_heiht = 210
            var line = margin
            var offset = 0

            var doc = new jsPDF('p','mm', 'a4');
            doc.setFont("times")

            function refresh(size, style, color){
                doc.setFontSize(size);
                rgb = scope.hexToRgb(color)
                doc.setTextColor(rgb.r, rgb.g, rgb.b)
                doc.setFontType(style);
            }

            function writeLine(text, size, style, color){
                refresh(size, style, color)
                doc.text( text, margin, line );
                line = line + (size/2.834645669)
            }

            function write(text, offset, size, style, color){
                refresh(size, style, color)
                doc.text( text, margin + offset, line );
            }

            function newLine(size){
                line = line + (size/2.434645669)
                offset = 0
            }


            doc.addImage(imgData, 'JPEG', page_width-margin-29, margin-15, 29, 14);

            refresh(12, "normal", "#000000")
            doc.text( scope.today(), margin, margin );
            refresh(14, "bold", "#000000")
            doc.text( 'Specyfikacja', (page_width/2)-12 , margin+15);

            doc.line(margin, margin+17, page_width-margin, margin+17);

            line =  margin+30


            write("Model: ", 0, 12, "bold", "#000000")
            if (scope.specification.model){
                write(scope.specification.model.name, 15, 12, "normal", "#000000")
            }

            write("Firma: ", 60, 12, "bold", "#000000")
            if(scope.specification.company){
                write(scope.specification.company.name, 75, 12, "normal", "#000000")
            }

            write("Data dodania: ", 112, 12, "bold", "#000000")
            write(scope.specification.adddate, 140, 12, "normal", "#000000")


            if(scope.specification.company){
                newLine(16)
                write("System: ", 0, 12, "bold", "#000000")
                write(scope.specification.operating_system, 15, 12, "normal", "#000000")

                write("Wersja: ", 50, 12, "bold", "#000000")
                write(scope.specification.system_version, 65, 12, "normal", "#000000")

                write("Numer wersji: ", 90, 12, "bold", "#000000")
                write(scope.specification.system_version_number, 120, 12, "normal", "#000000")
            }
            if(scope.specification.firmware){
                newLine(16)
                write("Firmware: ", 0, 12, "bold", "#000000")
                write(scope.specification.firmware, 15, 12, "normal", "#000000")

                write("Hardware: ", 60, 12, "bold", "#000000")
                write(scope.specification.hardware, 65, 12, "normal", "#000000")
            }
            if(scope.specification.description){
               newLine(36)
               writeLine(scope.description.toString(), 12, "normal", "#000000")

               for(var i = 0; i<parseInt(scope.description_lines);i++ ){
                    newLine(12)
               }
            }

            newLine(14)


            var col = 0
            var left_line = line
            var right_line = line

            function data(one, addspace, header, propertys, data){
                if(one==true){
                    col = 0
                }

                space = col*90
                if(col == 1){
                    line = right_line
                }else{
                    line = left_line
                }



                if((line+((propertys.length*5) +15)) > (297 -margin)){
                    doc.addPage()
                    line = margin
                    right_line = margin
                    left_line = margin
                }

                write(header, space, 12, "bold", "#000000")
                doc.line(margin+space, line+1, margin+space+40, line+1);

                newLine(14)
                for(var i = 0; i < propertys.length; i++){
                    write(propertys[i].toString(), space, 12, "bold", "#000000")
                    if( data[i] ==null){
                        var v = "---"
                    }else{
                       var v= data[i].toString()
                    }
                    write(v, space+20+addspace, 12, "normal", "#000000")
                    newLine(12)
                }
                newLine(24)

                if(col == 1){
                   right_line = line
                   col =0
                }else{
                    left_line =line
                    col =1
                }


                if(one==true){
                    right_line = line
                    left_line =line
                    col =0
                }
            }

            if(scope.datasheet_view.chipset){
                data(false, 0,"Chipset: ", ["Provider", "Model","Baseline"], [scope.datasheet.chipset_prvider.name, scope.datasheet.chipset_model, scope.datasheet.chipset_baseline])
            }

            if(scope.datasheet_view.terminal){
                data(false, 10,"Terminal category: ", ["3G Downlink", "3G Uplink","LTE"], [scope.datasheet.cat_3g_downlink, scope.datasheet.cat_3g_uplink, scope.datasheet.cat_lte])
            }

            if(scope.datasheet_view.mimo_duplex){
                if(scope.datasheet.mimo){
                    temp1 = scope.datasheet.mimo
                }else{
                    temp1 = "---"
                }
                if(scope.datasheet.duplex){
                    temp2 = scope.datasheet.duplex
                }else{
                    temp2 = "---"
                }
                data(false, 0,"MIMO , Duplex: ", ["MIMO", "Duplex"], [temp1, temp2])
            }

            if(scope.datasheet_view.tgpp){
                if(scope.datasheet.tgpp){
                  temp = scope.datasheet.tgpp
                }else{
                    temp = "---"
                }
                data(false, 0, "3GPP: ", ["Release"], [temp])
            }

            if(scope.datasheet_view.systems){
                temp = ""
                if(scope.datasheet.system_edge){
                   temp += "EDGE; "
                }
                if(scope.datasheet.system_gprs){
                   temp += "GPRS; "
                }
                if(scope.datasheet.system_gsm){
                   temp += "GSM; "
                }
                if(scope.datasheet.system_hspa){
                   temp += "HSPA; "
                }
                if(scope.datasheet.system_hspa_p){
                   temp += "HSPA+; "
                }
                if(scope.datasheet.system_lte){
                   temp += "LTE; "
                }
                if(scope.datasheet.system_lte_a){
                   temp += "LTE A; "
                }
                if(scope.datasheet.system_wifi){
                   temp += "WiFi; "
                }
                data(false, -20,"Systems: ", [" "], [temp])
            }


            if(scope.datasheet_view.mobility){
                headers = []
                content = []
                if(scope.datasheet.lte_3g_handover){
                    headers.push("")
                    content.push("LTE --> 3G handover ")
                }
                if(scope.datasheet.lte_2g_handover){
                    headers.push("")
                    content.push("LTE --> 2G handover ")
                }
                if(scope.datasheet.lte_2g_nacc){
                    headers.push("")
                    content.push("LTE --> 2G NACC")
                }
                if(scope.datasheet.tg_lte_handover){
                    headers.push("")
                    content.push("3G --> LTE handover")
                }
                if(scope.datasheet.dg_lte_handover){
                    headers.push("")
                    content.push("2G --> LTE handover")
                }
                if(scope.datasheet.inter_frequency_handover){
                    headers.push("")
                    content.push("Inter-frequency handover")
                }
                if(scope.datasheet.fdd_tdd_handover){
                    headers.push("")
                    content.push("FDD <--> TDD handover")
                }
                data(false, -20,"Mobility: ", headers, content)
            }

            if(scope.datasheet_view.rohc){
                if(scope.datasheet.rohc){
                  temp = scope.datasheet.rohc
                }else{
                    temp = "---"
                }
                data(false, -20, "ROHC: ", [""], [temp])
            }

            if(scope.datasheet_view.drx_dual_carrier){
                if(scope.datasheet.drx){
                  temp = scope.datasheet.drx
                }else{
                    temp = "---"
                }

                if(scope.datasheet.dual_carrier){
                  temp1 = scope.datasheet.dual_carrier
                }else{
                    temp1 = "---"
                }
                data(false, 20, "DRX & Dual Carrier: ", ["DRX" , "Dual Carier fro HSPA+" ], [temp,temp1])
            }


            if(scope.datasheet_view.services){
                temp = ""
                if(scope.datasheet.csfb){
                   temp += "CSFB; "
                }
                if(scope.datasheet.sms_sgs){
                   temp += "SMS over SGs; "
                }
                if(scope.datasheet.sms_ims){
                   temp += "SMS over IMS; "
                }
                if(scope.datasheet.volte){
                   temp += "VoLTE; "
                }

                data(false, -20,"Services: ", [""], [temp])
            }


            if(scope.datasheet_view.bands){
                if(scope.datasheet.band_2g){
                    band_2g = scope.datasheet.band_2g
                }else{
                    band_2g = "---"
                }
                if(scope.datasheet.band_3g){
                    band_3g = scope.datasheet.band_3g
                }else{
                    band_3g = "---"
                }
                if(scope.datasheet.band_lte){
                    band_lte = scope.datasheet.band_lte
                }else{
                    band_lte = "---"
                }
                if(scope.datasheet.band_wifi){
                    band_wifi = scope.datasheet.band_wifi
                }else{
                    band_wifi = "---"
                }
                data(true, 0,"Bands: ", ["2G", "3G", "LTE", "WiFi"], [band_2g, band_3g,band_lte,band_wifi])
            }


            if(scope.datasheet_view.domain){
                if(scope.datasheet.ue_usage_setting){
                    temp1 = scope.datasheet.ue_usage_setting
                }else{
                    temp1 = "---"
                }
                if(scope.datasheet.voice_domain){
                    temp2 = scope.datasheet.voice_domain
                }else{
                    temp2 = "---"
                }
                data(true, 55,"Domain preference: ", ["UE usage setting", "Voice domain preference for EUTRAN"], [temp1, temp2])
            }

            if(scope.datasheet_view.aggregation){

                if(scope.datasheet.intra_band_c_CA){
                   temp1  = scope.datasheet.intra_band_c_CA
                }else{
                    temp1 ="---"
                }

                if(scope.datasheet.intra_band_nc_CA){
                   temp2  =  scope.datasheet.intra_band_nc_CA
                }else{
                    temp2 ="---"
                }
                if(scope.datasheet.inter_band_CA){
                   temp3  = scope.datasheet.inter_band_CA
                }else{
                    temp3 ="---"
                }
                data(true, 40,"Carrier aggregation for LTE : ", ["Intra-band contiguous CA", "Intra-band non-contiguous CA", "Inter-band CA" ], [temp1, temp2, temp3])
            }
            doc.save('temp.pdf');

        })

    }

  };
});


app.directive('pdftransaction', function(CPLOGO){
  return {
    scope: {
      transaction: "=trans",
      devices: "=dev" ,
      sim: "=sim",
      subtrDev : "=subtrdev",
      subtrSim : "=subtrsim",
    },
    controller: function($scope, $element, $attrs, $transclude) {

        $scope.today = function(){
            var now = new Date()
            var day = now.getDate()
            var month = now.getMonth()+1

            if(day<10){
                day = "0"+day.toString()
            }
            if(month<10){
                month = "0"+month.toString()
            }

            return  day+"-"+month+"-"+now.getFullYear()
        }

        $scope.hexToRgb = function(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : {r:0, g:0, b:0};
        }

    },
    restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment

    link: function(scope, elem, attrs, controller){
        elem.on('click',function(){
            var imgData =  CPLOGO

            var margin = 25
            var page_width = 210
            var page_heiht = 210
            var line = margin
            var offset = 0

            var doc = new jsPDF('p','mm', 'a4');
            doc.setProperties({
                title: 'Raport',
                subject: 'Transakcja',
                author: 'CBS',
                charset:"UTF-8"
            });

            doc.setFont("times")

            function refresh(size, style, color){
                doc.setFontSize(size);
                rgb = scope.hexToRgb(color)
                doc.setTextColor(rgb.r, rgb.g, rgb.b)
                doc.setFontType(style);
            }

            function writeLine(text, size, style, color){
                refresh(size, style, color)
                doc.text( text, margin, line );
                line = line + (size/2.834645669)
            }

            function write(text, offset, size, style, color){
                refresh(size, style, color)
                doc.text( text, margin + offset, line );
            }

            function newLine(size){
                line = line + (size/2.434645669)
                offset = 0
            }

            function changechar(str){
                str = str.replace("ą", "a");
                str = str.replace("ć", "c");
                str = str.replace("ę", "e");
                str = str.replace("ń", "n");
                str = str.replace("ł", "l");
                str = str.replace("ó", "o");
                str = str.replace("ś", "s");
                str = str.replace("ż", "z");
                str = str.replace("ź", "z");

                str = str.replace("Ą", "A");
                str = str.replace("Ć", "C");
                str = str.replace("Ę", "E");
                str = str.replace("Ń", "N");
                str = str.replace("Ł", "L");
                str = str.replace("Ó", "O");
                str = str.replace("Ś", "S");
                str = str.replace("Ż", "Z");
                str = str.replace("Ź", "Z");

                return str
            }
            doc.addImage(imgData, 'JPEG', page_width-margin-29, margin-15, 29, 14);

            refresh(12, "normal", "#000000")
            doc.text( scope.today(), margin, margin );
            refresh(14, "bold", "#000000")
            doc.text( 'Raport przekazania', (page_width/2)-18 , margin+15);

            doc.line(margin, margin+17, page_width-margin, margin+17);

            line =  margin+30

            function getItem(src, id){ // zwraca element o określonym id
              for(var i = 0; i<src.length;i++ ){
                if(src[i].id == id){
                  return src[i]
                }
              }
            }

            /*
            write("Model: ", 0, 12, "bold", "#000000")
            write("Firma: ", 60, 12, "bold", "#000000")
            write("Data dodania: ", 112, 12, "bold", "#000000")
            */



            newLine(14)

            var col = 0
            var left_line = line
            var right_line = line

            function data(one, addspace, header, propertys, data){
                if(one==true){
                    col = 0
                }

                space = col*90
                if(col == 1){
                    line = right_line
                }else{
                    line = left_line
                }

                if((line+((propertys.length*5) +15)) > (297 -margin)){
                    doc.addPage()
                    line = margin
                    right_line = margin
                    left_line = margin
                }

                write(header, space, 12, "bold", "#000000")
                doc.line(margin+space, line+1, margin+space+40, line+1);

                newLine(14)
                for(var i = 0; i < propertys.length; i++){
                    write(propertys[i].toString(), space, 12, "bold", "#000000")
                    write(data[i].toString(), space+20+addspace, 12, "normal", "#000000")
                    newLine(12)
                }
                newLine(24)

                if(col == 1){
                   right_line = line
                   col =0
                }else{
                    left_line =line
                    col =1
                }


                if(one==true){
                    right_line = line
                    left_line =line
                    col =0
                }
            }

            newLine(12)
            if(scope.transaction.from.phone){
                phone = scope.transaction.from.phone
            }else{
               phone = ""
            }
            data(false, -20, "Od kogo", ["","","",""], [
                changechar(scope.transaction.from.firstname)+" "+changechar(scope.transaction.from.lastname),
                scope.transaction.from.email,
                scope.transaction.from.company.name,
                phone
                ])
            if(scope.transaction.to.phone){
                phone = scope.transaction.to.phone
            }else{
               phone = ""
            }
            data(false, -20, "Od kogo", ["","","",""], [
                changechar(scope.transaction.to.firstname)+" "+changechar(scope.transaction.to.lastname),
                scope.transaction.to.email,
                scope.transaction.to.company.name,
                phone
                ])

            data(true, 20, "Dane transakcji", ["Numer transakcji","Operacja","Data przekazania","Termin zwrotu"], [
                scope.transaction.id,
                changechar(scope.transaction.operation.name),
                scope.transaction.beginning,
                scope.transaction.ending,
                phone
                ])

            writeLine(changechar(scope.transaction.description),12, "normal", "#000000")
            newLine(20)

            left_line = line
            data(true, 0, "Zasoby", [], [])

            for(var i =0; i<scope.subtrDev.length;i++){
                dev = getItem(scope.devices, scope.subtrDev[i].device_id)
                writeLine((i+1).toString()+".",12, "normal", "#000000")
                temp = ""
                if(dev.sn){
                   temp += "       SN: "+dev.sn
                }

                if(dev.mac){
                   temp += "       MAC: "+dev.mac
                }

                if(!(dev.sn && dev.mac) && dev.imei){
                   temp += "       IMEI: "+dev.imei
                }
                writeLine(dev.type.name+"        "+dev.model.name+"        "+dev.company.name +temp ,12, "normal", "#000000")

                if((dev.sn && dev.mac) && dev.imei){
                    writeLine( "                                                       IMEI:  "+dev.imei ,12, "normal", "#000000")
                }

                if(line>(297-margin -10)){
                    doc.addPage()
                }
            }

            newLine(20)
            for(var i =0; i<scope.subtrSim.length;i++){
                writeLine((i+1).toString()+".",12, "normal", "#000000")
                sim = getItem(scope.sim, scope.subtrSim[i].sim_id)
                temp=""
                if(sim.msisdn){
                   temp += "       MSISDN: "+sim.msisdn
                }

                if(sim.iccid){
                   temp += "       ICCID: "+sim.iccid
                }


                writeLine("SIM:   "+sim.company.name +temp ,12, "normal", "#000000")
                if( sim.imsi){
                    writeLine("                                         IMSI:  "+sim.imsi ,12, "normal", "#000000")
                }


                if(line>(297-margin -10)){
                    doc.addPage()
                }

                if(line>(297-margin -10)){
                    doc.addPage()
                }
            }
            newLine(20)
            writeLine("                   Podpis wydajacego                                                          Podis odbiorcy"  ,12, "normal", "#000000")


            doc.save('raport.pdf');

        })

    }

  };
});
