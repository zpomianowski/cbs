app.controller("browseTransactionController", function($scope,flash,$q,ngTableParams, $filter, Transaction, SubTransactionDevice, SubTransactionSIM, Clients, Operations, Session, USER_ROLES, AuthService) {
	$scope.flash = flash
/*
	AuthService.isAuthorized

	this.userRoles = USER_ROLES;
 	this.isAuthorized = AuthService.isAuthorized;
*/

	$scope.clients = Clients.getClients();
	$scope.operations = Operations.getOperations();
	$scope.transactions = Transaction.getTransactions()

	//$scope.subtransactionsdevice = true//SubTransactionDevice.getSubTransactionsDevice()
	//$scope.subtransactionssim = true//SubTransactionSIM.getSubTransactionsSim()


/*GET CLIENTS ****************/
	if(!$scope.clients){
	 	$scope.clients = Clients.loadClientsData();
	}
	$scope.$on('clientsDataReady', function (event, data) {
		if($scope.transactions && !$scope.transactions[0].from){
			for(var i = 0; i<$scope.transactions.length;i++ ){
				$scope.transactions[i].from = $scope.getItem(data, $scope.transactions[i].from_id)
				$scope.transactions[i].to = $scope.getItem(data, $scope.transactions[i].to_id)
			}
		}
		$scope.clients = data;
		Clients.clients = data;

		$scope.changeLoading()
	});

/*GET OPERATION ****************/
	if(!$scope.operations){
	 	$scope.operations = Operations.loadOperationsData();
	}
	$scope.$on('operationsDataReady', function (event, data) {
		if($scope.transactions && !$scope.transactions[0].operation){
			for(var i = 0; i<$scope.transactions.length;i++ ){
				$scope.transactions[i].operation = $scope.getItem(data, $scope.transactions[i].operation_id)
			}
		}
		$scope.operations = data;
		Operations.operations = data;
		$scope.changeLoading()
	});

/*GET TRANSACJTION ****************/
	if(!$scope.transactions){
	 	$scope.transactions = Transaction.loadTransactionsData();
	}
	$scope.$on('transactionsDataReady', function (event, data) {
		if($scope.clients){
			for(var i = 0; i<data.length;i++ ){
				data[i].from = $scope.getItem($scope.clients, data[i].from_id)
				data[i].to = $scope.getItem($scope.clients, data[i].to_id)
				if($scope.operations){
					data[i].operation = $scope.getItem($scope.operations, data[i].operation_id)
				}
			}
		}
		if($scope.operations && !data[0].operation){
			for(var i = 0; i<data.length;i++ ){
				data[i].operation = $scope.getItem($scope.operations, data[i].operation_id)
			}
		}

		if(AuthService.isAuthorized([USER_ROLES.admin,USER_ROLES.manager])){
			$scope.transactions = data;
			Transaction.transactions = data;
		}else{
			var to = $filter('filter')(data, {"to_id": Session.user.client_id}, true );
 			var from = $filter('filter')(data, {"from_id": Session.user.client_id}, true );
			$scope.transactions = to.concat(from)
			Transaction.transactions = to.concat(from);
		}

		$scope.changeLoading()
	});

/*GET SUB TRANSACJTION DEVICES***************
	if(!$scope.subtransactionsdevice){
	 	$scope.subtransactionsdevice = SubTransactionDevice.loadSubTransactionDeviceData();
	}
	$scope.$on('subTransactionDeviceDataReady', function (event, data) {
		$scope.subtransactionsdevice = data;
		SubTransactionDevice.subtransactionsdevice = data;
		$scope.changeLoading()
	});*/

/*GET SUB TRANSACJTION SIM***************
	if(!$scope.subtransactionssim){
	 	$scope.subtransactionssim = SubTransactionSIM.loadSubTransactionSimData();
	}
	$scope.$on('subTransactionSimDataReady', function (event, data) {
		$scope.subtransactionssim = data;
		SubTransactionSIM.subtransactionssim = data;
		$scope.changeLoading()
	});*/

/** **/

	$scope.changeLoading = function(){	// sprawdź czy już można urkyć wizualizację ładowania
		if($scope.clients && $scope.operations && $scope.transactions /*&& $scope.subtransactionsdevice && $scope.subtransactionssim*/){
			$scope.initTable()
			$scope.pageLoading = false;
		}else{
			$scope.pageLoading = true;
		}
	}

	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id === id){
				return src[i]
			}
		}
	}


	$scope.customFilterParam ={
		oper1 : true,
		oper2: true,
		open: true,
		close: false,
		beginfrom: '',
		beginto: '',
		endfrom: '',
		endto: '',
	}

	$scope.duplicatedItem = function(arrays){
    	if(arrays.length <2){
    		return arrays[0]
    	}else{
	    	var firstarray = arrays[0]
	    	var secarray = arrays[1]
	    	var temp = []
	    	angular.forEach(firstarray, function(value, key) {
				if(secarray.indexOf(value) != -1){
					temp.push(value)
				}
			})
    		arrays[1] = temp
			arrays.splice(0, 1);
			return $scope.duplicatedItem(arrays)
    	}

    }

	$scope.getData = function(){ // przełączanie danych tabeli
		var returnedItems = $scope.transactions
		var oper1array = []
		var oper2array = []
		var openarray = []
		var cloaearray = []

		var datebeginfrom =  $scope.transactions
	 	var datebeginto=  $scope.transactions
	 	var dateendfrom =  $scope.transactions
	 	var dateendto=  $scope.transactions

		if($scope.customFilterParam.oper1){
 			oper1array = $filter('filter')($scope.transactions, {operation_id:1});
	 	}
	 	if($scope.customFilterParam.oper2){
	 		oper2array = $filter('filter')($scope.transactions, {operation_id:2});

	 	}

	 	if($scope.customFilterParam.open){
	 		openarray = $filter('filter')($scope.transactions, {is_open:true});
		}
		if($scope.customFilterParam.close){
		 	cloaearray = $filter('filter')($scope.transactions, {is_open:false});
		}

		if($scope.customFilterParam.beginfrom){datebeginfrom = [] }
		if($scope.customFilterParam.beginto){datebeginto = [] }
		if($scope.customFilterParam.endfrom){dateendfrom = [] }
		if($scope.customFilterParam.endto){dateendto = []}

 		if($scope.customFilterParam.beginfrom || $scope.customFilterParam.beginto || $scope.customFilterParam.endfrom || $scope.customFilterParam.endto){
	 		angular.forEach( $scope.transactions, function(value, key) {
				if($scope.customFilterParam.beginfrom){
				 	try{
				 		if(new Date(value.beginning) >= new Date($scope.customFilterParam.beginfrom)){
				 			datebeginfrom.push(value)
				 		}
				 	}
				 	catch(e){flash.show("Niewłaściwy format daty", "error")	}
				}
				if($scope.customFilterParam.beginto){
				 	try{
				 		if(new Date(value.beginning) <= new Date($scope.customFilterParam.beginto)){
				 			datebeginto.push(value)
				 		}
				 	}
				 	catch(e){flash.show("Niewłaściwy format daty", "error")	}
				}
				if($scope.customFilterParam.endfrom){
				 	try{
				 		if(new Date(value.ending) >= new Date($scope.customFilterParam.endfrom)){
				 			dateendfrom.push(value)
				 		}
				 	}
				 	catch(e){flash.show("Niewłaściwy format daty", "error")	}
				}

				if($scope.customFilterParam.endto){
				 	try{
				 		if(new Date(value.ending) <= new Date($scope.customFilterParam.endto)){
				 			dateendto.push(value)
				 		}

				 	}
				 	catch(e){flash.show("Niewłaściwy format daty", "error")	}
				}
			});
			returnedItems = $scope.duplicatedItem([ $scope.transactions, oper1array.concat(oper2array),  openarray.concat(cloaearray), datebeginfrom, datebeginto, dateendfrom, dateendto])
  		}else{
  			returnedItems = $scope.duplicatedItem([ $scope.transactions, oper1array.concat(oper2array),  openarray.concat(cloaearray)])
  		}

 		return returnedItems
    }

    $scope.initTable = function(){
	    $scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 50,          // count per page
	        sorting:{
	        	id: 'asc'
	        }
	    }, {
	        total:  $scope.getData().length, //$scope.transactions.length,     // length of data
	        getData: function($defer, params) {
	           // var filteredData = getData();
	            var filteredData = $filter('filter')( $scope.getData(), $scope.filter);  //getData()
	            var orderedData = params.sorting() ?
	                                $filter('orderBy')(filteredData, params.orderBy()) :
	                                filteredData;
	            params.total(orderedData.length);
	            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
	        },
	        //$scope: { $data: {} }
	    });
	    $scope.tableParams.settings().$scope = $scope;
	}

	$scope.changeLoading()

 	$scope.$watch("filter", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if($scope.transactions && $scope.clients  && $scope.operations && $scope.tableParams){
			$scope.tableParams.reload();
 		}
	}, true);

	$scope.$watch("customFilterParam", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
		if($scope.transactions && $scope.clients  && $scope.operations && $scope.tableParams){
			$scope.tableParams.reload();
 		}
	}, true);

});


app.filter('truncate', function (){
        return function (text, length, end) {
        	if(!text){
        		return
        	}
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                var temp = text;
                for(var i=parseInt(length); i>=0;i--){
            		 temp =  String(text).substring(0, length-end.length) + end;
            		if(temp.indexOf("<")==-1){
            			return temp
            		}
            		length--
            	}
            }
            else {
            	for(var i=parseInt(length); i>=0;i--){
            		var temp = String(text).substring(0, length-end.length) + end;
            		if(temp.indexOf("<")==-1){
            			return temp
            		}
            		length--
            	}
               // return String(text).substring(0, length-end.length) + end;
            }

        };
    });
