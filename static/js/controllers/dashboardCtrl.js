app.controller("dashboardCtrl", function ($scope, $q, $http, flash, $filter, side_menu, $dialogs, Clients, Users, Operations, Transaction, Session, USER_ROLES, AuthService) {
    "use strict";
    $scope.pageLoading = true;
    $scope.flash = flash;
    $scope.session = Session;
    $scope.clients = Clients.getClients();
    $scope.operations = Operations.getOperations();
    $scope.transactions = Transaction.getTransactions();
    $scope.showtable = false;


    $scope.users = Users.loadUsersData();
    $scope.$on('usersDataReady', function (data) {
        $scope.users = data;
        Users.users = data;
    });

    $scope.alltransaction = [];
    $scope.exfilter = function (trans) {
        var items = [], i = 0, item, temp, d, now, tomorrow;
        if (trans) {
            for (i = 0; i < trans.length; i += 1) {
                item = trans[i];
                temp = new Date(item.ending);
                d = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate(), 0, 0, 0, 0);
                temp = new Date();
                now = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate(), 0, 0, 0, 0);
                tomorrow = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate() + 1, 0, 0, 0, 0);


                if ((item.is_open) && (now.getTime() > d.getTime())) {
                    item.color = "red";
                    items.push(item);
                } else if ((item.is_open) && (now.getTime() == d.getTime())) {
                    item.color = "orange";
                    items.push(item);
                } else if ((item.is_open) && (tomorrow.getTime() == d.getTime())) {
                    item.color = "green";
                    items.push(item);
                }
            }
        }
        side_menu.a_home.span.content = items.length;
        if (items.length) {
            $scope.showtable = true;
        } else {
            $scope.showtable = false;
        }

        return items;
    };

/*GET CLIENTS ****************/
	if(!$scope.clients){
	 	$scope.clients = Clients.loadClientsData();
	}
	$scope.$on('clientsDataReady', function (event, data) {
		if($scope.transactions && $scope.transactions.length && !$scope.transactions[0].from){
			for(var i = 0; i<$scope.transactions.length;i++ ){
				$scope.transactions[i].from = $scope.getItem(data, $scope.transactions[i].from_id)
				$scope.transactions[i].to = $scope.getItem(data, $scope.transactions[i].to_id)
			}
		}
		$scope.clients = data;
		Clients.clients = data;

		$scope.changeLoading()
	});

/*GET OPERATION ****************/
	if(!$scope.operations){
	 	$scope.operations = Operations.loadOperationsData();
	}
	$scope.$on('operationsDataReady', function (event, data) {
		if($scope.transactions && $scope.transactions.length && !$scope.transactions[0].operation){

			for(var i = 0; i<$scope.transactions.length;i++ ){
				$scope.transactions[i].operation = $scope.getItem(data, $scope.transactions[i].operation_id)
			}
		}
		$scope.operations = data;
		Operations.operations = data;

		$scope.changeLoading()
	});


/*GET TRANSACJTION ****************/
	if(!$scope.transactions){
	 	$scope.transactions = Transaction.loadTransactionsData();
	}else{
		$scope.extransactions  = $scope.exfilter($scope.transactions)
	}
	$scope.$on('transactionsDataReady', function (event, data) {
		if($scope.clients){
			for(var i = 0; i<data.length;i++ ){
				data[i].from = $scope.getItem($scope.clients, data[i].from_id)
				data[i].to = $scope.getItem($scope.clients, data[i].to_id)
				if($scope.operations){

					data[i].operation = $scope.getItem($scope.operations, data[i].operation_id)
				}
			}
		}

		if($scope.operations && !data[0].operation){
			for(var i = 0; i<data.length;i++ ){
				data[i].operation = $scope.getItem($scope.operations, data[i].operation_id)
			}
		}

		$scope.alltransaction = data
		if(AuthService.isAuthorized([USER_ROLES.admin, USER_ROLES.manager])){
			$scope.transactions = data;
			Transaction.transactions = data;
		}else{
			if(Session.user){
				var to = $filter('filter')(data, {"to_id": Session.user.client_id}, true );
	 			var from = $filter('filter')(data, {"from_id": Session.user.client_id}, true );
				$scope.transactions = to.concat(from)
				Transaction.transactions = to.concat(from);
			}
		}

		$scope.extransactions  = $scope.exfilter($scope.transactions)
		$scope.changeLoading()
	});
//

	$scope.$watch("session", function(newVal, oldVal){
		if(!$scope.transactions && $scope.alltransaction && Session.user){
			var to = $filter('filter')($scope.alltransaction, {"to_id": Session.user.client_id}, true );
		 	var from = $filter('filter')($scope.alltransaction, {"from_id": Session.user.client_id}, true );
			$scope.transactions = to.concat(from)
			Transaction.transactions = to.concat(from);
		}
    }, true);

	$scope.changeLoading = function(){	// sprawdź czy już można urkyć wizualizację ładowania
		if($scope.clients && $scope.operations && $scope.transactions){
			$scope.pageLoading = false;
		}else{
			$scope.pageLoading = true;
		}
	}

	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id === id){
				return src[i]
			}
		}
	}

	$scope.changeLoading()
})
