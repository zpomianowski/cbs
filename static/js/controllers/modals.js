app.directive('dynamic', function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
});


/** TRANSACTION MODAL CONTROLLER**/
app.controller('transactionModalInstanceCtrl', function ($scope, $http, flash, $modal, $filter, $dialogs, $modalInstance, USER_ROLES, AuthService, transaction, SubTransactionDevice, SubTransactionSIM, Devices, SIM, Company, DevicesTypes, DevicesModels, DevicesStates, Clients, Operations, Comments, TransactionArchive, Session, Users ) {
  $scope.sendingEmail = false
  $scope.flash = flash
  $scope.transaction = transaction;

  $scope.USER_ROLES= USER_ROLES
  $scope.isAuthorized = AuthService.isAuthorized;

  $scope.newsettings = {
    newending: angular.copy($scope.transaction.ending),
    newsendnotification: angular.copy($scope.transaction.notification_email)
  }

  $scope.session = Session;
  $scope.users = Users.getUsers()

  /* GET ELEMENTS BY TRANSACTION_ID */
    $scope.getItemsForTransaction = function(src, id ){
      var items  =[]
      for(var i = 0; i<src.length;i++ ){
        if(src[i].transaction_id == id){
          items.push(src[i])
        }
      }
      return items
    }
  //

  /** EDIT RESOURCE MODAL **/
    $scope.openResourceModal = function (item) {
      var modalInstance = $modal.open({
          templateUrl: 'resourceModalContent.html',
          controller: 'resourceModalInstanceCtrl',
          size: 100,
          resolve: {
            res: function () {
              return item
            }
          }
      });
    modalInstance.result.then(function (){}, function (){} );
  };

  $scope.subtransactionsdevice = SubTransactionDevice.getSubTransactionsDevice()
  $scope.subTRdeviceThis = []
  $scope.subtransactionssim = SubTransactionSIM.getSubTransactionsSim()
  $scope.subTRsimThis = []

  $scope.devices = Devices.getDevices()
  $scope.devicetypes = DevicesTypes.getDeviceTypes()
  $scope.devicestates = DevicesStates.getDeviceState()
  $scope.devicemodels = DevicesModels.getDeviceModels()
  $scope.company = Company.getCompany()

  $scope.operations = Operations.getOperations()
  $scope.sim = SIM.getSim()

  $scope.clients = Clients.getClients();

  $scope.comments = Comments.getComments();
  $scope.commentsTRthis = []

  $scope.transactionarchive = TransactionArchive.getTransactionArchive();
  $scope.transactionarchiveTRthis = []




  /*GET CLIENTS ****************/
    if(!$scope.clients){
      $scope.clients = Clients.loadClientsData();
    }
    $scope.$on('clientsDataReady', function (event, data) {

      if($scope.devices && !$scope.devices[0].owner){
        for(var i =0; i<$scope.devices.length;i++){
          $scope.devices[i].holder = $scope.getItem(data,  $scope.devices[i].holder_id)
          $scope.devices[i].owner = $scope.getItem(data,  $scope.devices[i].owner_id)
        }
      }
      if($scope.sim && !$scope.sim[0].owner){
        for(var i =0; i<$scope.sim.length;i++){
          $scope.sim[i].holder = $scope.getItem(data,  $scope.sim[i].holder_id)
          $scope.sim[i].owner = $scope.getItem(data,  $scope.sim[i].owner_id)
        }
      }
      if($scope.comments && !$scope.comments[0].author){
         for(var i =0; i<$scope.comments.length;i++){
          $scope.comments[i].author = $scope.getItem(data,  $scope.comments[i].author_id)
        }
      }
      $scope.clients = data;
      Clients.clients = data;
    });
  //

  $scope.getArchive= function(){
    if($scope.transactionarchiveTRthis.length==0){
       var temp = []
       temp = $filter('filter')($scope.transactionarchive, {current_record:$scope.transaction.id }, true)

        if(!temp || (temp.length == 0)){
          $http.post("getTransactionArchive", {'transaction_id': $scope.transaction.id })
            .success(function(data) {
               $scope.transactionarchiveTRthis = data
               if($scope.transactionarchive){
                $scope.transactionarchive =  $scope.transactionarchive.concat(data);

                TransactionArchive.transactionarchive =  TransactionArchive.transactionarchive.concat(data);

               }else{
                $scope.transactionarchive = data;
                TransactionArchive.transactionarchive = data
               }
            })
            .error(function(err){
              flash.show("Wystąpił błąd podczas pobierania historii!", "error")
            })
        }else{
          $scope.transactionarchiveTRthis = temp;
        }
    }
  }

  /* GET SUB TRANSACTION DEVICE****************/
    if(!$scope.subtransactionsdevice){
      $scope.subtransactionsdevice = SubTransactionDevice.loadSubTransactionDeviceData()
    }else{
      $scope.subTRdeviceThis = $scope.getItemsForTransaction($scope.subtransactionsdevice, $scope.transaction.id)
    }
    $scope.$on('subTransactionDeviceDataReady', function (event, data) {
      $scope.subTRdeviceThis = $scope.getItemsForTransaction(data, $scope.transaction.id)
      $scope.subtransactionsdevice = data;
      SubTransactionDevice.subtransactionsdevice = data
    });

  /*GET SUB TRANSACTION SIM ****************/
    if(!$scope.subtransactionssim){
      $scope.subtransactionssim = SubTransactionSIM.loadSubTransactionSimData()
    }else{
      $scope.subTRsimThis = $scope.getItemsForTransaction($scope.subtransactionssim, $scope.transaction.id)
    }
    $scope.$on('subTransactionSimDataReady', function (event, data) {
      $scope.subTRsimThis = $scope.getItemsForTransaction(data, $scope.transaction.id)
      $scope.subtransactionssim = data;
      SubTransactionSIM.subtransactionssim = data
    });

  /*GET DEVICES TYPES ****************/
    if(!$scope.devicetypes){
      $scope.devicetypes = DevicesTypes.loadDeviceTypesData();
    }
    $scope.$on('deviceTypesDataReady', function (event, data) {
      if($scope.devices && !$scope.devices[0].type){
        for(var i =0; i<$scope.devices.length;i++){
          $scope.devices[i].type = $scope.getItem(data,  $scope.devices[i].type_id)
        }
      }
      $scope.devicetypes = data;
      DevicesTypes.devicetypes = data;
    });

  /*GET DEVICES MODELS ****************/
    if(!$scope.devicemodels){
      $scope.devicemodels = DevicesModels.loadDeviceModelsData();
    }
    $scope.$on('deviceModelsDataReady', function (event, data) {
      if($scope.devices && !$scope.devices[0].model){
        for(var i =0; i<$scope.devices.length;i++){
          $scope.devices[i].model = $scope.getItem(data,  $scope.devices[i].model_id)
        }
      }
      $scope.devicemodels = data;
      DevicesModels.devicemodels = data;
    });

  /*GET DEVICES STATES ****************/
    if(!$scope.devicestates){
      $scope.devicestates = DevicesStates.loadDeviceStateData();
    }
    $scope.$on('deviceStatesDataReady', function (event, data) {
      if($scope.devices && !$scope.devices[0].state){
        for(var i =0; i<$scope.devices.length;i++){
          $scope.devices[i].state = $scope.getItem(data,  $scope.devices[i].state_id)
        }
      }
      if($scope.sim && !$scope.sim[0].state){
        for(var i =0; i<$scope.sim.length;i++){
          $scope.sim[i].state = $scope.getItem(data,  $scope.sim[i].state_id)
        }
      }
      $scope.devicestates = data;
      DevicesStates.devicestates = data;
    });

  /*GET COMPANY ****************/
    if(!$scope.company){
      $scope.company = Company.loadCompanData();
    }
    $scope.$on('companyDataReady', function (event, data) {
      if($scope.devices && !$scope.devices[0].company){
        for(var i =0; i<$scope.devices.length;i++){
          $scope.devices[i].company = $scope.getItem(data,  $scope.devices[i].company_id)
        }
      }
      if($scope.sim && !$scope.sim[0].company){
        for(var i =0; i<$scope.sim.length;i++){
          $scope.sim[i].company = $scope.getItem(data,  $scope.sim[i].company_id)
        }
      }
      $scope.company = data;
      Company.company = data;
    });

  /*GET DEVICES ****************/
    if(!$scope.devices){
      $scope.devices = Devices.loadDevicesData()
    }

    $scope.$on('devicesDataReady', function (event, data) {
      if($scope.devicetypes && $scope.devicemodels && $scope.devicestates && $scope.company && $scope.clients){
        for(var i = 0; i<data.length;i++ ){
          data[i].type = $scope.getItem($scope.devicetypes, data[i].type_id)
          data[i].model = $scope.getItem($scope.devicemodels, data[i].model_id)
          data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)
          data[i].company = $scope.getItem($scope.company, data[i].company_id)
          data[i].holder = $scope.getItem($scope.clients, data[i].holder_id)
          data[i].owner = $scope.getItem($scope.clients, data[i].owner_id)
          if(!data[i].sn){data[i].sn=""}        // jak był null to chowałe je przy filtrowniu
          if(!data[i].imei){data[i].imei=""}
          if(!data[i].mac){data[i].mac=""}
        }
      }
      $scope.devices = data ;
      Devices.devices = data;
    });

  /*GET SIM ****************/
    if(!$scope.sim){
      $scope.sim = SIM.loadSimData();
    }

    $scope.$on('simsDataReady', function (event, data) {
      if($scope.company && $scope.devicestates && $scope.clients){
        for(var i = 0; i<data.length;i++ ){
          data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)
          data[i].company = $scope.getItem($scope.company, data[i].company_id)
          data[i].holder = $scope.getItem($scope.clients, data[i].holder_id)
          data[i].owner = $scope.getItem($scope.clients, data[i].owner_id)
          if(!data[i].pin){data[i].pin=""}
          if(!data[i].pin2){data[i].pin2=""}
          if(!data[i].puk){data[i].puk=""}
          if(!data[i].puk2){data[i].puk2=""}
          if(!data[i].imsi){data[i].imsi=""}
          if(!data[i].iccid){data[i].iccid=""}
          if(!data[i].msisdn){data[i].msisdn=""}
        }
      }
      $scope.sim = data;
      SIM.sim = data;
    });

  /*GET COMMENTS ****************/
    if(!$scope.comments){
      $scope.comments = Comments.loadCommentsData();
    }else{
       $scope.commentsTRthis = $scope.getItemsForTransaction($scope.comments, $scope.transaction.id)
    }

    $scope.$on('commentsDataReady', function (event, data) {
      if( $scope.clients){
        for(var i = 0; i<data.length;i++ ){
          data[i].author = $scope.getItem($scope.clients, data[i].author_id)
        }
      }
      $scope.commentsTRthis = $scope.getItemsForTransaction(data, $scope.transaction.id)
      $scope.comments = data;
      Comments.comments = data;
    });


  /** GET ITEM BY ID **/
    $scope.getItem = function(src, id){ // zwraca element o określonym id
      for(var i = 0; i<src.length;i++ ){
        if(src[i].id == id){
          return src[i]
        }
      }
    }

  /*RETURN DEVICE AND SIM  ****************/
    $scope.returnItem = function(item){

      var newData = {
        returned: true,
        return_date: $filter('date')(new Date(), 'yyyy-MM-dd')
      }

      if(item.device_id){
        var table = 'cbs_subtransaction_device'
      }else{
        var table = 'cbs_subtransaction_sim'
      }

      var newSupplayHolder = {}
      var supplayItem = {}
      var supplyTable = ""

      var supl_id = 0
      var supl_property = ""
      var supl_array = []
      /*
              if(item.device_id){
          supplyTable = 'cbs_device'
          supplayItem = $scope.getItem($scope.devices, item.device_id)
          supl_id = item.device_id
          var numSubTran = $filter('filter')($scope.subtransactionsdevice, {"returned": false,"device_id": item.device_id}, true)  // sprawdzamy czy to urządzenie jest kolejnym wyporzyczeniem
          if(numSubTran.length>1){ // więcej transakcji
             newSupplayHolder = $scope.transaction.from
          }else{ // bezpośrednie wyporzyczenie - jedna transakcja
            var owner = $scope.getItem($scope.devices, item.device_id).owner
            if(owner.user_id || (owner.id==Session.user.LAB)){
              newSupplayHolder = owner
            }else{
              newSupplayHolder = $scope.transaction.from
            }
          }

        }else{
          supplyTable = 'cbs_sim'
          supplayItem = $scope.getItem($scope.sim, item.sim_id)
          var numSubTran =$filter('filter')($scope.subtransactionssim, {"returned": false, "sim_id": item.sim_id }, true)  // sprawdzamy czy to urządzenie jest kolejnym wyporzyczeniem
          supl_id = item.sim_id
          if(numSubTran.length>1){ // więcej transakcji
             newSupplayHolder = $scope.transaction.from
          }else{ // bezpośrednie wyporzyczenie
            var owner = $scope.getItem($scope.sim, item.sim_id).owner
            if(owner.user_id || (owner.id==Session.user.LAB)){
              newSupplayHolder =owner
            }else{
              newSupplayHolder = $scope.transaction.from
            }
          }
        }

      */
      if(item.device_id){
        supplyTable = 'cbs_device'
        supl_id = item.device_id
        supl_property = "device_id"
        supl_array = $scope.devices
      }else{
        supplyTable = 'cbs_sim'
        supl_id = item.sim_id
        supl_property = "sim_id"
        supl_array = $scope.sim
       }




      //0000000000000000000000000000000000000000000000000000000000 EDYCJA PODTRANSAKCJI
        var devsArch = $http.get("getDeviceArchive.py", {params:{"device_id":supl_id}} ).success(function(data){ return data })

        devsArch.then(function(result){
          supplayItem = $scope.getItem(supl_array, supl_id)
          var numSubTran = $filter('filter')($scope.subtransactionsdevice, {"returned": false,"device_id": supl_id}, true)  // sprawdzamy czy to urządzenie jest kolejnym wyporzyczeniem

          if(numSubTran.length>1){ // więcej transakcji
            newSupplayHolder = $scope.transaction.from
          }else{ // bezpośrednie wyporzyczenie - jedna transakcja
            if(result.data && result.data.length){
              var dev_arch = $filter('orderBy')(result.data, "id", true)[0]
              newSupplayHolder = $filter('filter')($scope.clients, {"id": dev_arch.holder_id}, true)[0]
            }else{
              newSupplayHolder = $scope.transaction.from
            }
          }

          $http.post("editRecord.py", {'id': item.id, 'table': table, 'data':newData  })
            .success(function(data){
              item.returned = true
              item.return_date = $filter('date')(new Date(), 'yyyy-MM-dd')
              var close = true
              for(var i =0; i<$scope.subTRdeviceThis.length; i++){
                if($scope.subTRdeviceThis[i].returned){ continue }
                close = false
              }
              for(var i =0; i<$scope.subTRsimThis.length; i++){
                if($scope.subTRsimThis[i].returned){ continue }
                close = false
              }

              /// EDYCJA URZĄDZENIA
              $http.post("editRecord.py", {'table': supplyTable, 'data':{holder_id: newSupplayHolder.id}, 'id': supplayItem.id})
                  .success(function(data){
                     supplayItem.holder_id = newSupplayHolder.id
                     supplayItem.holder = newSupplayHolder
                  })
                  .error(function(err){
                    flash.show("Wystąpił błąd podczas zwracania urządzenia!", "error")
                  })

              if(close){ // ZAMKNIĘCIE TRANSAKCJI gdy zwrócono wszystkie zasowby
                $http.post("editRecord.py", {'id': $scope.transaction.id, 'table': 'cbs_transaction', 'data':{is_open: false}  })
                  .success(function(data){
                    $scope.transaction.is_open = false
                    ////// zwrócono EMAIL
                    if( $scope.transaction.notification_email){
                      $scope.sendEmail(false,true, "closeTransaction")
                    }

                    flash.show("Zwrócono wszystkie urządzenia", "success")
                  })
              }else{
                flash.show("Zwrócono urządzenie", "success")
              }
            })
            .error(function(err){
              flash.show("Wystąpił błąd podczas zwrotu urządzenia!", "error")
            })


        });



    }

  /* WATCH IS_OPEN  ****************/
    $scope.delayedreturn = false
    $scope.$watch("transaction.is_open", function(newVal, oldVal){
      if($scope.transaction.is_open && (new Date($scope.transaction.ending) < new Date())){
         $scope.delayedreturn = true
      }else{
         $scope.delayedreturn = false
      }
    }, true);

  /* ADD COMMENT  ****************/
    $scope.addComment = function(newComment){
      newData = {
        author_id: Session.client.id,
        description: newComment,
        posted:  $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
        transaction_id: $scope.transaction.id
      }
      $http.post("addRecord.py", { 'table': "cbs_comment", 'data':newData  })
        .success(function(data) {
          newData.author = Session.client
          $scope.commentsTRthis.push(newData)
          $scope.comments.push(newData)
          flash.show("Dodano komentarz", "success")
        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas dodawania komentarza!", "error")
        })
    }

    $scope.cancel = function(){
      $modalInstance.dismiss('cancel');
    };


  // FILE /////////////////////////////////////////////////////
    $scope.newfilename = ""
    $scope.clearnewfile = function(){
       $scope.newfilename = ""
    }

    $scope.getFileInfo = function(){
      $scope.fileLink = ''
      $http.post("getFile.py", {'id': $scope.transaction.report})
        .success(function(data) {
          $scope.newfile.id = data[0].id
          $scope.newfile.title = data[0].title
          $scope.newfile.size = data[0].size
          $scope.newfile.type = data[0].type

          $scope.fileLink = data[0].link

        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas pobierania załącznika!", "error")
        })
    }

    $scope.newfile = {}
    $scope.fileLink = false
    if($scope.transaction.report){
       $scope.getFileInfo()
    }

    $scope.setFiles = function(element){
      $scope.$apply(function($scope){
        $scope.newfile = element.files[0]
        $scope.newfilename = $scope.newfile.name.toString()
        $scope.progressVisible = false
      });
    };

    $scope.uploadFile = function() {
          var fd = new FormData()

          fd.append("testfile", $scope.newfile)
          fd.append("filesize", $scope.newfile.size)
          fd.append("filetype", $scope.newfile.type)

          var xhr = new XMLHttpRequest()
          xhr.upload.addEventListener("progress", uploadProgress, false)
          xhr.addEventListener("load", uploadComplete, false)
          xhr.addEventListener("error", uploadFailed, false)
          xhr.addEventListener("abort", uploadCanceled, false)
          xhr.open("POST", "addFile")
          $scope.progressVisible = true
          xhr.send(fd)
      }

      function uploadProgress(evt) {
          $scope.$apply(function(){
              if (evt.lengthComputable) {
                  $scope.progress = Math.round(evt.loaded * 100 / evt.total)
              } else {
                  $scope.progress = 'unable to compute'
              }
          })
      }

      function uploadComplete(evt) {
        if(parseInt(evt.target.response)>0){
          $http.post("editRecord.py", {'id': $scope.transaction.id, 'table': 'cbs_transaction', 'data':{report: evt.target.response}})
            .success(function(data) {
               $scope.transaction.report = parseInt(evt.target.response)
               $scope.getFileInfo()
               flash.show("Dodano załącznik", "success")
            })
            .error(function(err){
              flash.show("Wystąpił błąd podczas aktualizacji danych transakcji (załącznik)!", "error")
            })
        }
      }

      function uploadFailed(evt) {
          flash.show("Wystąpił błąd podczas dodawania załącznika!", "error")
      }

      function uploadCanceled(evt) {
          $scope.$apply(function(){
              $scope.progressVisible = false
          })
          flash.show("Przerwano.", "info")
      }
  //

  $scope.saveChange = function(){
    if($scope.newfilename && $scope.newfile){
      $scope.uploadFile()
      $scope.newfilename = ""
      $scope.newfile = {}

    }

    var neeewww = $filter('date')(new Date($scope.newsettings.newending), 'yyyy-MM-dd').toString()
    var newDate = {}

    if(neeewww != $scope.transaction.ending){
      newDate.ending =  $filter('date')(new Date($scope.newsettings.newending), 'yyyy-MM-dd')
    }

    if($scope.newsettings.newsendnotification != $scope.transaction.notification_email){
      newDate.notification_email = $scope.newsettings.newsendnotification
    }

    if(('ending' in newDate) || ('notification_email' in newDate)){

      $http.post("editRecord.py", {'id': $scope.transaction.id, 'table': 'cbs_transaction', 'data': newDate})
        .success(function(data) {
          $scope.transaction.ending = $filter('date')(new Date($scope.newsettings.newending), 'yyyy-MM-dd')
          if($scope.transaction.is_open && (new Date($scope.transaction.ending) < new Date())){
             $scope.delayedreturn = true

          }else{
             $scope.delayedreturn = false

          }

          $scope.transaction.notification_email = $scope.newsettings.newsendnotification

          if( $scope.transaction.notification_email  && ('ending' in newDate)){ // e
               $scope.sendEmail(false,true, "transactionChangeEnd")
          }

          flash.show("Zmodyfikowano transakcję!", "success")
        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas modyfikacji transakcji!", "error")
        })
    }
  }

  $scope.deleteFile = function(){
    var dlg = null;
    dlg = $dialogs.confirm('Usuń załącznik','Czy chcesz usunąć załącznik?');
    dlg.result.then(function(btn){
      $http.post("delRecord.py", {'id': $scope.newfile.id, 'table': 'cbs_attachments'})
        .success(function(data) {
          $scope.fileLink = ""
          $http.post("editRecord.py", {'id': $scope.transaction.id, 'table': 'cbs_transaction', 'data': {report: ''}})
            .success(function(data){
              $scope.newfile = {}
              $scope.transaction.report = ""
              flash.show("Usunięto załącznik!", "success")
            })
            .error(function(err){
              flash.show("Usunięto plik ale nie zaktualizowano transakcji!", "error")
            })
        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas usuwania pliku!", "error")
        })
    },function(btn){ });
  }


  $scope.sendEmail = function(asking,alldev, type){
    var send = function(asking,alldev, type){
      $scope.sendingEmail = true
      var devidlist = "0_"
      for(var i =0; i<$scope.subTRdeviceThis.length;i++ ){
        if(alldev){
          devidlist += $scope.subTRdeviceThis[i].device_id+"_"
        }else{
          if(!$scope.subTRdeviceThis[i].returned){
            devidlist += $scope.subTRdeviceThis[i].device_id+"_"
          }
        }

      }
      devidlist = devidlist.substring(0, devidlist.length - 1);

      var simidlist  = "0_"
      for(var i =0; i<$scope.subTRsimThis.length;i++ ){
        if(alldev){
          simidlist += $scope.subTRsimThis[i].sim_id+"_"
        }else{
          if(!$scope.subTRsimThis[i].returned){
            simidlist += $scope.subTRsimThis[i].sim_id+"_"
          }
        }

      }
      simidlist = simidlist.substring(0, simidlist.length - 1);

      if(new Date($scope.transaction.ending) < new Date()){
        var expire = "true"
      }else{
        var expire = ""
      }

      var  emaildata ={
        "fromwho": $scope.transaction.from.firstname+" "+$scope.transaction.from.lastname,
        'fromemail':$scope.transaction.from.email,
        'fromcompany':$scope.transaction.from.company.name,
        'fromphone':$scope.transaction.from.phone,


        "towho":$scope.transaction.to.firstname+" "+$scope.transaction.to.lastname,
        'toemail':$scope.transaction.to.email,
        'tocompany':$scope.transaction.to.company.name,
        'tophone':$scope.transaction.to.phone,

        "trid": $scope.transaction.id,
        "beginning": $scope.transaction.beginning,
        "ending": $scope.transaction.ending,

        "expire" :expire,
        "msgtype":type ,
        "dev":devidlist,
        "sim":simidlist,
      }
      $http.post("sendEmail.py", emaildata)
        .success(function(data){
            $scope.sendingEmail = false
            if(data.toString()=="True"){
              if(asking){
                flash.show("Wysłano powiadomienie.", "success")
              }
            }else{
               flash.show("Nie wysłano powiadomnienia !", "info")
            }

        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas wysyłania emaila!", "error")
        })
    }
    if(asking){
      var dlg = null;
        dlg = $dialogs.confirm('Wysłać email','Czy chcesz wysłać email z przypomnieniem?');
        dlg.result.then(function(btn){
          send(asking,alldev, type)
      },function(btn){ });
    }else{
      send(asking,alldev, type)
    }

  }

  $scope.generateRaport = function(){
    flash.show("test", "error")

  }
})


/** RESOURCE MODAL CONTROLLER**/
app.controller('resourceModalInstanceCtrl', function ($scope, $http, $timeout, Users, flash, $filter, SubTransactionDevice, SubTransactionSIM, $dialogs, $modal, USER_ROLES, AuthService, $modalInstance, res, DeviceArchive, SimArchive, Company, DevicesTypes, DevicesModels, DevicesStates, Clients, Session) {
  $scope.resource = res;
  $scope.session = Session
  $scope.toogleEdit = false;
  $scope.newResource = angular.copy($scope.resource)
  $scope.issim = false;


  $scope.USER_ROLES= USER_ROLES
  $scope.isAuthorized = AuthService.isAuthorized;

  if($scope.resource.hasOwnProperty('pin')){
    $scope.issim = true
    $scope.simarchive = SimArchive.getSimArchive()
    $scope.simarchiveThis = []
  }else{
    $scope.issim = false
    $scope.devicearchive = DeviceArchive.getDeviceArchive()
    $scope.devicearchiveThis = []
  }

  $scope.devicetypes = DevicesTypes.getDeviceTypes()
  $scope.devicemodels = DevicesModels.getDeviceModels()
  $scope.devicestates = DevicesStates.getDeviceState()
  $scope.company = Company.getCompany()
  $scope.clients = Clients.getClients();

  $scope.users = Users.getUsers()

  $scope.borrowed = false

  if($scope.issim){
     $scope.subtransactionssim = SubTransactionSIM.getSubTransactionsSim()

     var open_sub_ts = $filter('filter')($scope.subtransactionssim , {returned: false, sim_id:$scope.resource.id}, true)
      if(open_sub_ts && (open_sub_ts.length >0)){
        $scope.borrowed = true
      }else{
        $scope.borrowed = false
      }
  }else{
    $scope.subtransactionsdevice = SubTransactionDevice.getSubTransactionsDevice()
    if($scope.subtransactionsdevice){

      var open_sub_td = $filter('filter')($scope.subtransactionsdevice, {returned: false, device_id:$scope.resource.id}, true)

      if(open_sub_td.length > 0){
        $scope.borrowed = true
      }else{
        $scope.borrowed =false
      }
    }

  }

  /* GET SUB TRANSACTION DEVICE****************/
   if(!$scope.issim &&!$scope.subtransactionsdevice){

      $scope.subtransactionsdevice = SubTransactionDevice.loadSubTransactionDeviceData()

    }
    $scope.$on('subTransactionDeviceDataReady', function (event, data) {
      $scope.subtransactionsdevice = data;
      SubTransactionDevice.subtransactionsdevice = data
     var  open_sub_tr = $filter('filter')(data, {returned: false, device_id:$scope.resource.id}, true)

      if(open_sub_tr.length >0){
        $scope.borrowed =true
      }else{
        $scope.borrowed =false
      }
    });

    /*GET SUB TRANSACTION SIM ****************/
    if($scope.issim && !$scope.subtransactionssim){
      $scope.subtransactionssim = SubTransactionSIM.loadSubTransactionSimData()

    }
    $scope.$on('subTransactionSimDataReady', function (event, data) {
      $scope.subtransactionssim = data;
      SubTransactionSIM.subtransactionssim = data
      var open_sub_tr = $filter('filter')(data, {returned: false, sim_id:$scope.resource.id}, true)
      if(open_sub_tr.length >0){
        $scope.borrowed = true
      }else{
        $scope.borrowed = false
      }
  });




  //$scope.devices =  Devices.getDevices()

  /** FAS TRANSACTION  **/
    $scope.openFastTransactionModal = function (item) {
      var modalInstance = $modal.open({
          templateUrl: 'fastTransactionModalContent.html',
          controller: 'fastTransactionModalInstanceCtrl',
          size: 100,
          resolve: {
            item: function () {
              return item
            }
          }
      });
      modalInstance.result.then(function (){}, function (){} );
    };

  $scope.getItem = function(src, id){ // zwraca element o określonym id
    for(var i = 0; i<src.length;i++ ){
      if(src[i].id == id){
        return src[i]
      }
    }
  }

  $scope.autor = $scope.getItem( $scope.users, $scope.resource.created_by)


  $scope.getSimArchive= function(){
    if($scope.simarchiveThis.length==0){
       var temp = []
       temp = $filter('filter')($scope.simarchive, {current_record:$scope.resource.id }, true)

        if(!temp || (temp.length == 0)){
          $http.post("getSimArchive", {'sim_id': $scope.resource.id })
            .success(function(data) {
              for(var i=0; i<data.length ;i++){
                data[i].company = $filter('filter')($scope.company, {id: data[i].company_id}, true)[0]
                data[i].state = $filter('filter')($scope.devicestates, {id: data[i].state_id}, true)[0]
                data[i].holder = $filter('filter')($scope.clients, {id: data[i].holder_id}, true)[0]
                data[i].owner = $filter('filter')($scope.clients, {id: data[i].owner_id}, true)[0]
              }
              $scope.simarchiveThis = data
              if($scope.simarchive){
                $scope.simarchive =  $scope.simarchive.concat(data);
                SimArchive.simarchive =  SimArchive.simarchive.concat(data);

              }else{
                $scope.simarchive = data;
                SimArchive.simarchive = data
               }
            })
            .error(function(err){
              flash.show("Wystąpił błąd podczas pobierania historii!", "error")
            })
        }else{
          $scope.simarchiveThis = temp;
        }


    }
  }


  $scope.getDevArchive= function(){
    if($scope.devicearchiveThis.length==0){
       var temp = []
       temp = $filter('filter')($scope.devicearchive, {current_record:$scope.resource.id }, true)

        if(!temp || (temp.length == 0)){
          $http.post("getDeviceArchive", {'device_id': $scope.resource.id })
            .success(function(data) {
              for(var i=0; i<data.length ;i++){
                data[i].company = $filter('filter')($scope.company, {id: data[i].company_id}, true)[0]
                data[i].model = $filter('filter')($scope.devicemodels, {id: data[i].model_id}, true)[0]
                data[i].type = $filter('filter')($scope.devicetypes, {id: data[i].type_id}, true)[0]
                data[i].state = $filter('filter')($scope.devicestates, {id: data[i].state_id}, true)[0]
                data[i].holder = $filter('filter')($scope.clients, {id: data[i].holder_id}, true)[0]
                data[i].owner = $filter('filter')($scope.clients, {id: data[i].owner_id}, true)[0]

              }
              $scope.devicearchiveThis = data
              if($scope.devicearchive){
                $scope.devicearchive =  $scope.devicearchive.concat(data);

                DeviceArchive.devicearchive =  DeviceArchive.devicearchive.concat(data);

              }else{
                $scope.devicearchive = data;
                DeviceArchive.devicearchive = data
              }
            })
            .error(function(err){
              flash.show("Wystąpił błąd podczas pobierania historii!", "error")
            })
        }else{
          $scope.devicearchiveThis = temp;
        }
    }
  }

  $scope.getArchive =function(){
    if($scope.issim){
      $scope.getSimArchive()
    }else{
      $scope.getDevArchive()
    }
  }

  $scope.owner = $scope.resource.owner
  $scope.ownerText =  $scope.resource.owner.firstname+' '+ $scope.resource.owner.lastname

  $scope.$watch("ownerText", function(newVal, oldVal){
    if(newVal && (typeof newVal == "object")){
        $scope.ownerText = (newVal.firstname.toString()+' '+newVal.lastname.toString())
        $scope.owner = angular.copy(newVal);
    }
  }, true);

  $scope.holder = $scope.resource.holder
  $scope.holderText =  $scope.resource.holder.firstname+' '+ $scope.resource.holder.lastname

  $scope.$watch("holderText", function(newVal, oldVal){

    if(newVal && (typeof newVal == "object")){
        $scope.holderText = (newVal.firstname.toString()+' '+newVal.lastname.toString())
        $scope.holder = angular.copy(newVal);
    }
  }, true);

  $scope.displaytext = {
    company: $scope.resource.company.name,
   // model: $scope.resource.model.name,
  }

  if(!$scope.issim){
    $scope.displaytext.model = $scope.resource.model.name
  }

  $scope.$watch("displaytext.company", function(newVal, oldVal){
    if(newVal && (typeof newVal == "object")){
      $scope.newResource.company = newVal
      $scope.displaytext.company =  newVal.name
    }
  }, true);


  $scope.$watch("displaytext.model", function(newVal, oldVal){
    if(newVal && (typeof newVal == "object")){
      $scope.newResource.model = newVal
      $scope.displaytext.model =  newVal.name
    }
  }, true);



  $scope.cancelEdit = function () {
    $scope.newResource = angular.copy($scope.resource)
    //$scope.resource  = $scope.newResource

    $scope.model = $scope.newResource.model

    if(!$scope.issim){
      $scope.displaytext.model =  $scope.newResource.model.name
    }

    $scope.company = $scope.newResource.company
    $scope.displaytext.company =  $scope.newResource.company.name

    $scope.owner = $scope.newResource.owner
    $scope.ownerText =  $scope.newResource.owner.firstname+' '+ $scope.newResource.owner.lastname
    $scope.holder = $scope.newResource.holder
    $scope.holderText =  $scope.newResource.holder.firstname+' '+ $scope.newResource.holder.lastname
  };



  $scope.sendEmail = function(){  //resourceChenged
      var devidlist =""
      var simidlist = ""
      if($scope.issim){
        simidlist =  $scope.resource.id
      }else{
        devidlist = $scope.resource.id
      }

      var  emaildata ={
        "dev":devidlist,
        "sim":simidlist,
      }
      $http.post("sendChangedEmail.py", emaildata)
        .success(function(data){
            if(data.toString()!="True"){
               flash.show("Nie wysłano powiadomnienia !", "info")
            }

        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas wysyłania emaila!", "error")
        })


  }


  $scope.finddifferences =function(array1, array2){
    var temp = {}
    angular.forEach(array1, function(value, key){
      if(!angular.equals(value, array2[key])){
        temp[key] = value
      }
    });
    return temp
  }

  $scope.isEmpty  = function(map) {
    for(var key in map) {
      if (map.hasOwnProperty(key)) {
         return false;
      }
    }
    return true;
  }

  $scope.save = function (){
    //$scope.resource = $scope.newResource
    $scope.newResource.holder = $scope.holder;
    $scope.newResource.owner = $scope.owner;

    var differences = $scope.finddifferences($scope.newResource, $scope.resource)

    var temp = $scope.owner.firstname+' '+ $scope.owner.lastname
    if(temp != $scope.ownerText){
      $scope.ownerText = ""
    }
    temp = $scope.holder.firstname+' '+ $scope.holder.lastname
    if(temp != $scope.holderText){
      $scope.holderText = ""
    }

    if(($scope.holderText == "") || ($scope.ownerText == "")){
      flash.show("Wypełnij dane poprawnie", "info")
    }else{
      if($scope.isEmpty(differences)){
        flash.show("Nie wprowadzono żadnych modyfikacji danych", "info")
        return
      }
      var newData = {
        company_id: $scope.newResource.company.id,
        state_id: $scope.newResource.state.id,
        holder_id: $scope.holder.id,
        owner_id: $scope.owner.id,
        description : $scope.newResource.description
      }

      if($scope.issim){
        var table = "cbs_sim"
        newData.iccid = $scope.newResource.iccid
        newData.imsi = $scope.newResource.imsi
        newData.msisdn = $scope.newResource.msisdn
        newData.pin = $scope.newResource.pin
        newData.pin2 = $scope.newResource.pin2
        newData.puk = $scope.newResource.puk
        newData.puk2 = $scope.newResource.puk2
      }else{
        var table = "cbs_device"
          newData.imei = $scope.newResource.imei
          newData.mac = $scope.newResource.mac
          newData.sn = $scope.newResource.sn
          newData.model_id = $scope.newResource.model.id;
          newData.type_id = $scope.newResource.type.id;
      }

      $http.post("editRecord.py", { 'id' :  $scope.resource.id , 'table': table, data:newData})
        .success(function(data){
          $scope.resource.holder = $scope.holder
          $scope.resource.holder_id = $scope.holder.id
          $scope.resource.owner = $scope.owner
          $scope.resource.owner_id = $scope.owner.id
          $scope.resource.company = $scope.newResource.company
          $scope.resource.company_id = $scope.newResource.company.id;
          $scope.resource.state = $scope.newResource.state
          $scope.resource.state_id = $scope.newResource.state.id;
          $scope.resource.description = $scope.newResource.description
          if($scope.issim){
            $scope.resource.iccid = $scope.newResource.iccid
            $scope.resource.imsi = $scope.newResource.imsi
            $scope.resource.msisdn = $scope.newResource.msisdn
            $scope.resource.pin = $scope.newResource.pin
            $scope.resource.pin2 = $scope.newResource.pin2
            $scope.resource.puk = $scope.newResource.puk
            $scope.resource.puk2 = $scope.newResource.puk2
          }else{
            $scope.resource.imei = $scope.newResource.imei
            $scope.resource.mac = $scope.newResource.mac
            $scope.resource.sn = $scope.newResource.sn
            $scope.resource.model = $scope.newResource.model
            $scope.resource.model_id = $scope.newResource.model.id;
            $scope.resource.type = $scope.newResource.type
            $scope.resource.type_id = $scope.newResource.type.id;
          }

          flash.show("Zmodyfikowano dane urządzenia!", "success")
          if(differences.hasOwnProperty("holder") || differences.hasOwnProperty("owner") || differences.hasOwnProperty("state") ){
            $scope.sendEmail()
          }

          $scope.cancel()
        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas zmiany danych urządzenia!", "error")
        })
    }
  };

  $scope.remove = function(){
    var dlg = null;
        dlg = $dialogs.confirm('Usuń urządzenie','Czy chcesz usunąć urządzenie?');
        dlg.result.then(function(btn){
          if($scope.issim){
            $http.post("hideRecord.py", { 'id' :  $scope.resource.id , 'table': 'cbs_sim'})
              .success(function(data) {
                 $scope.resource.visible = false;
                 $scope.cancel()
              })
              .error(function(err){
                flash.show("Błąd!", "error")
              })
          }else{
            $http.post("hideRecord.py", { 'id' :  $scope.resource.id , 'table': 'cbs_device'})
              .success(function(data) {
                 $scope.resource.visible = false;
                 $scope.cancel()
              })
              .error(function(err){
                flash.show("Błąd!", "error")
              })
          }

        },function(btn){ });
  }
  $scope.cancel = function(){
    $scope.cancelEdit()
    //$modalInstance.close('cancel');
    $modalInstance.dismiss('cancel');
  };
});


/** DEV OPTION MODAL CONTROLLER**/
app.controller('devOptionModalInstanceCtrl', function ($scope,$http, flash, $dialogs, $modalInstance, USER_ROLES, AuthService, dest, DevicesTypes, DevicesModels, DevicesStates, Company){
  $scope.items = {}
  $scope.USER_ROLES= USER_ROLES
  $scope.isAuthorized = AuthService.isAuthorized;
  switch(dest){
    case 'type':
        $scope.items = DevicesTypes.getDeviceTypes();
        if(!$scope.items){
          $scope.items = DevicesTypes.loadDeviceTypesData();
        }
        $scope.$on('deviceTypesDataReady', function (event, data) {
          $scope.items = data;
          DevicesTypes.devicetypes = data;
        });
        var table = 'cbs_device_type'
        $scope.header = 'typ urządzenia'
      break;
    case 'model':
        $scope.items = DevicesModels.getDeviceModels();
        if(!$scope.items){
          $scope.items = DevicesModels.loadDeviceModelsData();
        }
        $scope.$on('deviceModelsDataReady', function (event, data) {
          $scope.items = data;
          DevicesModels.devicemodels = data;
        });
        var table = 'cbs_device_model'
        $scope.header = 'model'
      break;

    case 'company':
        $scope.items = Company.getCompany()
        if(!$scope.items){
          $scope.items = Company.loadCompanData();
        }
        $scope.$on('companyDataReady', function (event, data) {
          $scope.items = data;
          Company.company = data;
        });
        var table = 'cbs_company'
        $scope.header = 'firmę'
      break;

    case 'state':
        $scope.items =DevicesStates.getDeviceState()
        if(!$scope.items){
          $scope.items = DevicesStates.loadDeviceStateData();
        }
        $scope.$on('deviceStatesDataReady', function (event, data) {
          $scope.items = data;
          DevicesStates.devicestates = data;
        });
        var table = 'cbs_device_state'
        $scope.header = 'stan urządzenia'
      break;
  }


  $scope.selectedItem = $scope.items[0]
  $scope.editItem = angular.copy($scope.selectedItem)
  $scope.selectItem = function(){
    $scope.editItem = angular.copy($scope.selectedItem)
  }

  $scope.add = function(){
    var dlg = null;
    dlg = $dialogs.confirm('Potwierdzenie','Dodać nowy rekord?');
    dlg.result.then(function(btn){
      var newData ={
        description : $scope.editItem.description,
        name : $scope.editItem.name,
        visible: true
      }

      $http.post("addRecord.py", { 'table': table, 'data':newData})
          .success(function(data) {
            if(data > 0){
              flash.show("Dodano nowy typ urządzenia.", "success")
              newData.id = parseInt(data)
              $scope.items.push(newData)
              $scope.selectedItem = newData
              $scope.selectItem()
              $scope.cancel()
            }else{
              flash.show("Nie dodano nowego typu urządzenia.", "warning")
            }
        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas dodawnia nowego typu!", "error")
        })
    },function(btn){});
  };

   $scope.save = function (){
     var dlg = null;
        dlg = $dialogs.confirm('Modyfikacja danych','Czy chcesz zmodyfikować dane?');
        dlg.result.then(function(btn){
          var newData ={
            description : $scope.editItem.description,
            name : $scope.editItem.name,
            visible: $scope.editItem.visible,
          }
          $http.post("editRecord.py", { 'id' : $scope.selectedItem.id ,'table': table, 'data':newData})
              .success(function(data) {
                if(data > 0){
                  flash.show("Zaktalizowano dane.", "success")
                  $scope.selectedItem = $scope.editItem;
                  angular.forEach($scope.items, function(value, key) {
                    if( value.id == $scope.selectedItem.id){
                      value.name = $scope.editItem.name
                      value.description = $scope.editItem.description
                      value.visible = $scope.editItem.visible
                   }
                  },  null);
                  $scope.cancel()
                }else{
                  flash.show("Nie zaktualizowano danych.", "warning")
                }
            })
            .error(function(err){
              flash.show("Wystąpił błąd podczas aktualizacji danych!", "error")
            })
        },function(btn){});
   };

   $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});


/** CLIENT MODAL CONTROLLER**/
app.controller('clientModalInstanceCtrl', function ($scope,$http, flash, $dialogs, $modalInstance, USER_ROLES, AuthService, Clients, Company){

  $scope.editItem = {}

  $scope.clients = Clients.getClients();
  $scope.company = Company.getCompany()
  $scope.USER_ROLES= USER_ROLES
  $scope.isAuthorized = AuthService.isAuthorized;
  /*GET CLIENTS ****************/
    if(!$scope.clients){
      $scope.clients = Clients.loadClientsData();
    }
    $scope.$on('clientsDataReady', function (event, data) {
      $scope.clients = data;
      Clients.clients = data;
    });

  /*GET COMPANY ****************/
    if(!$scope.company){
      $scope.company = Company.loadCompanData();
    }
    $scope.$on('companyDataReady', function (event, data) {
      $scope.company = data;
      Company.company = data;
    });

  $scope.getItem = function(src, id){ // zwraca element o określonym id
    for(var i = 0; i<src.length;i++ ){
      if(src[i].id == id){
        return src[i]
      }
    }
  }


  $scope.add = function(){
    if(!$scope.editItem.firstname){
      flash.show("Uzupełnij poprawnie dane klienta.", "info")
      return;
    }
    if(!$scope.editItem.lastname){
      flash.show("Uzupełnij poprawnie dane klienta.", "info")
      return;
    }
    if(!$scope.editItem.company){
      flash.show("Uzupełnij poprawnie dane klienta.", "info")
      return;
    }
    if(!$scope.editItem.email){
      flash.show("Uzupełnij poprawnie dane klienta.", "info")
      return;
    }
    var newData ={
      firstname : $scope.editItem.firstname,
      lastname : $scope.editItem.lastname,
      company_id: $scope.editItem.company.id,
      email:  $scope.editItem.email,
      phone : $scope.editItem.phone,
      visible: true
    }

    $http.post("addRecord.py", { 'table': 'cbs_client', 'data':newData})
        .success(function(data) {
          if(data > 0){
            flash.show("Dodano nowego klienta.", "success")
            newData.id = parseInt(data)
            newData.company = $scope.editItem.company
            $scope.clients.push(newData)
            $scope.cancel()
          }else{
            flash.show("Nie dodano nowego klienta.", "warning")
          }
      })
      .error(function(err){
        flash.show("Wystąpił błąd podczas dodawnia nowego klient!", "error")
      })
  };


   $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});



/** NEW RESOURCE FILE CONTROLLER**/
app.controller('newResourcesFileModalInstanceCtrl', function ($scope, $http, flash, $filter, USER_ROLES, AuthService, $dialogs, $modalInstance, item, DevicesTypes, DevicesModels, DevicesStates, Company, Clients, Devices, SIM) {
  $scope.file = file;
  $scope.issim = false;
  $scope.header = 'Zmień wprowadzane dane';

  $scope.USER_ROLES= USER_ROLES
  $scope.isAuthorized = AuthService.isAuthorized;
  if($scope.file.hasOwnProperty('msisdn') || $scope.file.hasOwnProperty('iccid') || $scope.file.hasOwnProperty('imsi')){
    $scope.issim = true;
    $scope.header = 'Dodaj karty SIM';
  }

  $scope.clients = Clients.getClients();
  $scope.company = Company.getCompany()
  $scope.devicetypes = DevicesTypes.getDeviceTypes()
  $scope.devicemodels = DevicesModels.getDeviceModels()
  $scope.devicestates = DevicesStates.getDeviceState()




  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});


/** FAST TRANSACTION MODAL CONTROLLER**/
app.controller('fastTransactionModalInstanceCtrl', function ($scope, $rootScope, $http, flash, USER_ROLES, AuthService, $filter,$modalInstance,Operations, Transaction, SubTransactionDevice, SubTransactionSIM, $dialogs, item, Company, Clients, Session) {
  $scope.item = item
  $scope.issim = false
  if(item.hasOwnProperty("pin")){
    $scope.issim = true
  }
  $scope.session =Session
  $scope.USER_ROLES= USER_ROLES
  $scope.isAuthorized = AuthService.isAuthorized;

  $scope.transaction = Transaction.getTransactions()
  $scope.subtransactionsdevice = SubTransactionDevice.getSubTransactionsDevice()
  $scope.subtransactionssim = SubTransactionSIM.getSubTransactionsSim()

  $scope.header = "Szybkie wypożyczenie: "+ $scope.item.id

  $scope.clients = Clients.getClients();
  $scope.company = Company.getCompany()

  $scope.operations = Operations.getOperations();
  $scope.operation  =  $scope.operations[0]

  /* GET CLIENTS ****************/
    if(!$scope.clients){
      $scope.clients = Clients.loadClientsData();
    }
    $scope.$on('clientsDataReady', function (event, data) {
      $scope.clients = data;
      Clients.clients = data;
    });

  $scope.begin =  $filter('date')(new Date(), 'yyyy-MM-dd') //new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate();
  $scope.end =  ''
  $scope.endErr =  false

  $scope.notification_email =true

  $scope.fromWho = Session.client
  $scope.toWho = {}
  $scope.displaytext = {}

  $scope.$watch("displaytext.toWho", function(newVal, oldVal){
    if(newVal && (typeof newVal == "object")){
      $scope.toWho = newVal
      $scope.displaytext.toWho =  newVal.firstname+" "+newVal.lastname
    }
  }, true);

  $scope.transaction_id =0,

  $scope.sendEmail = function(asking,alldev, type){
    var send = function(asking,alldev, type){
      $scope.sendingEmail = true

      var devidlist = "0_"
      if(!$scope.issim){
        devidlist += $scope.item.id+"_"
      }

      devidlist = devidlist.substring(0, devidlist.length - 1);

      var simidlist  = "0_"
      if($scope.issim){
        simidlist += $scope.item.id+"_"
      }

      simidlist = simidlist.substring(0, simidlist.length - 1);

      if(new Date($scope.end) < new Date()){
        var expire = "true"
      }else{
        var expire = ""
      }
      var oper = "borow"
      if($scope.operation.id>1){
        oper = "pass"
      }
      var  emaildata ={
        "fromwho": $scope.fromWho.firstname+" "+$scope.fromWho.lastname,
        'fromemail':$scope.fromWho.email,
        'fromcompany':$scope.fromWho.company.name,
        'fromphone':$scope.fromWho.phone,



        "towho":$scope.toWho.firstname+" "+$scope.toWho.lastname,
        'toemail':$scope.toWho.email,
        'tocompany':$scope.toWho.company.name,
        'tophone':$scope.toWho.phone,

        "trid": $scope.transaction_id,
        "beginning": $filter('date')(new Date($scope.begin), 'yyyy-MM-dd'),
        "ending": $filter('date')(new Date($scope.end), 'yyyy-MM-dd'),
        "operation":oper,

        "expire" :expire,
        "msgtype":type ,
        "dev":devidlist,
        "sim":simidlist,
      }
      $http.post("sendEmail.py", emaildata)
        .success(function(data){
            $scope.sendingEmail = false
            if(data.toString()=="True"){
              if(asking){
                flash.show("Wysłano powiadomienie.", "success")
              }
            }else{
               flash.show("Nie wysłano powiadomnienia !", "info")
            }

        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas wysyłania emaila!", "error")
        })
    }
    if(asking){
      var dlg = null;
        dlg = $dialogs.confirm('Wysłać email','Czy chcesz wysłać email z przypomnieniem?');
        dlg.result.then(function(btn){
          send(asking,alldev, type)
      },function(btn){ });
    }else{
      send(asking,alldev, type)
    }
  }

  $scope.borow = function (){
    var goforward = true
    if(!$scope.displaytext.toWho){

      goforward = false
      $scope.displaytext.toWho = ""
    }else if(!angular.equals($scope.displaytext.toWho, $scope.toWho.firstname+" "+$scope.toWho.lastname) ){
      goforward = false

      $scope.displaytext.toWho = ""
    }

    if(!$scope.end){

      goforward = false
      $scope.end= ""
      $scope.endErr= true
    }

    if(!$scope.begin){

      goforward = false
      $scope.begin= ""
    }

    if(!goforward){
      flash.show("Wypełnij poprawnie wymagane pola", "error")
      return
    }

    var newData ={
      from_id :  $scope.fromWho.id,
      to_id :  $scope.toWho.id,
      operation_id : 1, ///  !!!! WYPOŻYCZENIE
      beginning: $filter('date')(new Date($scope.begin), 'yyyy-MM-dd'),
      ending:  $filter('date')(new Date($scope.end), 'yyyy-MM-dd'),
      is_open: true,
      description: "",
      visible: true,
      notification_email: $scope.notification_email,
    }

    $http.post("addRecord.py", {'table': 'cbs_transaction', 'data':newData})
      .success(function(data) {
        newData.id = parseInt(data)
        $scope.transaction_id = parseInt(data)
        var newSub={
          transaction_id :  parseInt(data),
          returned : false
        }

        if($scope.issim){
          var subtab = "cbs_subtransaction_sim"
          var itemtab = "cbs_device"
          newSub.sim_id =$scope.item.id
        }else{
          var subtab = "cbs_subtransaction_device"
          var itemtab = "cbs_sim"
          newSub.device_id =$scope.item.id
        }


        $http.post("addRecord.py", {'table': subtab, 'data':newSub})
          .success(function(data){
            newSub.id=parseInt(data),
            $http.post("editRecord.py", {'table': itemtab, 'data':{holder_id: $scope.toWho.id}, 'id': $scope.item.id})
              .success(function(data){
                $scope.item.holder_id = $scope.toWho.id
                $scope.item.holder= $scope.toWho


                if($scope.issim){ // DODAJ PODTRSANSAKCJĘ
                  if($scope.subtransactionssim){
                    $scope.subtransactionssim.push(newSub)
                  }
                }else{
                  if($scope.subtransactionsdevice){
                    $scope.subtransactionsdevice.push(newSub)
                  }
                }

                if($scope.transactions){
                  newData.from = $scope.fromWho;
                  newData.to = $scope.toWho;
                  newData.operation =  $scope.operation
                  $scope.transactions.push(newData)
                }
                if(newData.notification_email){
                  $scope.sendEmail(false,false, "newTransaction")
                }

                flash.show("Wypożyczono urządzenie!", "success")
                $rootScope.$broadcast('reloadBrowseResTable',  data)
                $scope.cancel()
              })
              .error(function(err){
               flash.show("Wystąpił błąd podczas dodawania nowej podtransakcji!", "error")
              })

          })
          .error(function(err){
           flash.show("Wystąpił błąd podczas dodawania nowej podtransakcji!", "error")
          })



        })
        .error(function(err){
          flash.show("Wystąpił błąd podczas dodawania nowej transakcji!", "error")
        })
  };


  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});


/** SPECIFICATION MODAL CONTROLLER**/
app.controller('specificationModalInstanceCtrl', function ($scope, $http, flash,  $dialogs, $filter, USER_ROLES, AuthService, specification, $modalInstance, Datasheet, Company){
  $scope.toogleEdit = false;
  $scope.selectedTab = "spec1";

  $scope.company =  Company.getCompany();
  $scope.USER_ROLES= USER_ROLES
  $scope.isAuthorized = AuthService.isAuthorized;
  $scope.specification = specification;
  if($scope.specification.datasheet_id){

  }
  $scope.datasheet = $filter('filter')( Datasheet.getDatasheets(), {id:parseInt($scope.specification.datasheet_id)}, true)[0]
  $scope.editSpec = angular.copy(specification);
  $scope.editData = angular.copy($scope.datasheet);


  $scope.displayText = {
    chipset_prvider: ""
  }
  if($scope.datasheet && $scope.datasheet.chipset_prvider){
    $scope.displayText.chipset_prvider = $scope.datasheet.chipset_prvider.name
    $scope.editData.chipset_prvider = $scope.datasheet.chipset_prvider
  }else{
    $scope.displayText.chipset_prvider = ""
  }

  $scope.$watch("displayText.chipset_prvider", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
    if(newVal && (typeof newVal == "object")){
        $scope.displayText.chipset_prvider = newVal.name
        $scope.editData.chipset_prvider_id = newVal.id
        $scope.editData.chipset_prvider = newVal
    }
  }, true);


  $scope.selectoptions ={
    tgpp: ["Rel-13", "Rel-12", "Rel-11", "Rel-10", "Rel-9", "Rel-8", "Rel-7", "Rel-6", "Rel-5", "Rel-4", "Rel-99"],
    usagesetting: ["voice centric","data centric" ],
    voice_domain: ["CS Voice only", "IMS PS Voice only", "CS Voice preferred, IMS PS Voice as secondary", "IMS PS Voice preferred, CS Voice as secondary"],
    drx: ["short", "long", "short/long"],
    mimo: ["2x2", "3x3", "4x4", "8x8"],
    duplex: ["TDD", "FDD", "TDD/FDD"]
  }

  $scope.band_lte =[
    {title:"B1", selected:false}, {title:"B2", selected:false},
    {title:"B3", selected:false}, {title:"B4", selected:false},
    {title:"B5", selected:false}, {title:"B6", selected:false},
    {title:"B7", selected:false}, {title:"B8", selected:false},
    {title:"B9", selected:false}, {title:"B10", selected:false},
    {title:"B11", selected:false}, {title:"B12", selected:false},
    {title:"B13", selected:false}, {title:"B14", selected:false},

    {title:"B17", selected:false}, {title:"B18", selected:false},
    {title:"B19", selected:false}, {title:"B20", selected:false},
    {title:"B21", selected:false}, {title:"B22", selected:false},
    {title:"B23", selected:false}, {title:"B24", selected:false},
    {title:"B25", selected:false}, {title:"B26", selected:false},
    {title:"B27", selected:false}, {title:"B28", selected:false},
    {title:"B29", selected:false}, {title:"B30", selected:false},

    {title:"B31", selected:false},
    {title:"B33", selected:false}, {title:"B34", selected:false},
    {title:"B35", selected:false}, {title:"B36", selected:false},
    {title:"B37", selected:false}, {title:"B38", selected:false},
    {title:"B39", selected:false}, {title:"B40", selected:false},
    {title:"B41", selected:false}, {title:"B42", selected:false},
    {title:"B43", selected:false}, {title:"B44", selected:false}
  ]
  $scope.band_3g =[
    {title:"B1", selected:false}, {title:"B2", selected:false},
    {title:"B3", selected:false}, {title:"B4", selected:false},
    {title:"B5", selected:false}, {title:"B6", selected:false},
    {title:"B7", selected:false}, {title:"B8", selected:false},
    {title:"B9", selected:false}, {title:"B10", selected:false},
    {title:"B11", selected:false}, {title:"B12", selected:false},
    {title:"B13", selected:false}, {title:"B14", selected:false},

    {title:"B19", selected:false}, {title:"B20", selected:false},
    {title:"B21", selected:false}, {title:"B22", selected:false},
    {title:"B25", selected:false}, {title:"B26", selected:false}
  ]
  $scope.band_2g =[
    {title:"850", selected:false}, {title:"900", selected:false},
    {title:"1800", selected:false}, {title:"1900", selected:false}
  ]
  $scope.band_wifi =[
    {title:"2,4", selected:false},
    {title:"5", selected:false}
  ]
  $scope.standard_wifi =[
    {title:"a", selected:false}, {title:"b", selected:false},
    {title:"g", selected:false}, {title:"n", selected:false},
    {title:"ac", selected:false}
  ]
  $scope.rohc =[
    {title:"0x0001", selected:false}, {title:"0x0002", selected:false},
    {title:"0x0003", selected:false}, {title:"0x0004", selected:false},
    {title:"0x0006", selected:false},{title:"0x0101", selected:false},
    {title:"0x0102", selected:false},{title:"0x0103", selected:false},
    {title:"0x0104", selected:false}
  ]

  $scope.$watch("band_lte", function(newVal, oldVal){ // zamień zaznaczone pasma na string
    if(!angular.equals(newVal, oldVal)){
      var temp =""
      for(var i =0; i < $scope.band_lte.length;i++ ){
        if($scope.band_lte[i].selected==true){
          temp += $scope.band_lte[i].title+" "
        }
      }

      $scope.editData.band_lte = temp.trim()
    }
  }, true);
  $scope.$watch("band_3g", function(newVal, oldVal){ // zamień zaznaczone pasma na string
    if(!angular.equals(newVal, oldVal)){
      var temp =""
      for(var i =0; i < $scope.band_3g.length;i++ ){
        if($scope.band_3g[i].selected==true){
          temp += $scope.band_3g[i].title+" "
        }
      }
      $scope.editData.band_3g = temp.trim()
    }
  }, true);
  $scope.$watch("band_2g", function(newVal, oldVal){ // zamień zaznaczone pasma na string
    if(!angular.equals(newVal, oldVal)){
      var temp =""
      for(var i =0; i < $scope.band_2g.length;i++ ){
        if($scope.band_2g[i].selected==true){
          temp += $scope.band_2g[i].title+" "
        }
      }
      $scope.editData.band_2g = temp.trim()
    }
  }, true);
  $scope.$watch("band_wifi", function(newVal, oldVal){ // zamień zaznaczone pasma na string
    if(!angular.equals(newVal, oldVal)){
      var temp =""
      for(var i =0; i < $scope.band_wifi.length;i++ ){
        if($scope.band_wifi[i].selected==true){
          temp += $scope.band_wifi[i].title+" "
        }
      }
      $scope.editData.band_wifi = temp.trim()
    }
  }, true);
  $scope.$watch("standard_wifi", function(newVal, oldVal){ // zamień zaznaczone pasma na string
    if(!angular.equals(newVal, oldVal)){
      var temp =""
      for(var i =0; i < $scope.standard_wifi.length;i++ ){
        if($scope.standard_wifi[i].selected==true){
          temp += $scope.standard_wifi[i].title+" "
        }
      }
      $scope.editData.standard_wifi = temp.trim()
    }
  }, true);

  $scope.$watch("rohc", function(newVal, oldVal){ // zamień zaznaczone pasma na string
    if(!angular.equals(newVal, oldVal)){
      var temp =""
      for(var i =0; i < $scope.rohc.length;i++ ){
        if($scope.rohc[i].selected==true){
          temp += $scope.rohc[i].title+" "
        }
      }
      $scope.editData.rohc = temp.trim()
    }
  }, true);


  $scope.selectItemFromDatasheet = function(){
    if($scope.editData){
      arraysToselect = ["band_lte", "band_3g", "band_2g", "band_wifi" ,"standard_wifi", "rohc"]
      for(var n =0; n<arraysToselect.length; n++){
        if(!$scope.editData.hasOwnProperty(arraysToselect[n])){
          continue;
        }
        if($scope.editData.hasOwnProperty(arraysToselect[n]) && $scope.editData[arraysToselect[n]]){
          var bands = $scope.editData[arraysToselect[n]].split(" ")
          for(var i =0; i< bands.length;i++){
            var item = $filter('filter')($scope[arraysToselect[n]], {title:bands[i]}, true)
            if(item.length>=1){
              item[0].selected = true
            }
          }
        }

      }
    }else{
      arraysToselect = ["band_lte", "band_3g", "band_2g", "band_wifi" ,"standard_wifi", "rohc"]
      for(var n =0; n<arraysToselect.length; n++){
        for(var i =0; i< $scope[arraysToselect[n]].length;i++){
          $scope[arraysToselect[n]][i].selected = false
        }
      }
    }
  }
  $scope.selectItemFromDatasheet()


  $scope.save = function(){
    var tempSpec ={}
    angular.forEach($scope.editSpec, function(value, key){
      if((key != "company") && (key != "model") && (key != "$$hashKey")){
        tempSpec[key] = value
      }
    });
    $http.post("editRecord.py", { 'id' : $scope.editSpec.id , 'table': "cbs_specification", data:tempSpec})
      .success(function(data){
        angular.forEach($scope.editSpec, function(value, key){
          if((key != "company") && (key != "model") && (key != "$$hashKey")){
            $scope.specification[key] = tempSpec[key]
          }
        });
        flash.show("Zaktalizowano dane specyfikacji", "success")

      })
      .error(function(data){
        flash.show("Wystąpił błąd podczas modyfikacji danych specyfikacji", "error")
      })
  }

  $scope.saveDatasheet = function(){

    if(!angular.equals($scope.datasheet, $scope.editData)){

       var temp = {}

       angular.forEach($scope.editData, function(value, key){
          if(key != "chipset_prvider"){
            temp[key] = $scope.editData[key]
          }
        });

      $http.post("editRecord.py", { 'id' : parseInt($scope.editData.id), 'table': "cbs_datasheet", data: temp})
        .success(function(data){
          $scope.datasheet = $scope.editData;
          flash.show("Zaktalizowano dane katalogowe", "success")
        })
        .error(function(data){
          flash.show("Wystąpił błąd podczas modyfikacji danych katalogowych", "error")
        })


    }
  }

  /**   RAPORT **/

   $scope.options = {
      height: 100,
      /*width: 500,*/
      lang:"pl-PL",
      disableDragAndDrop: true,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
    };

      $scope.init = function() {  };
      $scope.enter = function() {  };
      $scope.focus = function(e) {  };
      $scope.blur = function(e) { };
      $scope.paste = function() {  };
      $scope.change = function(contents, editable$) {

      };
      $scope.keyup = function(e) { /*console.log('Key is released:', e.keyCode); */ };
      $scope.keydown = function(e) { /*console.log('Key is pressed:', e.keyCode); */};
      $scope.imageUpload = function(files, editor, $editable) {
         // console.log('image upload:', files, editor, $editable);
    };
    $scope.raport = {}
    if($scope.specification.description){
        $scope.description = angular.copy($scope.specification.description).toString()
    }else{
        $scope.description = ""
    }

    $scope.description_lines = 1


  $scope.$watch("description", function(newVal, oldVal){ // zamień zaznaczone pasma na string
     $scope.description_lines = $scope.description.split(/\r\n|\r|\n/).length
  }, true);


  $scope.cancelEdit = function(){
    $scope.editSpec = angular.copy(specification);
  }

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
