app.controller("browseSpecificationCtrl", function($scope,flash,  $filter, Datasheet, DevicesModels, Company, Specification, ngTableParams) {
	$scope.flash = flash
	$scope.pageLoading = true;

	$scope.openTab ='fs1';

	$scope.company = Company.getCompany()
	$scope.devicemodels = DevicesModels.getDeviceModels()
	$scope.specifications = Specification.getSpecification()
	$scope.datasheets = Datasheet.getDatasheets()
	 

/*GET DEVICES MODELS ****************/
	if(!$scope.devicemodels){
	 	$scope.devicemodels = DevicesModels.loadDeviceModelsData();
	}  
	$scope.$on('deviceModelsDataReady', function (event, data) {
		if($scope.specifications && $scope.specifications.length>0 && !$scope.specifications[0].hasOwnProperty("model")){
			for(var i =0; i<$scope.specifications.length;i++){
				$scope.specifications[i].model = $filter('filter')(data, {id:parseInt($scope.specifications[i].model_id)}, true)[0]	
			}
		}
		$scope.devicemodels = data;
		DevicesModels.devicemodels = data;
		$scope.changeLoading()
	});

/*GET COMPANY ****************/
  	if(!$scope.company){
	 	$scope.company = Company.loadCompanData();
	}  
	$scope.$on('companyDataReady', function (event, data) {
		if($scope.specifications && $scope.specifications.length>0 && !$scope.specifications[0].hasOwnProperty("company")){
			for(var i =0; i<$scope.specifications.length;i++){
				$scope.specifications[i].company = $filter('filter')(data, {id:parseInt($scope.specifications[i].company_id)}, true)[0]
			}
		}

		if($scope.datasheets && $scope.datasheets.length>0 && !$scope.datasheets[0].hasOwnProperty("chipset_prvider")){
			for(var i =0; i<$scope.datasheets.length;i++){
				$scope.datasheets[i].chipset_prvider = $filter('filter')(data, {id:parseInt($scope.datasheets[i].chipset_prvider_id)}, true)[0]
			}
		}

		$scope.company = data;
		Company.company = data;
		$scope.changeLoading()
	});

/*GET SPECIFICATIONS ****************/
	if(!$scope.specifications){
	 	$scope.specifications = Specification.loadSpecificationsData();
	}  
	$scope.$on('specificationsDataReady', function (event, data) {
		if(( $scope.devicemodels && ($scope.devicemodels.length>0) )|| ($scope.company &&  ($scope.company.length>0))){
			for(var i =0; i<data.length;i++){
				if( $scope.company && ($scope.company.length>0)){
					data[i].company = $filter('filter')($scope.company, {id:parseInt(data[i].company_id)}, true)[0] 
				}
				if( $scope.devicemodels && ($scope.devicemodels.length>0) ){
					data[i].model = $filter('filter')($scope.devicemodels, {id:parseInt(data[i].model_id)}, true)[0]
				}	
			}
		}
		$scope.specifications = data;
		Specification.specifications = data;
		$scope.changeLoading()
	});

/*GET DATASHEETS ****************/
	if(!$scope.datasheets || ($scope.datasheets.length==0)){
	 	$scope.datasheets = Datasheet.loadDatasheetsData();
	}  
	$scope.$on('datasheetsDataReady', function (event, data) {
		if($scope.company && ($scope.company.length>0)){
			for(var i =0; i<data.length;i++){
				data[i].chipset_prvider = $filter('filter')($scope.company, {id:parseInt(data[i].chipset_prvider_id)}, true)[0] 
			}
		}
		$scope.datasheets = data;
		Datasheet.datasheets = data;
		$scope.changeLoading()
	});
//
	
	$scope.changeLoading = function(){	// sprawdĹş czy juĹĽ moĹĽna urkyÄ‡ wizualizacjÄ™ Ĺ‚adowania 
		if($scope.company && $scope.devicemodels && $scope.specifications && $scope.datasheets){
			$scope.pageLoading = false;
			$scope.initTable();
		} else{
			$scope.pageLoading = true;
		}			
	}
	

	//$scope.columns = {id: true, model: true, company: true };

	$scope.columns = {id:{title:"Id", visible: true}, model:{title:"Model", visible: true}, company:{title:"Firma", visible: true}, operating_system:{title:"System", visible: true},
					  system_version:{title:"Wersja Systemu", visible: true}, system_version_number:{title:"Numer Wersji Sys.", visible: false},  firmware:{title:"Firmware", visible: true},
					  hardware:{title:"Hardware", visible: false},	adddate:{title:"Data", visible: true}, description:{title:"Opis", visible: false}};

	$scope.band_lte =[
		{title:"B1", selected:false}, {title:"B2", selected:false},
		{title:"B3", selected:false}, {title:"B4", selected:false},
		{title:"B5", selected:false}, {title:"B6", selected:false},
		{title:"B7", selected:false}, {title:"B8", selected:false},
		{title:"B9", selected:false}, {title:"B10", selected:false},
		{title:"B11", selected:false}, {title:"B12", selected:false},
		{title:"B13", selected:false}, {title:"B14", selected:false},

		{title:"B17", selected:false}, {title:"B18", selected:false},
		{title:"B19", selected:false}, {title:"B20", selected:false},
		{title:"B21", selected:false}, {title:"B22", selected:false},
		{title:"B23", selected:false}, {title:"B24", selected:false},
		{title:"B25", selected:false}, {title:"B26", selected:false},
		{title:"B27", selected:false}, {title:"B28", selected:false},
		{title:"B29", selected:false}, {title:"B30", selected:false},

		{title:"B31", selected:false},
		{title:"B33", selected:false}, {title:"B34", selected:false},
		{title:"B35", selected:false}, {title:"B36", selected:false},
		{title:"B37", selected:false}, {title:"B38", selected:false},
		{title:"B39", selected:false}, {title:"B40", selected:false},
		{title:"B41", selected:false}, {title:"B42", selected:false},
		{title:"B43", selected:false}, {title:"B44", selected:false}
	]
	$scope.band_3g =[
		{title:"B1", selected:false}, {title:"B2", selected:false},
		{title:"B3", selected:false}, {title:"B4", selected:false},
		{title:"B5", selected:false}, {title:"B6", selected:false},
		{title:"B7", selected:false}, {title:"B8", selected:false},
		{title:"B9", selected:false}, {title:"B10", selected:false},
		{title:"B11", selected:false}, {title:"B12", selected:false},
		{title:"B13", selected:false}, {title:"B14", selected:false},

		{title:"B19", selected:false}, {title:"B20", selected:false},
		{title:"B21", selected:false}, {title:"B22", selected:false},
		{title:"B25", selected:false}, {title:"B26", selected:false}
	]
	$scope.band_2g =[
		{title:"850", selected:false}, {title:"900", selected:false},
		{title:"1800", selected:false}, {title:"1900", selected:false}
	]
	$scope.band_wifi =[
		{title:"2,4", selected:false},
		{title:"5", selected:false}
	]
	/*
	$scope.standard_wifi =[
		{title:"a", selected:false}, {title:"b", selected:false},
		{title:"g", selected:false}, {title:"n", selected:false},
		{title:"ac", selected:false} 
	]*/
	$scope.rohc =[
		{title:"0x0001", selected:false}, {title:"0x0002", selected:false},
		{title:"0x0003", selected:false}, {title:"0x0004", selected:false},
		{title:"0x0006", selected:false},{title:"0x0101", selected:false},
		{title:"0x0102", selected:false},{title:"0x0103", selected:false},
		{title:"0x0104", selected:false}
	]
 
 
	$scope.mimo =[
		{title:"2x2", selected:false}, {title:"3x3", selected:false},
		{title:"4x4", selected:false}, {title:"8x8", selected:false} 
	] 
 
	$scope.drx =[
		{title:"short", selected:false}, {title:"long", selected:false},
		{title:"short/long", selected:false}
	]

	$scope.duplex =[
		{title:"TDD", selected:false}, {title:"FDD", selected:false},
		{title:"TDD/FDD", selected:false}
	]


	$scope.shown_bands_filter = $scope.band_lte;
	$scope.show_bands_filter = function(tab){
		$scope.shown_bands_filter = tab
	}

	$scope.datasheet_filter = {
		chipset : {},

		ue_usage_setting : {},
		voice_domain : {},

		terminal: {},
		tgpp: [],
		system_gsm : false,
        system_gprs : false,
        system_edge : false,
        system_umts : false,
        system_hspa : false,
        system_hspa_p : false,
        system_lte : false, 
        system_lte_a : false,
        system_wifi : false,

        standard_wifi : [],
        band_lte  : [],
        band_3g : [],
        band_2g : [],
        band_wifi : [],

        lte_3g_handover : false,
        lte_2g_handover : false,
        lte_2g_nacc : false,
        tg_lte_handover : false,
        dg_lte_handover : false,
        inter_frequency_handover : false,
        fdd_tdd_handover : false,

        intra_band_c_CA : "",
        intra_band_nc_CA : "",
        inter_band_CA : "",

        dual_carrier : false,

        mimo : [],
        rohc : [],
        drx : [],
        duplex: []
	}

	$scope.$watch("f_chipset_provider", function(newVal, oldVal){ // odĹ›wieĹĽa tabelÄ™ podczas filtrowania
 		if(newVal && (typeof newVal == "object")){
	    	$scope.f_chipset_provider = newVal.name
	    	 
 		}   
	}, true);

	$scope.datasheet_filter.chipset.providers =[]
	$scope.datasheet_filter.chipset.models =[]
	$scope.datasheet_filter.chipset.baselines =[]


	$scope.datasheet_filter.ue_usage_setting.voice_centric = false;
	$scope.datasheet_filter.ue_usage_setting.data_centric = false;
	 
	$scope.datasheet_filter.voice_domain.cs_voice = false;
	$scope.datasheet_filter.voice_domain.ps_voice = false;
	$scope.datasheet_filter.voice_domain.cs_voice_ps_voice = false;
	$scope.datasheet_filter.voice_domain.ps_voice_cs_voice = false

	$scope.datasheet_filter.terminal.cat_3g_downlink = []
	$scope.datasheet_filter.terminal.cat_3g_uplink = []
	$scope.datasheet_filter.terminal.cat_lte = []

 
	$scope.addChipsetFilter = function(provider, model, baseline){
		if(provider && provider!="" && ($scope.datasheet_filter.chipset.providers.indexOf(provider)==-1)){
			$scope.datasheet_filter.chipset.providers.push(provider);
		}
		if(model && model!="" && ($scope.datasheet_filter.chipset.models.indexOf(model)==-1)){
			$scope.datasheet_filter.chipset.models.push(model);	
		}
		if(baseline && baseline!="" && ($scope.datasheet_filter.chipset.baselines.indexOf(baseline)==-1)){
			$scope.datasheet_filter.chipset.baselines.push(baseline);	
		}
		//$scope.datasheet_filter.chipset
	}

	$scope.addVoiceFilter = function(ues, vd){
		if(!ues && !vd){
			$scope.datasheet_filter.ue_usage_setting.voice_centric = false;
			$scope.datasheet_filter.ue_usage_setting.data_centric = false;
			$scope.datasheet_filter.voice_domain.cs_voice = false;
			$scope.datasheet_filter.voice_domain.ps_voice = false;
			$scope.datasheet_filter.voice_domain.cs_voice_ps_voice = false;
			$scope.datasheet_filter.voice_domain.ps_voice_cs_voice = false	
		}else{
			if(ues){
				$scope.datasheet_filter.ue_usage_setting.voice_centric = ues.voice;
				$scope.datasheet_filter.ue_usage_setting.data_centric = ues.data;	
			}
			if(vd){
				$scope.datasheet_filter.voice_domain.cs_voice = vd.cs_voice;
				$scope.datasheet_filter.voice_domain.ps_voice = vd.ps_voice;
				$scope.datasheet_filter.voice_domain.cs_voice_ps_voice = vd.cs_voice_ps_voice;
				$scope.datasheet_filter.voice_domain.ps_voice_cs_voice = vd.ps_voice_cs_voice
			}		
		}		 
	}

	$scope.addTerminalFilter = function(terminal){
		if(terminal){
			if(terminal.hasOwnProperty("downlink") && (terminal.downlink.trim() != "")){
				 
				$scope.datasheet_filter.terminal.cat_3g_downlink = terminal.downlink.trim().split(" ")
			}else{
				$scope.datasheet_filter.terminal.cat_3g_downlink = []
			}

			if(terminal.hasOwnProperty("uplink")  && (terminal.uplink.trim() != "")){
				$scope.datasheet_filter.terminal.cat_3g_uplink = terminal.uplink.trim().split(" ") 
			}else{
			 	$scope.datasheet_filter.terminal.cat_3g_uplink = []
			}

			if(terminal.hasOwnProperty("lte")  && (terminal.lte.trim() != "")){
				$scope.datasheet_filter.terminal.cat_lte = terminal.lte.trim().split(" ")
			}else{
				$scope.datasheet_filter.terminal.cat_lte = []
			}	
		}
		
	}

	$scope.add3GPPFilter = function(tgpp){
		$scope.datasheet_filter.tgpp = []
		angular.forEach(tgpp, function(value, key) {
			if(value){
				$scope.datasheet_filter.tgpp.push(key.toString())	
			}
		   
		}, false);
	}
 
	$scope.addSystemsFilter = function(systems, wifi){
		$scope.datasheet_filter.standard_wifi = []
		angular.forEach(wifi, function(value, key) {
			if(value){
				$scope.datasheet_filter.standard_wifi.push(key.toString())	
			}
		}, false);

		if(systems){
			$scope.datasheet_filter.system_gsm = systems.system_gsm
	        $scope.datasheet_filter.system_gprs = systems.system_gprs
	        $scope.datasheet_filter.system_edge = systems.system_edge
	        $scope.datasheet_filter.system_umts = systems.system_umts
	        $scope.datasheet_filter.system_hspa = systems.system_hspa
	        $scope.datasheet_filter.system_hspa_p = systems.system_hspa_p
	        $scope.datasheet_filter.system_lte = systems.system_lte
	        $scope.datasheet_filter. system_lte_a = systems.system_lte_a
	        $scope.datasheet_filter.system_wifi = systems.system_wifi	
		}else{
			$scope.datasheet_filter.system_gsm = false
	        $scope.datasheet_filter.system_gprs = false
	        $scope.datasheet_filter.system_edge = false
	        $scope.datasheet_filter.system_umts = false
	        $scope.datasheet_filter.system_hspa = false
	        $scope.datasheet_filter.system_hspa_p = false
	        $scope.datasheet_filter.system_lte = false
	        $scope.datasheet_filter. system_lte_a = false
	        $scope.datasheet_filter.system_wifi = false
		}
	}

	$scope.addBandsFilter = function(clear){
		$scope.datasheet_filter.band_lte = []
		$scope.datasheet_filter.band_3g = []
		$scope.datasheet_filter.band_2g = []
		$scope.datasheet_filter.band_wifi = []

		angular.forEach($scope.band_lte, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.band_lte.push(value.title.toString())	
			}
		}, false);

		angular.forEach($scope.band_3g, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.band_3g.push(value.title.toString())	
			}
		}, false);

		angular.forEach($scope.band_2g, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.band_2g.push(value.title.toString())	
			}
		}, false);

		angular.forEach($scope.band_wifi, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.band_wifi.push(value.title.toString())	
			}
		}, false);
		if(clear){
			$scope.datasheet_filter.band_lte = []
			$scope.datasheet_filter.band_3g = []
			$scope.datasheet_filter.band_2g = []
			$scope.datasheet_filter.band_wifi = []	
		}	  
	}
	 
	$scope.addMobilityFilter = function(mob){
		if(mob){
			$scope.datasheet_filter.lte_3g_handover = mob.lte_3g_handover;
	        $scope.datasheet_filter.lte_2g_handover = mob.lte_2g_handover;
	        $scope.datasheet_filter.lte_2g_nacc = mob.lte_2g_nacc;
	        $scope.datasheet_filter.tg_lte_handover = mob.tg_lte_handover;
	        $scope.datasheet_filter.dg_lte_handover = mob.dg_lte_handover;
	        $scope.datasheet_filter.inter_frequency_handover = mob.inter_frequency_handover;
	        $scope.datasheet_filter.fdd_tdd_handover = mob.fdd_tdd_handover;
		}else{
			$scope.datasheet_filter.lte_3g_handover = false;
	        $scope.datasheet_filter.lte_2g_handover = false;
	        $scope.datasheet_filter.lte_2g_nacc = false;
	        $scope.datasheet_filter.tg_lte_handover = false;
	        $scope.datasheet_filter.dg_lte_handover =false;
	        $scope.datasheet_filter.inter_frequency_handover = false;
	        $scope.datasheet_filter.fdd_tdd_handover = false;
		}
	}

	$scope.addCarrierAgregationFilter = function(temp, clear){
		if(!clear && temp){
			$scope.datasheet_filter.intra_band_c_CA = temp.intra_band_c_CA;
        	$scope.datasheet_filter.intra_band_nc_CA = temp.intra_band_nc_CA;
        	$scope.datasheet_filter.inter_band_CA =temp.inter_band_CA;
		}else{
			$scope.datasheet_filter.intra_band_c_CA = "";
        	$scope.datasheet_filter.intra_band_nc_CA = "";
        	$scope.datasheet_filter.inter_band_CA ="";	
		}
	}

	$scope.addOthersFilter = function(others,clear){
		if(!clear && others){
			$scope.datasheet_filter.dual_carrier =	others.dual_carrier;
		}else{
			$scope.datasheet_filter.dual_carrier = false	
		}
		

		$scope.datasheet_filter.mimo = []
		$scope.datasheet_filter.rohc = []
		$scope.datasheet_filter.drx = []
		$scope.datasheet_filter.duplex = []

		angular.forEach($scope.mimo, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.mimo.push(value.title.toString())	
			}
		}, false);

		angular.forEach($scope.rohc, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.rohc.push(value.title.toString())	
			}
		}, false);

		angular.forEach($scope.drx, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.drx.push(value.title.toString())	
			}
		}, false);

		angular.forEach($scope.duplex, function(value, key) {
			if(value.selected){
				$scope.datasheet_filter.duplex.push(value.title.toString())	
			}
		}, false);
		if(clear){
			$scope.datasheet_filter.mimo = []
			$scope.datasheet_filter.rohc = []
			$scope.datasheet_filter.drx = []
			$scope.datasheet_filter.duplex = []	
		}	  
	}


/******  cCUSTOM FILTER  ******/ 

	$scope.is_in_array = function(name, items_to_check){
		var ret = items_to_check.indexOf(name);
		if(ret>=0){
			return true;
		}else{
			return false;
		}
	}

	$scope.check_bands =function(datasheets, filter){
		if((filter.length==0)){ //!datasheets || 
			return true	
		}
		if(!datasheets &&   (filter.length>=0)){ //!datasheets || 
			return false	
		}
		var bands_from_str = datasheets.split(" ");
		if(bands_from_str.length==0){
			return true		
		}
		for(var i =0; i<filter.length;i++){
			var temp = filter[i] 
			if(bands_from_str.indexOf(temp) ==-1){
				return false
			}
		}
		return true	
	}

	$scope.filterDatasheet = function(){
		var returne_items = []

		for(var i = 0; i < $scope.datasheets.length; i++) {
			var item = $scope.datasheets[i]
                        
			var chipset_flag = true
			if($scope.datasheet_filter.chipset.providers.length){
				if(item.chipset_prvider){
					chipset_flag &= $scope.is_in_array(item.chipset_prvider.name, $scope.datasheet_filter.chipset.providers)
				}else{
					chipset_flag &= false;
				}

			}
			if($scope.datasheet_filter.chipset.models.length){
				if(item.chipset_model){
					chipset_flag &= $scope.is_in_array(item.chipset_model, $scope.datasheet_filter.chipset.models)
				}else{
					chipset_flag &= false;
				}
			}
			if($scope.datasheet_filter.chipset.baselines.length){
				if(item.chipset_baseline){
					chipset_flag &= $scope.is_in_array(item.chipset_baseline, $scope.datasheet_filter.chipset.baselines)
				}else{
					chipset_flag &= false;
				}

			}
			if(!chipset_flag){continue}

			var ues_flag = true	
			if($scope.datasheet_filter.ue_usage_setting.voice_centric && !angular.equals(item.ue_usage_setting, "voice centric")){
				ues_flag = false		
			}
			if(ues_flag && $scope.datasheet_filter.ue_usage_setting.data_centric && !angular.equals(item.ue_usage_setting, "data centric")){
				ues_flag = false		
			}
			if($scope.datasheet_filter.ue_usage_setting.voice_centric && $scope.datasheet_filter.ue_usage_setting.data_centric){
				ues_flag = true	
			}
			if(!ues_flag){continue}

			
			var domain_flag_1 = false	
			var domain_flag_2 = false
			var domain_flag_3 = false
			var domain_flag_4 = false
			var domain_flag_all=false
			if($scope.datasheet_filter.voice_domain.cs_voice && angular.equals(item.voice_domain, "CS Voice only")){
				domain_flag_1 = true
				domain_flag_all=true	
			}
			if($scope.datasheet_filter.voice_domain.ps_voice && angular.equals(item.voice_domain, "IMS PS Voice only")){
				domain_flag_2 = true
				domain_flag_all=true			
			}
			if($scope.datasheet_filter.voice_domain.cs_voice_ps_voice && angular.equals(item.voice_domain, "CS Voice preferred, IMS PS Voice as secondary")){
				domain_flag_3 = true
				domain_flag_all=true			
			}
			if($scope.datasheet_filter.voice_domain.ps_voice_cs_voice && angular.equals(item.voice_domain, "IMS PS Voice preferred, CS Voice as secondary")){
				domain_flag_4 = true
				domain_flag_all=true			
			}
			if(!(domain_flag_1 || domain_flag_2 || domain_flag_3 || domain_flag_4) && domain_flag_all){continue}
			

			var terminal = true
			if($scope.datasheet_filter.terminal.cat_3g_downlink.length>0){
				if(item.cat_3g_downlink){
					terminal &= $scope.is_in_array(item.cat_3g_downlink, $scope.datasheet_filter.terminal.cat_3g_downlink)
				}else{
					terminal &= false;
				}	
			}
		 	if($scope.datasheet_filter.terminal.cat_3g_uplink.length>0){
				if(item.cat_3g_uplink){
 					terminal &= $scope.is_in_array(item.cat_3g_uplink, $scope.datasheet_filter.terminal.cat_3g_uplink)
				}else{
					terminal &= false;
				}
 			}
 			if($scope.datasheet_filter.terminal.cat_lte.length>0){
				if(item.cat_lte){
 					terminal &= $scope.is_in_array(item.cat_lte, $scope.datasheet_filter.terminal.cat_lte)
				}else{
					terminal &= false;
				}

 			}
 			if(!terminal){continue}
		 	

		 	var tgpp = true
			if($scope.datasheet_filter.tgpp.length){
				tgpp &= $scope.is_in_array(item.tgpp, $scope.datasheet_filter.tgpp)	
			}
			if(!tgpp){continue}

			var systems = true
			if($scope.datasheet_filter.system_gsm){ systems &= item.system_gsm}
			if($scope.datasheet_filter.system_gprs){ systems &= item.system_gprs}
			if($scope.datasheet_filter.system_edge){ systems &= item.system_edge}
			if($scope.datasheet_filter.system_umts){ systems &= item.system_umts}
			if($scope.datasheet_filter.system_hspa){ systems &= item.system_hspa}
			if($scope.datasheet_filter.system_hspa_p){ systems &= item.system_hspa_p}
			if($scope.datasheet_filter.system_lte){ systems &= item.system_lte}
			if($scope.datasheet_filter.system_lte_a){ systems &= item.system_lte_a}
			if($scope.datasheet_filter.system_wifi){ systems &= item.system_wifi}
			if(!systems){continue}
			
			

			var bands = true

		 	bands &= $scope.check_bands(item.band_lte, $scope.datasheet_filter.band_lte)
		 	bands &= $scope.check_bands(item.band_3g, $scope.datasheet_filter.band_3g)
		 	bands &= $scope.check_bands(item.band_2g, $scope.datasheet_filter.band_2g)
		 	bands &= $scope.check_bands(item.band_wifi, $scope.datasheet_filter.band_wifi)
			bands &= $scope.check_bands(item.standard_wifi, $scope.datasheet_filter.standard_wifi)
			if(!bands){continue}
			
			

			var handover = true
			if($scope.datasheet_filter.lte_3g_handover){ handover &= item.lte_3g_handover}
			if($scope.datasheet_filter.lte_2g_handover){ handover &= item.lte_2g_handover}
			if($scope.datasheet_filter.lte_2g_nacc){ handover &= item.lte_2g_nacc}
			if($scope.datasheet_filter.tg_lte_handover){ handover &= item.tg_lte_handover}
			if($scope.datasheet_filter.dg_lte_handover){ handover &= item.dg_lte_handover}	
			if($scope.datasheet_filter.inter_frequency_handover){ handover &= item.inter_frequency_handover}	
			if($scope.datasheet_filter.fdd_tdd_handover){ handover &= item.fdd_tdd_handover}	
			if(!handover){continue}

			var inter = true
			if($scope.datasheet_filter.intra_band_c_CA){
				if(!item.intra_band_c_CA){
					continue
				}
				if(item.intra_band_c_CA.indexOf($scope.datasheet_filter.intra_band_c_CA) >=0){
					inter &= true
				}else{
					inter &= false
				}
			}
			if($scope.datasheet_filter.intra_band_nc_CA){
				if(!item.intra_band_nc_CA){
					continue
				}
				if(item.intra_band_nc_CA.indexOf($scope.datasheet_filter.intra_band_nc_CA) >=0){
					inter &= true
				}else{
					inter &= false
				}
			}
			if($scope.datasheet_filter.inter_band_CA){
				if(!item.inter_band_CA){
					continue
				}
				if(item.inter_band_CA.indexOf($scope.datasheet_filter.inter_band_CA) >=0){
					inter &= true
				}else{
					inter &= false
				}
			}

	 		if($scope.datasheet_filter.dual_carrier){ inter &= item.dual_carrier}
	 		if(!inter){continue}
       

			var other = true
			if($scope.datasheet_filter.mimo.length >0){
				other &= $scope.is_in_array(item.mimo, $scope.datasheet_filter.mimo)
			}
			if($scope.datasheet_filter.rohc.length >0){
				other &= $scope.check_bands(item.rohc, $scope.datasheet_filter.rohc)
			}
			if($scope.datasheet_filter.drx.length >0){
				other &= $scope.is_in_array(item.drx, $scope.datasheet_filter.drx)
			}
			if($scope.datasheet_filter.duplex.length >0){
				other &= $scope.is_in_array(item.duplex, $scope.datasheet_filter.duplex)
			}
			if(!other){continue}
			returne_items.push(item.id)
			
		};
    	return returne_items
	};



	var getData = function(){ // przeĹ‚Ä…czanie danych tabeli
		if($scope.openTab =='fs1'){
			return $scope.specifications
		}else{
			var filteredDatasheets_id = $scope.filterDatasheet()
			var filteredSpec = []
			for(var i =0; i <  $scope.specifications.length ;i++ ){
				if(filteredDatasheets_id.indexOf(parseInt($scope.specifications[i].datasheet_id))>=0 ){
					filteredSpec.push($scope.specifications[i])
				}
			}
			return filteredSpec
		}
    } 	

    $scope.initTable = function(){ 
	    $scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 50          // count per page
	    }, {
	        total: getData().length,//function () { return getData().length; }, // length of data
	        getData: function($defer, params) {
	           // var filteredData = getData();   
	            var filteredData = $filter('filter')( getData(), $scope.filter);
	            var orderedData = params.sorting() ?
	                                $filter('orderBy')(filteredData, params.orderBy()) :
	                                filteredData;
	            params.total(orderedData.length);
	            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
	        }//,
	        //$scope: { $data: {} }
	    });  
	    $scope.tableParams.settings().$scope = $scope;  
	}

	$scope.$watch("filter", function (newValue, oldvalue) {
		if($scope.company && $scope.devicemodels && $scope.specifications && $scope.datasheets){
			$scope.tableParams.reload();
 		}
    }, true); 


   $scope.changeLoading();
}) 