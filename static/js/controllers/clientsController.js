/** CLIENT CONTROLLER**/
app.controller("ClientsCtrl", function($scope, $rootScope, $http, $q, $timeout, flash, $filter, Clients, Company, Users, ngTableParams, $dialogs){
	var scope = $scope;

	$scope.clients = Clients.getClients();
	$scope.company = Company.getCompany();
  $scope.users = null;
	// flash.show()
	$scope.flash = flash

	/** CLIENT EDIT FORM VALIDATION **/
	$scope.nameFormsValid  = true;
	$scope.lnameFormValid   = true;
	$scope.companyFormValid   = true;
	$scope.emailFormValid  = true;
	$scope.phoneFormValid = true;

	$scope.setFormValidation= function(value, form){
		switch(form) {
		    case 'name':
		        $scope.nameFormsValid  = value;
		        break;
		    case 'lastname':
		        $scope.lnameFormValid  = value;
		        break;
		    case 'company':
		        $scope.companyFormValid   = value;
		        break;
		    case 'email':
		        $scope.emailFormValid  = value;
		        break;
		    case 'phone':
		        $scope.phoneFormValid = value;
		        break;
		}
	}

	/** TABLE **/
	$scope.filter = {
        firstname: '',
        lastname: '',
        email: ''
    }

	$scope.initTable = function(data){
	 	$scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 50           // count per page
	   	}, {
	        total: data.length, // length of data
	        getData: function($defer, params) {
	            var filteredData = $filter('filter')(data, $scope.filter);
	            var orderedData = params.sorting() ?
                                $filter('orderBy')(filteredData, params.orderBy()) :
                                filteredData;
                params.total(orderedData.length);
            	$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
	        },
	        $scope: $scope
	    });
 	}

 	$scope.$watch("filter", function(newVal, oldVal){
 		if($scope.clients && $scope.company){
 			$scope.tableParams.reload();
 		}
	}, true);


	/* GET CLIENTS*/
	if($scope.clients){
	 	$scope.clients = Clients.getClients();
	 	$scope.company = Company.getCompany();
	 	$scope.initTable($scope.clients)
	}else{
		$scope.clients = Clients.loadClientsData();
	}

	/* GET COMPANY */
	if($scope.company){
	 	$scope.company = Company.getCompany();
	}else{
		$scope.company = Company.loadCompanData();
	}


 	$scope.setClient = function(data){
 		$scope.clients = data;
 	}
	$scope.$on('clientsDataReady', function (event, data) {
		scope.setClient(data)
		Clients.clients = data;
        $scope.initTable(data)
	});

	/** COMPANY **/
	$scope.setCompany = function(data){
 		$scope.company = data;
 	}
	$scope.$on('companyDataReady', function (event, data) {
		scope.setCompany(data)
		Company.company = data;
	});

	/**  REMOVE CLIENT**/
  	$scope.removeClient = function(client){
    	var dlg = null;
        dlg = $dialogs.confirm('Usuń klienta','Czy chcesz usunąć klienta?');
        dlg.result.then(function(btn){
        	$http.post("hideRecord.py", { 'id' : client.id , 'table': 'cbs_client'})
        		.success(function(data) {
        		    client.visible = false;
    		    })
        },function(btn){ });
  	};

 	/**  EDIT CLIENT**/
  	$scope.editId = -1;
  	$scope.setEditId =  function(pid) {
        $scope.editId = pid;
    }

    $scope.selectedClient = null
    $scope.copyClient = null

    $scope.setEditingClient = function(client){
    	$scope.selectedClient = client
    	$scope.copyClient = angular.copy($scope.selectedClient)
    }

    $scope.saveClient = function(client) {
    	if($scope.editId==client.id){
    		if($scope.nameFormsValid && $scope.lnameFormValid && $scope.companyFormValid && $scope.emailFormValid && $scope.phoneFormValid){
	     		if(!angular.equals(client,$scope.copyClient)){
		    		data = {
		    			id: $scope.copyClient.id,
		    			user_id : parseInt($scope.copyClient.user_id),
		    			firstname :$scope.copyClient.firstname,
				    	lastname :$scope.copyClient.lastname,
				    	company_id :$scope.copyClient.company.id,
				    	email :$scope.copyClient.email,
				    	phone :$scope.copyClient.phone
		    		}

		    		$http.post("editRecord.py", { 'id' : client.id, 'table': 'cbs_client', 'data':data})  // edytuj klinta
        				.success(function(data) {

                    if($scope.copyClient.user_id){ //gdy klient jest użytkownikiem, edytuj również dane użytkownika

                        data = {
                          first_name :$scope.copyClient.firstname,
                          last_name :$scope.copyClient.lastname,
                          email :$scope.copyClient.email,
                        }
                      $http.post("editRecord.py", { 'id' : $scope.copyClient.user_id, 'table': 'cbs_auth_user', 'data':data})
                         .success(function(data) {
                            if(data > 0){
                              client.firstname = $scope.copyClient.firstname;
                              client.lastname = $scope.copyClient.lastname;
                              client.company = $scope.copyClient.company;
                              client.email = $scope.copyClient.email;
                              client.phone = $scope.copyClient.phone;
                              $scope.setEditId(-1);
                              $scope.users = Users.getUsers();
                              if($scope.users){  // gdy użytkownicy zostali już załadowanie to zmodyfikuj rekord
                                for(var i=0; i<$scope.users.length;i++){
                                  if($scope.users[i].client_id == $scope.copyClient.id){
                                    $scope.users[i].firstname = $scope.copyClient.firstname;
                                    $scope.users[i].lastname = $scope.copyClient.lastname;
                                    $scope.users[i].email = $scope.copyClient.email;
                                  }
                                }
                              }
                              flash.show("Zaktalizowano dane klienta i użytkownika", "success")
                            }else{
                                flash.show("Nie zaktualizowano danych", "warning")
                            }
                         })
                         .error(function(err){
                            flash.show("Wystąpił błąd podczas aktualizacji danych użytkownika", "error")
                          })
                    }else{      // edycja tylko klienta
                      if(data > 0){
                        client.firstname=$scope.copyClient.firstname;
                        client.lastname=$scope.copyClient.lastname;
                        client.company=$scope.copyClient.company;
                        client.email=$scope.copyClient.email;
                        client.phone=$scope.copyClient.phone;
                        $scope.setEditId(-1);
                        flash.show("Zaktalizowano dane klienta", "success")
                      }else{
                          flash.show("Nie zaktualizowano danych", "warning")
                      }
                    }
    					})
    					.error(function(err){
    						flash.show("Wystąpił błąd podczas aktualizacji danych klienta", "error")
    					})
				}else{
					$scope.setEditId(-1);
					//console.log("no change")
				}
    		}else{
    			flash.show("Wprowadź poprawne dane", "info")
    		}
       	}else{
		  //console.log("select")
    	}
    }

    /**  NEW CLIENT**/
    $scope.newClient ={}

    $scope.newFormValid = null
    $scope.setNewClientFormValidation = function(form){
       $scope.newFormValid = form;
    }

    $scope.submitNewClient = function(form){
     	if(form.email.$invalid){
     		form.email.$pristine = false;
     	}
     	if(form.fname.$invalid){
     		form.fname.$pristine= false;
     	}
     	if(form.lname.$invalid){
     		form.lname.$pristine= false;
     	}
     	if(form.company.$invalid){
     		form.company.$pristine= false;
     	}
     }

    $scope.addNewClient = function(){

      if($scope.newFormValid){
      	var newData={
      		firstname :  $scope.newClient.firstname,
      		lastname :  $scope.newClient.lastname,
      		company_id : $scope.newClient.company.id,
     			email:  $scope.newClient.email,
      		phone:  $scope.newClient.phone,
      		visible: true
      	}

      	$http.post("addRecord.py", {'table': 'cbs_client', 'data':newData})
        	.success(function(data) {
        		if(data > 0){
        			delete newData.company_id
	        		newData.company = scope.newClient.company;
	        		newData.id = parseInt(data)
	        		$scope.clients.push(newData)
					flash.show("Dodano nowego klienta.", "success")
        		}else{
        		    flash.show("Nie dodano nowego klienta.", "warning")
        		}
    		})
    		.error(function(err){
    			flash.show("Wystąpił błąd podczas dodawania nowego klienta!", "error")
    		})

		}else{
			flash.show("Uzupełnij dane.", "info")
		}
	}

});
