app.controller("newTransactionCtrl", function ($scope, Clients, flash, $q, ngTableParams, $dialogs, $filter, $http, $timeout, Transaction, SubTransactionDevice, SubTransactionSIM, Operations, Session, Devices, Company, DevicesTypes, DevicesModels, DevicesStates, SIM) {

    "use strict";
    $scope.session = Session;

    $scope.addedDev = [];
    $scope.addedSim = [];
    $scope.sendedEmail = false;

    $scope.flash = flash;
    $scope.step = [{ class: 'active', id: 1}, {class: '', id: 2}, {class: '', id: 3}, {class: '', id: 4}, {class: '', id: 5}]
    $scope.activStep = $scope.step[0];

    $scope.selectedDevise = false;

    $scope.changeStep = function (atr, type) { // prześcia pomiędzy zakładkami wizarda
        switch (type) {
        case 'arrow':
            if (atr > 0) {
                var index = $scope.step.indexOf($scope.activStep)
            //if($scope.firstStepValid()){
            if (index==0 && !$scope.firstStepValid()) {
                    flash.show("Wprowadź poprawne dane", "info");
                    return;
                }

            if ( index==1  && !$scope.selectedDevise) {
                    flash.show("Wybjerz przynajmniej jedno urządzenie ", "info");
                    return;
                }
                if (index == 3  && !$scope.selectedDevise) {
                    flash.show("Wybjerz przynajmniej jedno urządzenie ", "info");
                    return;
					           }
            if(index == 1  && $filter('filter')($scope.devices, {$selected: true}).length == 0 && $filter('filter')($scope.sim, {$selected: true}).length == 0) {
                    flash.show("Wybjerz przynajmniej jedno urządzenie ", "info");
                    return;
                }
                $scope.activStep = $scope.step[index + atr];
                $scope.activStep.class = "active";
                angular.forEach($scope.step.slice(0, $scope.activStep.id-1), function(value, key) {
                    value.class = "complete";
					    }, null);
					    angular.forEach($scope.step.slice($scope.activStep.id, $scope.step.length), function(value, key) {
                    value.class = "";
                },  null);
                /*}else{
                	flash.show("Wprowadź poprawne dane", "info")
                }*/
            } else {
                var index = $scope.step.indexOf($scope.activStep);
                $scope.activStep = $scope.step[index + atr];
                $scope.activStep.class = "active";
                angular.forEach($scope.step.slice(0, $scope.activStep.id - 1), function (value, key) {
                    value.class="complete";
                }, null);
                angular.forEach($scope.step.slice($scope.activStep.id, $scope.step.length), function(value, key) {
                    value.class = "";
                }, null);
            }

            if ($scope.step.indexOf($scope.activStep) === ($scope.step.length - 1)) {
                $scope.addNewTransaction();
                //$timeout(update, 200);
            }
            break;
        case 'click':
				if($scope.step.indexOf($scope.activStep)!=($scope.step.length-1) && atr.class=='complete'){
					$scope.activStep = atr
					$scope.activStep.class = "active"
					angular.forEach($scope.step.slice(0, $scope.activStep.id-1), function(value, key) {
				           value.class="complete"
				    },  null);
				    angular.forEach($scope.step.slice($scope.activStep.id, $scope.step.length), function(value, key) {
				           value.class=""
				    },  null);
				}
				break;
		}
	}

	$scope.transactions = Transaction.getTransactions()
	$scope.subtransactionsdevice = SubTransactionDevice.getSubTransactionsDevice()
	$scope.subtransactionssim = SubTransactionSIM.getSubTransactionsSim()

	$scope.devices = Devices.getDevices()
	$scope.devicetypes = DevicesTypes.getDeviceTypes()
	$scope.devicemodels = DevicesModels.getDeviceModels()
	$scope.devicestates = DevicesStates.getDeviceState()
	$scope.company = Company.getCompany()
	$scope.sim = SIM.getSim()

	$scope.clients = Clients.getClients();
	$scope.operations = Operations.getOperations();

	if(!$scope.devicestates || !$scope.devicetypes || !$scope.devicemodels || !$scope.clients || !$scope.operations || !$scope.company ){
		$scope.pageLoading = true;
	}

	if($scope.devices){
		for(var i =0;i<$scope.devices.length;i++ ){
			$scope.devices[i].$selected = false;
		}
	}

	if($scope.devices && $scope.devicetypes){
		for(var i = 0; i<$scope.devicetypes.length;i++ ){
			var clie = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.client_id)}, true );
 			var lab = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.LAB) }, true );
			$scope.devicetypes[i].len = clie.concat(lab).length;
		}
		var clie = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.client_id)}, true );
	 	var lab = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.LAB) }, true );
	 	$scope.alldev = clie.concat(lab).length;
	}

	if($scope.sim){
		var clie = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.client_id)}, true );
 		var lab = $filter('filter')($scope.sim, { holder_id: parseInt($scope.session.user.LAB) }, true );
 		$scope.allsim = clie.concat(lab).length;
	}

/*GET CLIENTS ****************/
	if(!$scope.clients){
	 	$scope.clients = Clients.loadClientsData();
	}
	$scope.$on('clientsDataReady', function (event, data) {
		if($scope.devices && !$scope.devices[0].owner){
	      for(var i =0; i<$scope.devices.length;i++){
	        $scope.devices[i].holder = $scope.getItem(data,  $scope.devices[i].holder_id)
	        $scope.devices[i].owner = $scope.getItem(data,  $scope.devices[i].owner_id)
	      }
	    }
	    if($scope.sim && !$scope.sim[0].owner){
	      for(var i =0; i<$scope.sim.length;i++){
	        $scope.sim[i].holder = $scope.getItem(data,  $scope.sim[i].holder_id)
	        $scope.sim[i].owner = $scope.getItem(data,  $scope.sim[i].owner_id)
	      }
	    }
		$scope.clients = data;
		Clients.clients = data;
		if(Session.client){
			$scope.fromWho = Session.client
			$scope.fromWhoText = $scope.fromWho.firstname+' '+$scope.fromWho.lastname
		}
		$scope.changeLoading(1)
	});

/*GET OPERATIONS ****************/
	if(!$scope.operations){
	 	$scope.operations = Operations.loadOperationsData();
	}else{
		$scope.operation = $scope.operations[0]
	}
	$scope.$on('operationsDataReady', function (event, data) {
		$scope.operations = data;
		$scope.operation = data[0]
		Operations.operations = data;
		$scope.changeLoading(1)
	});

/*GET DEVICES ****************/
    $scope.$on('devicesDataReady', function (event, data) {
		if($scope.devicetypes || $scope.devicemodels || $scope.devicestates || $scope.company || $scope.clients){
			for(var i = 0; i<data.length;i++ ){
				if($scope.devicetypes){ data[i].type = $scope.getItem($scope.devicetypes, data[i].type_id) }
				if($scope.devicemodels){data[i].model = $scope.getItem($scope.devicemodels, data[i].model_id)}
				if($scope.devicestates){data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)}
				if($scope.company){data[i].company = $scope.getItem($scope.company, data[i].company_id)}
				if($scope.clients){data[i].holder = $scope.getItem($scope.clients, data[i].holder_id)}
				if($scope.clients){data[i].owner = $scope.getItem($scope.clients, data[i].owner_id)}
				if(!data[i].sn){data[i].sn=""}				// jak był null to chowałe je przy filtrowniu
				if(!data[i].imei){data[i].imei=""}
				if(!data[i].mac){data[i].mac=""}
				//data[i].&selected = false;
			}
			if($scope.devicetypes){
				for(var i = 0; i<$scope.devicetypes.length;i++ ){
					var clie = $filter('filter')(data, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.client_id)}, true );
	 				var lab = $filter('filter')(data, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.LAB) }, true );
					$scope.devicetypes[i].len = clie.concat(lab).length;
				}
			}
			var clie = $filter('filter')(data, {holder_id: parseInt($scope.session.user.client_id)}, true );
	 		var lab = $filter('filter')(data, {holder_id: parseInt($scope.session.user.LAB) }, true );
	 		$scope.alldev = clie.concat(lab).length;

		}
			//holderDevices
		$scope.devices = data ;
		Devices.devices = data;
		$scope.changeLoading(2)
		$scope.initTable()
	});

/*GET DEVICES TYPES ****************/
  	if(!$scope.devicetypes){
	 	$scope.devicetypes = DevicesTypes.loadDeviceTypesData();
	}
	$scope.$on('deviceTypesDataReady', function (event, data) {
		if($scope.devices && !$scope.devices[0].type){
	      for(var i =0; i<$scope.devices.length;i++){
	        $scope.devices[i].type = $scope.getItem(data,  $scope.devices[i].type_id)
	      }
	    }
	    if($scope.devices){
			for(var i = 0; i<data.length;i++ ){
				var clie = $filter('filter')($scope.devices, {type_id: data[i].id, holder_id: parseInt($scope.session.user.client_id)}, true );
	 			var lab = $filter('filter')($scope.devices, {type_id: data[i].id, holder_id: parseInt($scope.session.user.LAB) }, true );
				data[i].len = clie.concat(lab).length;
			}
		}

		$scope.devicetypes = data;
		DevicesTypes.devicetypes = data;
		$scope.changeLoading(1)
	});

/*GET DEVICES MODELS ****************/
  	if(!$scope.devicemodels){
	 	$scope.devicemodels = DevicesModels.loadDeviceModelsData();
	}
	$scope.$on('deviceModelsDataReady', function (event, data) {
		if($scope.devices && !$scope.devices[0].model){
	      for(var i =0; i<$scope.devices.length;i++){
	        $scope.devices[i].model = $scope.getItem(data,  $scope.devices[i].model_id)
	      }
	    }
		$scope.devicemodels = data;
		DevicesModels.devicemodels = data;
		$scope.changeLoading(1)
	});

/*GET DEVICES STATES ****************/
  	if(!$scope.devicestates){
	 	$scope.devicestates = DevicesStates.loadDeviceStateData();
	}
	$scope.$on('deviceStatesDataReady', function (event, data) {
		if($scope.devices && !$scope.devices[0].state){
	      for(var i =0; i<$scope.devices.length;i++){
	        $scope.devices[i].state = $scope.getItem(data,  $scope.devices[i].state_id)
	      }
	    }
	    if($scope.sim && !$scope.sim[0].state){
	      for(var i =0; i<$scope.sim.length;i++){
	        $scope.sim[i].state = $scope.getItem(data,  $scope.sim[i].state_id)
	      }
	    }
		$scope.devicestates = data;
		DevicesStates.devicestates = data;
		$scope.changeLoading(1)
	});

/*GET COMPANY ****************/
  	if(!$scope.company){
	 	$scope.company = Company.loadCompanData();
	}
	$scope.$on('companyDataReady', function (event, data) {
		if($scope.devices && !$scope.devices[0].company){
	      for(var i =0; i<$scope.devices.length;i++){
	        $scope.devices[i].company = $scope.getItem(data,  $scope.devices[i].company_id)
	      }
	    }
	    if($scope.sim && !$scope.sim[0].company){
	      for(var i =0; i<$scope.sim.length;i++){
	        $scope.sim[i].company = $scope.getItem(data,  $scope.sim[i].company_id)
	      }
	    }
		$scope.company = data;
		Company.company = data;
		$scope.changeLoading(1)
	});

/*GET SIM ****************/
	$scope.$on('simsDataReady', function (event, data) {
		if($scope.company || $scope.devicestates || $scope.clients){
			for(var i = 0; i<data.length;i++ ){
				if($scope.devicestates){ data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)}
				if($scope.company){data[i].company = $scope.getItem($scope.company, data[i].company_id)}
				if($scope.clients){data[i].holder = $scope.getItem($scope.clients, data[i].holder_id)}
				if($scope.clients){data[i].owner = $scope.getItem($scope.clients, data[i].owner_id)}
				if(!data[i].pin){data[i].pin=""}
				if(!data[i].pin2){data[i].pin2=""}
				if(!data[i].puk){data[i].puk=""}
				if(!data[i].puk2){data[i].puk2=""}
				if(!data[i].imsi){data[i].imsi=""}
				if(!data[i].iccid){data[i].iccid=""}
				if(!data[i].msisdn){data[i].msisdn=""}
			}
		}
		var clie = $filter('filter')(data, {holder_id: parseInt($scope.session.user.client_id)}, true );
 		var lab = $filter('filter')(data, { holder_id: parseInt($scope.session.user.LAB) }, true );
 		$scope.allsim = clie.concat(lab).length;

		$scope.sim = data;
		SIM.sim = data;
		$scope.changeLoading(2)
	});

	$scope.changeLoading = function(step){	// sprawdź czy już można urkyć wizualizację ładowania
		switch(step){
			case 1:
				if($scope.devicestates && $scope.devicetypes && $scope.devicemodels && $scope.clients && $scope.operations && $scope.company){
					$scope.pageLoading = false;
				}
				break;
			case 2:
				if($scope.devicestates && $scope.devicetypes && $scope.devicemodels && $scope.clients && $scope.operations && $scope.company && $scope.devices && $scope.sim){
					$scope.pageLoading = false;
				}
				break
		}
	}

	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id == id){
				return src[i]
			}
		}
	}

/*******   SETP 1*********/

	if(Session.client){
		$scope.fromWho = Session.client
		$scope.fromWhoText = $scope.fromWho.firstname+' '+$scope.fromWho.lastname
	}else{
		$scope.fromWho = null
		$scope.fromWhoText = " "
	}
 	$scope.toWho = null;
 	//$scope.operation = null
	//$scope.begin =  new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate();
	$scope.begin = $filter('date')(new Date(), 'yyyy-MM-dd')
	$scope.end = '';

	$scope.endValid=true;
	$scope.toWhoText = " ";

	$scope.$watch("fromWhoText", function(newVal, oldVal){
 		if(newVal && (typeof newVal == "object")){
	    	$scope.fromWhoText = (newVal.firstname.toString()+' '+newVal.lastname.toString())
	    	$scope.fromWho = angular.copy(newVal);
 		}
  	}, true);

  	$scope.$watch("toWhoText", function(newVal, oldVal){
 		if(newVal && (typeof newVal == "object")){
	    	$scope.toWhoText = (newVal.firstname.toString()+' '+newVal.lastname.toString())
	    	$scope.toWho = angular.copy(newVal);
 		}
  	}, true);

  	$scope.firstStepValid = function(){
  		var valid = true;

  		if(!$scope.fromWho || (typeof $scope.fromWho != "object")){
  			valid = false;
  			$scope.fromWhoText = "";
  		}else{
  			if($scope.fromWho && !angular.equals($scope.fromWhoText,($scope.fromWho.firstname+' '+$scope.fromWho.lastname))){
  				valid = false;
  				$scope.fromWhoText = "";
  			}
  		}

  		if(!$scope.toWho || (typeof $scope.toWho != "object")){
  			valid = false;
  			$scope.toWhoText = "";
  		}else{
  			if($scope.toWho && !angular.equals($scope.toWhoText,($scope.toWho.firstname+' '+$scope.toWho.lastname))){
  				valid = false;
  				$scope.toWhoText = "";
  			}
  		}

  		if(!$scope.operation){
  			$scope.operation  = null
  			valid = false;
  		}

  		if(!$scope.begin){
  			$scope.begin = "";
  			valid = false;
  		}/*else{
  				 $filter('date')(new Date(), 'yyyy-MM-dd')
  			var today= new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate()
  			if(!angular.equals($scope.begin,today) && !($scope.begin instanceof Date)){
	  			if(!($scope.begin instanceof Date)){
	  				$scope.begin = "";
	  				valid = false;
	  			}
  			}
  		}
	*/
  		if(!$scope.end){
  			$scope.end = null
  			$scope.endValid=false
  			valid = false;
  		}else{
  			 if(!($scope.end instanceof Date)){
  			 	$scope.endValid=false
	  			$scope.end = null
  				valid = false;
	  		}else{
	  			$scope.endValid=true
	  		}
  		}
  		if(angular.equals($scope.fromWho,$scope.toWho)){
  			valid = false;
  			$scope.toWhoText = "";
  		}

  		if(!$scope.devices){
	 		$scope.devices = Devices.loadDevicesData();
		}
		if(!$scope.sim){
	 		$scope.sim = SIM.loadSimData();
		}
		if(valid && (!$scope.devices || !$scope.sim)){
			$scope.pageLoading = true;
		}
		if($scope.devices && $scope.sim){
			$scope.pageLoading = false;
		}
  		return valid;
  	}

/*******   SETP 2 *********/

	$scope.filter = {
    }

	$scope.selectedType = 0
	$scope.selectTyppe = function(type_id){
		$scope.selectedType = type_id;
		if($scope.devices){
			$scope.tableParams.reload();
		}
        $scope.filter = {}

	}

	$scope.columns =   // widoczność kolumn w tabeli
			{cbs_id:true, id: false, model: true, company: true, sn: true, imei:true, mac:true,
			 pin:false, pin2:false, puk:false, puk2:false, msisdn:true, imsi:true, iccid:true, description:false,
			 holder:true, owner:true, state:true};


 	var getData = function(){ // przełączanie danych tabeli
 		if($scope.selectedType ==0){
 			var clie = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.client_id)}, true );
 			var lab = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.LAB) }, true );
 			return clie.concat(lab);
 		}else if($scope.selectedType == -1){
 			var clie = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.client_id)}, true );
 			var lab = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.LAB) }, true );
 			return clie.concat(lab);
 		}else{
 			var clie = $filter('filter')($scope.devices, {type_id: $scope.selectedType, holder_id: parseInt($scope.session.user.client_id)}, true );
 			var lab = $filter('filter')($scope.devices, {type_id: $scope.selectedType, holder_id: parseInt($scope.session.user.LAB) }, true );
 			return  clie.concat(lab);
 		}
    }

    $scope.initTable = function(){
	    $scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 50          // count per page
	    }, {
	        total: getData().length,//function () { return getData().length; }, // length of data
	        getData: function($defer, params) {
	           // var filteredData = getData();
	            var filteredData = $filter('filter')( getData(), $scope.filter);
	            var orderedData = params.sorting() ?
	                                $filter('orderBy')(filteredData, params.orderBy()) :
	                                filteredData;
	             params.total(orderedData.length);
	            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
	        }//,
	        //$scope: { $data: {} }
	    });
	    $scope.tableParams.settings().$scope = $scope;
	}


	if($scope.devices && $scope.clients && $scope.sim){ // gdy dane nie wymagają ładowania to od razu wyświetl tbelę
  		$scope.initTable()
	}

	$scope.$watch("filter", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if($scope.activStep === $scope.step[1]){
			$scope.tableParams.reload();
 		}
	}, true);

/*******   SETP 3 *********/


	$scope.changeSelection = function() {
		if($filter('filter')($scope.devices, {$selected:true}).length ==0 && $filter('filter')($scope.sim, {$selected:true}).length ==0){
			$scope.selectedDevise = false
		}else{
			$scope.selectedDevise = true
		}

    }

	$scope.notification_email = true

    $scope.options = {
      height: 150,
      width: 500,
      lang:"pl-PL",
      disableDragAndDrop: true,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
    };

	    $scope.init = function() {  };
	    $scope.enter = function() {  };
	    $scope.focus = function(e) {  };
	    $scope.blur = function(e) { };
	    $scope.paste = function() {  };
	    $scope.change = function(contents, editable$) {
	    	//$scope.test = contents;
	    	//console.log('contents are changed:', contents, $scope.test);
	    };
	    $scope.keyup = function(e) { /*console.log('Key is released:', e.keyCode); */ };
	    $scope.keydown = function(e) { /*console.log('Key is pressed:', e.keyCode); */};
	    $scope.imageUpload = function(files, editor, $editable) {
	       // console.log('image upload:', files, editor, $editable);
    };

/*******   SETP 5 *********/
	$scope.progress = 0;

	$scope.$watch("progress", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if($scope.progress >= 100){
			flash.show("Dodano nową transakcję", "success")
			if($scope.notification_email && !$scope.sendedEmail){
				$scope.sendEmail(false,true, "newTransaction")
			}
 		}
	}, true);

	function update() {
  		var n = 1
  		n +=  $filter('filter')($scope.devices, {$selected:true}).length
  		n += $filter('filter')($scope.sim, {'$selected':true}).length
  		if($scope.progress!=100){
  			$scope.progress += 100/n
  		}
  	}


	$scope.addSubTransactionDevice = function(items, transaction_id){

		var item;
		if(items.length==0){
			return
		}else{
			items[0].$selected = false
			item = items[0]
			items.splice(0, 1);
		}
		var newData={
      		transaction_id :  transaction_id,
      		device_id : item.id,
      		returned : false
      	}

      	var  devData={
      		holder_id : $scope.toWho.id,
      	}
		switch($scope.operation.id){
			case 1: // wypożyczenie
				break;
			case 2: // zmiana właściciela
				devData.owner_id = $scope.toWho.id
				break;
		}

		$http.post("addRecord.py", {'table': 'cbs_subtransaction_device', 'data':newData})
        	.success(function(data) {
        		newData.id=parseInt(data)
        		if($scope.subtransactionsdevice){
        			$scope.subtransactionsdevice.push(newData)
        		}

        		$http.post("editRecord.py", {'table': 'cbs_device', 'data':devData, 'id': item.id})
        			.success(function(data) {
        				item.holder_id = $scope.toWho.id
        				item.holder= $scope.toWho
        				if($scope.operation.id ==2){ // zmiana właściciela
        					item.owner_id = $scope.toWho.id
        					item.owner= $scope.toWho
        				}

        				$timeout(update, 0);
        				$scope.addSubTransactionDevice(items, transaction_id)
        			})
        			.error(function(err){
    					flash.show("Wystąpił błąd podczas dodawania nowej podtransakcji!", "error")
    				})
        	})
        	.error(function(err){
    			flash.show("Wystąpił błąd podczas dodawania nowej podtransakcji!", "error")
    		})
	}

	$scope.addSubTransactionSim = function(items, transaction_id){
	 	var item;
		if(items.length==0){
			return
		}else{
			items[0].$selected = false;
			item = items[0];
			items.splice(0, 1);
		}

		var newData={
      		transaction_id :  transaction_id,
      		sim_id : item.id,
      		returned : false
      	}

      	var  devData={
      		holder_id : $scope.toWho.id,
      	}
		switch($scope.operation.id){
			//case 1: // wypożyczenie
			//	break;
			case 2: // zmiana właściciela
				devData.owner_id = $scope.toWho.id
				break;
		}
		$http.post("addRecord.py", {'table': 'cbs_subtransaction_sim', 'data':newData})
        	.success(function(data) {
        		newData.id=parseInt(data)
        		if($scope.subtransactionssim){
        			$scope.subtransactionssim.push(newData)
        		}

        		$http.post("editRecord.py", {'table': 'cbs_sim', 'data':devData, 'id': item.id})
        			.success(function(data) {
        				item.holder_id = $scope.toWho.id
        				item.holder= $scope.toWho
        				item.$selected = false
        				if($scope.operation.id ==2){ // zmiana właściciela
        					item.owner_id = $scope.toWho.id
        					item.owner= $scope.toWho
        				}

        				$timeout(update, 0);
        				$scope.addSubTransactionSim(items, transaction_id)
        			})
        			.error(function(err){
    					flash.show("Wystąpił błąd podczas dodawania nowej podtransakcji!", "error")
    				})

        	})
        	.error(function(err){
    			flash.show("Wystąpił błąd podczas dodawania nowej podtransakcji!", "error")
    		})
	}

	$scope.newtransaction = {}


/*************************************************************/
  	$scope.addNewTransaction = function(){

  	 	var newData={
      		from_id :  $scope.fromWho.id,
      		to_id :  $scope.toWho.id,
      		operation_id : $scope.operation.id,
     		beginning: $filter('date')(new Date($scope.begin), 'yyyy-MM-dd'),  //date :'yyyy-MM-dd' date :'yyyy-MM-dd'
      		ending:  $filter('date')(new Date($scope.end), 'yyyy-MM-dd'),//new Date($scope.end),
      		is_open: true,
      		description: $scope.description,
      		visible: true,
      		notification_email: $scope.notification_email,
      	}
      	if($scope.operation.id==2){ // zmiana właściciela
      		newData.is_open = false
      	}

  		$http.post("addRecord.py", {'table': 'cbs_transaction', 'data':newData})
        	.success(function(data) {
        		if($scope.transactions){
        			newData.from = $scope.fromWho;
        			newData.to = $scope.toWho;
        			newData.operation = $scope.operation;
        			newData.id = parseInt(data)

        			$scope.newtransaction = newData

        			$scope.transactions.push(newData)
        		}

        		var tempD = $filter('filter')($scope.devices, {$selected:true})

        		$scope.addedDev = angular.copy(tempD)  //$filter('filter')($scope.devices, {$selected:true})
        		$scope.addSubTransactionDevice(tempD, parseInt(data))

				var tempS = $filter('filter')($scope.sim, {$selected:true})
				$scope.addedSim = angular.copy(tempS)
        		$scope.addSubTransactionSim(tempS, parseInt(data))

              	$timeout(update, 200);
    		})
    		.error(function(err){
    			flash.show("Wystąpił błąd podczas dodawania nowej transakcji!", "error")
    		})
  	};




  	$scope.sendEmail = function(asking,alldev, type){
	    var send = function(asking,alldev, type){
	      $scope.sendingEmail = true
	      var devidlist = "0_"
	      for(var i =0; i<$scope.addedDev.length;i++ ){
	        devidlist += $scope.addedDev[i].id+"_"
	      }
	      devidlist = devidlist.substring(0, devidlist.length - 1);

	      var simidlist  = "0_"
	      for(var i =0; i<$scope.addedSim.length;i++ ){
	        simidlist += $scope.addedSim[i].id+"_"
	      }
	      simidlist = simidlist.substring(0, simidlist.length - 1);

	      if(new Date($scope.end) < new Date()){
	        var expire = "true"
	      }else{
	        var expire = ""
	      }

      	 var oper = "borow"
	      if($scope.operation.id>1){
	        oper = "pass"
	      }
	      var  emaildata ={
	        "fromwho": $scope.fromWho.firstname+" "+$scope.fromWho.lastname,
	        'fromemail':$scope.fromWho.email,
	        'fromcompany':$scope.fromWho.company.name,
	        'fromphone':$scope.fromWho.phone,


	        "towho":$scope.toWho.firstname+" "+$scope.toWho.lastname,
	        'toemail':$scope.toWho.email,
	        'tocompany':$scope.toWho.company.name,
	        'tophone':$scope.toWho.phone,

	        "trid": $scope.newtransaction.id,
	        "beginning": $filter('date')(new Date($scope.begin), 'yyyy-MM-dd'),
	        "ending": $filter('date')(new Date($scope.end), 'yyyy-MM-dd'),
	        "operation": oper,
	        "expire" :expire,
	        "msgtype":type ,
	        "dev":devidlist,
	        "sim":simidlist,
	      }
	      $http.post("sendEmail.py", emaildata)
	        .success(function(data){
	            $scope.sendingEmail = false
	            $scope.sendedEmail = true
	            if(data.toString()=="True"){
	              if(asking){
	                flash.show("Wysłano powiadomienie.", "success")
	              }
	            }else{
	               flash.show("Nie wysłano powiadomnienia !", "info")
	            }

	        })
	        .error(function(err){
	          flash.show("Wystąpił błąd podczas wysyłania emaila!", "error")
	        })
	    }
	    if(asking){
	      var dlg = null;
	        dlg = $dialogs.confirm('Wysłać email','Czy chcesz wysłać email z przypomnieniem?');
	        dlg.result.then(function(btn){
	          send(asking,alldev, type)
	      },function(btn){ });
	    }else{
	      send(asking,alldev, type)
	    }
 	}

})

app.directive('dynamic', function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
});


app.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);
    for (var i=1; i<total; i++)
      input.push(i);
    return input;
  };
});
