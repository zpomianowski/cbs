app.controller("transferSimCtrl", function($scope,$q, $http, flash,$filter, $dialogs, ngTableParams, SIM, Transfer, DevicesStates, Clients, Company, Session, USER_ROLES, AuthService, limitToFilter, XLSXReaderService){
	$scope.flash = flash
	$scope.pageLoading = true;

	$scope.devicestates = DevicesStates.getDeviceState()
	$scope.company = Company.getCompany()
	$scope.clients = Clients.getClients();
 	$scope.sim = SIM.getSim()

 	$scope.transfer = Transfer.getSimTransfer()

 	/*GET CLIENTS ****************/ 
		if(!$scope.clients){ 
		 	$scope.clients = Clients.loadClientsData();
		}  
		$scope.$on('clientsDataReady', function (event, data) {
		    if($scope.sim && !$scope.sim[0].owner){
		      for(var i =0; i<$scope.sim.length;i++){
		        $scope.sim[i].holder = $scope.getItem(data,  $scope.sim[i].holder_id)
		        $scope.sim[i].owner = $scope.getItem(data,  $scope.sim[i].owner_id)
		      }
		    }
			$scope.clients = data;
			Clients.clients = data;
			$scope.changeLoading()
		});

	/* STATE ****************/ 
		if(!$scope.devicestates){
		 	$scope.devicestates = DevicesStates.loadDeviceStateData();
		}  
		$scope.$on('deviceStatesDataReady', function (event, data) {
		    if($scope.sim && !$scope.sim[0].state){
		      for(var i =0; i<$scope.sim.length;i++){
		        $scope.sim[i].state = $scope.getItem(data,  $scope.sim[i].state_id)
		      }
		    } 
			$scope.devicestates = data;
			DevicesStates.devicestates = data;
			$scope.changeLoading()
		});

	/*GET COMPANY ****************/
	  	if(!$scope.company){
		 	$scope.company = Company.loadCompanData();
		}  
		$scope.$on('companyDataReady', function (event, data) {
		    if($scope.sim && !$scope.sim[0].company){
		      for(var i =0; i<$scope.sim.length;i++){
		        $scope.sim[i].company = $scope.getItem(data,  $scope.sim[i].company_id)
		      }
		    }  
			$scope.company = data;
			Company.company = data;
			$scope.changeLoading()
		});

	/*GET SIM ****************/
		if(!$scope.sim){ 
		 	$scope.sim = SIM.loadSimData();
		} 
		$scope.$on('simsDataReady', function (event, data) {
			if($scope.company || $scope.devicestates || $scope.clients){
				for(var i = 0; i<data.length;i++ ){
					if($scope.devicestates){ data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)}
					if($scope.company){data[i].company = $scope.getItem($scope.company, data[i].company_id)}
					if($scope.clients){data[i].holder = $scope.getItem($scope.clients, data[i].holder_id)}
					if($scope.clients){data[i].owner = $scope.getItem($scope.clients, data[i].owner_id)}
					if(!data[i].pin){data[i].pin=""}
					if(!data[i].pin2){data[i].pin2=""}
					if(!data[i].puk){data[i].puk=""}
					if(!data[i].puk2){data[i].puk2=""}
					if(!data[i].imsi){data[i].imsi=""}
					if(!data[i].iccid){data[i].iccid=""}
					if(!data[i].msisdn){data[i].msisdn=""}
				}
			}
		 
			if($scope.transfer && $scope.transfer.length && !$scope.transfer[0].sim){
		      for(var i =0; i<$scope.transfer.length;i++){
		        $scope.transfer[i].sim = $scope.getItem(data,  $scope.transfer[i].sim_id)
		      }
		    } 
			$scope.sim = data;
			SIM.sim = data;
			$scope.changeLoading()
		});

	/* TRANSFER ****************/ 
		if(!$scope.transfer){
		 	$scope.transfer = Transfer.loadSimTransferData();
		}  
		$scope.$on('simsTransfersDataReady', function (event, data) {
		    if($scope.sim){
		      for(var i =0; i<data.length;i++){
		      	 data[i].sim =$scope.getItem($scope.sim,  data[i].sim_id)
		      }
		    }
			$scope.transfer = data;
			Transfer.transfer = data;
			$scope.filteredTransfer = $scope.transfer
			$scope.convertTransferToChart($scope.filteredTransfer)
			$scope.changeLoading()
		});

	//


	$scope.months = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj','Czerwiec','Lipiec','Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień']

	$scope.changeLoading = function(){	// sprawdź czy już można urkyć wizualizację ładowania 
		if($scope.devicestates && $scope.clients && $scope.company && $scope.sim  && $scope.transfer){
			$scope.pageLoading = false;
		 	$scope.initTable();
		}else{
			$scope.pageLoading = true;
		}
	}
	

	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id == id){
				return src[i]
			}
		}
	} 

	/*TABELA Z TRANSFERAMI  ****************/ 
		$scope.filter ={	 
		}
		
		$scope.customFilterParam ={
			
		}	

		 

		$scope.getData = function(){ // przełączanie danych tabeli
			var tempfilteredTransfer = $filter('filter')( $scope.transfer, $scope.filter); 

			var temp =  []
			if($scope.customFilterParam.fromyear || $scope.customFilterParam.frommonth || $scope.customFilterParam.toyear || $scope.customFilterParam.tomonth){
				temp = []
				angular.forEach( tempfilteredTransfer, function(value, key) {
					if(($scope.customFilterParam.toyear || $scope.customFilterParam.tomonth) && ($scope.customFilterParam.fromyear || $scope.customFilterParam.frommonth)){
						if($scope.customFilterParam.fromyear){var year = $scope.customFilterParam.fromyear}else{var year = 2011}
						if($scope.customFilterParam.frommonth){var month = $scope.customFilterParam.frommonth-1}else{var month = 0}
						var beginDate = new Date(year, month, 1, 0, 0, 0, 0)

						if($scope.customFilterParam.toyear){var year = $scope.customFilterParam.toyear}else{var year = new Date().getFullYear()}
						if($scope.customFilterParam.tomonth){var month = $scope.customFilterParam.tomonth-1}else{var month = 11}
						var endDate = new Date(year, month, 28, 0, 0, 0, 0)   
						
						if( (endDate >= new Date(value.year, value.month-1, 27, 0, 0, 0, 0)) && (beginDate <= new Date(value.year, value.month-1, 2, 0, 0, 0, 0) )){
							temp.push(value)		
						}
					}else if($scope.customFilterParam.toyear || $scope.customFilterParam.tomonth){
						if($scope.customFilterParam.toyear){var year = $scope.customFilterParam.toyear}else{var year = new Date().getFullYear()}
						if($scope.customFilterParam.tomonth){var month = $scope.customFilterParam.tomonth-1}else{var month = 11}
						var endDate = new Date(year, month, 28, 0, 0, 0, 0)   
						if( endDate >= new Date(value.year, value.month-1, 27, 0, 0, 0, 0) ){
							temp.push(value)	
						}
					}else if($scope.customFilterParam.fromyear || $scope.customFilterParam.frommonth){
						if($scope.customFilterParam.fromyear){var year = $scope.customFilterParam.fromyear}else{var year = 2011}
						if($scope.customFilterParam.frommonth){var month = $scope.customFilterParam.frommonth-1}else{var month = 0}
						var beginDate = new Date(year, month, 1, 0, 0, 0, 0)

						if(beginDate <= new Date(value.year, value.month-1, 2, 0, 0, 0, 0) ){
							temp.push(value)	
						}
					}


				});

	
				$scope.filteredTransfer = temp 

			}else{
				$scope.filteredTransfer = tempfilteredTransfer
			}
		}

		$scope.initTable = function(){ 
		    $scope.tableParams = new ngTableParams({
		        page: 1,            // show first page
		        count: 50,          // count per page
		        sorting:{
		        	id: 'asc'
		        }
		    }, {
		        total:  $scope.filteredTransfer.length, //$scope.transactions.length,     // length of data
		        getData: function($defer, params) {
		           // var filteredData = getData();   
		            var filteredData = $scope.filteredTransfer; //$filter('filter')( $scope.filteredTransfer, $scope.filter);  //getData()
		            var orderedData = params.sorting() ?
		                                $filter('orderBy')(filteredData, params.orderBy()) :
		                                filteredData;
		            params.total(orderedData.length);
		            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
		        },
		        //$scope: { $data: {} }
		    });  
		    $scope.tableParams.settings().$scope = $scope; 
		}
	// END TABELA

	$scope.displayTransfer ={
		download: 0,
		upload: 0,
		total:0,
		unit: "GB"
	}

	

 	$scope.xseries = []
	$scope.transfersChart = []

    $scope.convertTransferToChart = function(items){
    	$scope.xseries = []
		$scope.transfersChart = []
    	var transfers = $filter('orderBy')(items, ['year','month'])
    	if(!transfers){ return}
    	var download ={
    		name: 'Downlink',
    		data: []
    	}
    	var upload ={
    		name: 'Uplink',
    		data: []
    	}
    	var total ={
    		name: 'Total',
    		data: []
    	}

    	$scope.displayTransfer.download = 0
    	$scope.displayTransfer.upload = 0
    	$scope.displayTransfer.total = 0

    	for(var i = 0; i<transfers.length;i++ ){
    		var x = transfers[i].month+ "/"+ transfers[i].year.toString().slice(-2);
    		var index = $scope.xseries.indexOf(x)
    	 	if(index>=0){
    	 		download.data[index] += transfers[i].downlink ;
    	 		upload.data[index] += transfers[i].uplink ;
    	 		total.data[index] +=  transfers[i].total;

    	 	}else{
    	 		$scope.xseries.push(x)
    	 		download.data.push(  transfers[i].downlink ) 
    	 		upload.data.push(  transfers[i].uplink ) 
    	 		total.data.push(  transfers[i].total ) 
    	 	}	
    	}

    	for(var i = 0; i<download.data.length;i++){
    		download.data[i] = download.data[i]/1000	
    		upload.data[i] = upload.data[i]/1000
    		total.data[i] = total.data[i]/1000
    		$scope.displayTransfer.download += download.data[i] 
    		$scope.displayTransfer.upload += upload.data[i] 
    		$scope.displayTransfer.total += total.data[i] 
    	}

    	console.log($scope.displayTransfer.total)
    	if($scope.displayTransfer.total > 1000){
    		$scope.displayTransfer.unit = "TB"
    		$scope.displayTransfer.download = $scope.displayTransfer.download/1000
    		$scope.displayTransfer.upload = $scope.displayTransfer.upload/1000
    		$scope.displayTransfer.total = $scope.displayTransfer.total/1000	
    	}else if($scope.displayTransfer.total < 0.1){
    		$scope.displayTransfer.unit = "MB"
    		$scope.displayTransfer.download = $scope.displayTransfer.download *1000
    		$scope.displayTransfer.upload = $scope.displayTransfer.upload *1000
    		$scope.displayTransfer.total = $scope.displayTransfer.total	*1000
    	}else{
    		$scope.displayTransfer.unit = "GB"
    		$scope.displayTransfer.download = $scope.displayTransfer.download
    		$scope.displayTransfer.upload = $scope.displayTransfer.upload
    		$scope.displayTransfer.total = $scope.displayTransfer.total	
    	}
    	
    	$scope.transfersChart = [download,upload,total]
    } 
 
	$scope.limitedIdeas = limitToFilter($scope.ideas, 5);
	
	$scope.$watch("filter", function (newValue, oldvalue) {
		if($scope.devicestates && $scope.clients && $scope.company && $scope.sim  && $scope.transfer){
			$scope.getData()
			$scope.tableParams.reload();
			$scope.convertTransferToChart($scope.filteredTransfer)
 		}
    }, true); 

 
   $scope.$watch("customFilterParam", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
		if($scope.devicestates && $scope.clients && $scope.company && $scope.sim  && $scope.transfer){
			$scope.getData()
			$scope.tableParams.reload();
			$scope.convertTransferToChart($scope.filteredTransfer)
 		}
	}, true); 

/////////////////////////// DODAJ TRANSFER //////////////
	
	
	$scope.showPreview = false;
    $scope.showJSONPreview = true;
    $scope.json_string = "";
    $scope.file = {};
    $scope.newfilename =""

    $scope.progress = 0;
    $scope.items = [];

	$scope.parseCsvToJson = function(csv){
    	var file = []
    	var header = csv.split('\n')[0].split(";");
    	if( header[header.length-1].indexOf("\r")>0){
    		header[header.length-1] = header[header.length-1].substring(0, header[header.length-1].length - 1);
    	}
    	var items = []

    	for(var i=1; i<csv.split('\n').length; i++ ){
    		var line = csv.split('\n')[i]
    		if(line ==""){
    			continue
    		}
    		var item = {}
    		
    		for(var n=0 ;n<header.length; n++ ){
    			item[header[n]] = line.split(";")[n]
    			if(item[header[n]].indexOf("\r")>0){
    				item[header[n]] = item[header[n]].substring(0, item[header[n]].length - 1);
    			}
    		} 
    		items.push(item)
    	}
    	return items
    }

    $scope.fileChanged = function(files){		
        $scope.excelFile = files[0];	
		$scope.newfilename = files[0].name.toString()
		console.log(files[0])
        if(angular.equals($scope.excelFile.type.toString(),"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
        	$scope.sheets = [];
        	$scope.isProcessing = true;
        	XLSXReaderService.readFile($scope.excelFile, $scope.showPreview, $scope.showJSONPreview).then(function(xlsxData) {
		        $scope.sheets = xlsxData.sheets;
		        var keys = [];
		        console.log(keys)
				for(var k in $scope.sheets) keys.push(k);
		        if(keys.length==0){
		        	flash.show("Niewłaściwy format pliku.", "error")
		        	return
		        }		        
		        $scope.file= $scope.sheets[keys[0]] 
		        $scope.isProcessing = false;
        	});
        }else{
        	if(angular.equals($scope.excelFile.type.toString(),"application/vnd.ms-excel")){
        		var reader = new FileReader();		
        		reader.onload = function(e) {
				    $scope.$apply(function() {
				        $scope.csvFile = reader.result;
				        $scope.file = $scope.parseCsvToJson($scope.csvFile)
				    });
				};
				reader.readAsText($scope.excelFile);
			}else if(angular.equals($scope.excelFile.type.toString(),"text/csv")){
				var reader = new FileReader();		
        		reader.onload = function(e) {
				    $scope.$apply(function() {
				        $scope.csvFile = reader.result;
				        $scope.file = $scope.parseCsvToJson($scope.csvFile)
				    });
				};
				reader.readAsText($scope.excelFile);
 	        }else{
 	        	$scope.newfilename =""
 	        	flash.show("Niewłaściwy typ pliku.", "error")

	        }
        }
        
    }

 
	/** GET SIM BY MSISDN **/
	$scope.getSimByMSISDN = function(msisdn){ // zwraca element o określonej nazwie
		var simcard = $filter('filter')( $scope.sim, {msisdn:msisdn}); 
		 
	    for(var i = 0; i<simcard.length;i++ ){
	       	if($filter('lowercase')(simcard[i].state.name)== "aktywna"){
	          return simcard[i]
	        }
	    }
	    return null
	} 

    $scope.createnewitems = function(){
    	$scope.items = []
		$scope.progress = 0
		$scope.showprogress = false
		$scope.showNewRes = false;

		for(var i = 0; i < $scope.file.length;i++){
	    	var item = {}
	    	$scope.file[i]
	      	item.sim = $scope.getSimByMSISDN($scope.file[i]["MSISDN"])

	      	var dow = parseFloat($scope.file[i]["DOWNLINK[MB]"])
	       
	      	if(!dow || dow=="\r" || dow==""  || dow==NaN){
	      		dow = 0
	      	} 

	      	var upl = parseFloat($scope.file[i]["UPLINK[MB]"])
	      	if(!upl || upl=="\r" || upl=="" || upl==NaN){
	      		upl = 0
	      	} 

	      	var tot = parseFloat($scope.file[i]["TOTAL[MB]"])
	      	if(!tot || tot=="\r" || tot=="" || tot==NaN){
	      		tot = 0
	      	} 

	        item.downlink = parseFloat(dow)
	      	item.uplink =  parseFloat(upl)
	      	item.total =  parseFloat(tot)
	      	item.visible = true
		  	if(item.sim){
	        	item.ok = true
	        	item.$selected = true
	        }else{
	        	item.ok = false
	        	item.$selected = false
	        } 
	       
	     	$scope.items.push(item)
	    }
	    $scope.showNewRes = true; 

	}  


	$scope.changeAllSelection = function(selectall){
		for(var i = 0; i < $scope.items.length;i++){
			if($scope.items[i].ok){
				$scope.items[i].$selected = angular.copy(!selectall)	
			}
		}
	}

	$scope.checkProperties = function(item){
		if(item.sim){
			item.ok = true
			item.$selected = true
		}else{
			 item.ok = false
			item.$selected = false
		}
	}


	$scope.editTransfer = {}
	$scope.selectEditTransfer= function(item){
		$scope.editTransfer = item
		if(item.sim){
			$scope.displaytextNR.sim = item.sim.msisdn
		}else{
			$scope.displaytextNR.sim = ""	
		}
	}
	 

	$scope.displaytextNR={
		sim: ""
	}

	$scope.fileDate = {
		//year: 2014
		//month: new Date().getMonth()+1
	}

	$scope.$watch("displaytextNR.sim", function(newVal, oldVal){
		if(newVal && (typeof newVal == "object")){
			$scope.displaytextNR.sim = newVal.msisdn;
	 		$scope.editTransfer.sim = angular.copy(newVal);
	 		$scope.editTransfer.sim_id = angular.copy($scope.editTransfer.sim.id);
	 		$scope.checkProperties($scope.editTransfer)
		    $scope.editTransfer = {}
		}
	}, true);


	$scope.update= function(numb) {
  		var n = 1
  		if(numb >0){
  			n =  numb
  		}		 
  		if($scope.progress!=100){
  			$scope.progress += 100/n
  		}
  		if($scope.progress==100){
  			flash.show("Dodano nowe transfery kart SIM.", "success") 
  		}
  	}

	$scope.addSingleTransferFromFile = function(item, year, month, numtran){
	 	
		var newData = {
			downlink : item.downlink,
			uplink :  item.uplink,
			total : item.total,
			sim_id: item.sim.id,
			visible: true,
			year_id: year,
			month_id: month,
		}
		var table = "cbs_transfer"

		 
		$http.post("addRecord.py", { 'table': table, 'data':newData})
		    .success(function(data) {
		        if(data > 0){
		            if($scope.transfer){
		            	newData.id = parseInt(data)
		            	newData.sim = item.sim 
			            newData.year = newData.year_id
			            newData.month = newData.month_id

			            delete newData.month_id
			            delete newData.year_id
			             

			           	if($scope.transfer){
			           		$scope.transfer.push(newData) 	
			           	}
		          	}
		          	$scope.update(numtran)
		          }else{
		            flash.show("Nie dodano nowego transferu.", "warning")
		          }
		    })
		    .error(function(err){
		    	flash.show("Wystąpił błąd podczas dodawnia nowego transferu!", "error")
		    })
		 
	}

	$scope.addTransferFromFile = function(){
		$scope.showprogress = true
		var items = $filter('filter')($scope.items, {ok:true, $selected:true} )
		var numtran = items.length

		console.log(items)

 		var goForward = true
 		if(!$scope.fileDate.year){
			goForward = false
			$scope.fileDate.year = ""
		}
		if(!$scope.fileDate.month){
			goForward = false
			$scope.fileDate.month = ""
		}
		if(!goForward){
			flash.show("Wprowadź poprawnie miesiąc i rok.", "info")
			return
		}
		for(var i =0; i<items.length;i++){	
			$scope.addSingleTransferFromFile(angular.copy(items[i]), $scope.fileDate.year, $scope.fileDate.month, numtran)
 	 
			var index = $scope.items.indexOf(items[i])
			$scope.items[index].$selected = false
			$scope.items[index].visible = false

        	delete items[i].ok
			delete items[i].$selected
		}	

	}


///////////////////////////////////////////////
	///// DODAJ POJEDYNCZY TRANSFER
	$scope.newTransfer= {}

	$scope.displaytextNewT={
		sim: ""
	}

	$scope.$watch("displaytextNewT.sim", function(newVal, oldVal){
		if(newVal && (typeof newVal == "object")){
			$scope.displaytextNewT.sim = newVal.msisdn
 			$scope.newTransfer.sim = angular.copy(newVal);;	
		}
	}, true);


	$scope.addSingleTransfer = function(){
		var goForward = true

		if($scope.newTransfer.sim){
			if(!(angular.equals($scope.newTransfer.sim.msisdn,  $scope.displaytextNewT.sim))){
				$scope.displaytextNewT.sim = ""
				$scope.newTransfer.sim= ""
				goForward = false
			}
		}else{
			$scope.newTransfer.sim= ""
			$scope.displaytextNewT.sim = ""
			goForward = false
		}

		if(!$scope.newTransfer.year){
			goForward = false
			$scope.newTransfer.year = ""
		 
		}
		if(!$scope.newTransfer.month){
			goForward = false
			$scope.newTransfer.month = ""	
		} 

		if(!goForward){
			flash.show("Wprowadź poprawnie wszystie wymagane dane.", "info")
			return
		}
		//$scope.newTransfer.sim_id = $scope.newTransfer.sim.id
		//$scope.newTransfer.visible = true

		var newData = {
			downlink : parseFloat($scope.newTransfer.downlink),
			uplink : parseFloat($scope.newTransfer.uplink),
			total : parseFloat($scope.newTransfer.total),
			sim_id: parseInt($scope.newTransfer.sim.id),
			visible: true,
			year_id: parseInt($scope.newTransfer.year),
			month_id: parseInt($scope.newTransfer.month),
		}
		var table = "cbs_transfer"

		var dlg = null;
	    dlg = $dialogs.confirm('Potwierdzenie','Dodać nowy transfer karty SIM?');
	    dlg.result.then(function(btn){
		    $http.post("addRecord.py", { 'table': table, 'data':newData})
		        .success(function(data) {
		          if(data > 0){
		            flash.show("Dodano nowy transfer.", "success")

		            if($scope.transfer){
		            	newData.id = parseInt(data)
		            	newData.sim = $scope.newTransfer.sim 
			            newData.year = newData.year_id
			            newData.month = newData.month_id
			            delete newData.month_id
			            delete newData.year_id
			            $scope.transfer.push(newData) 
		          	}
		           
		          }else{
		            flash.show("Nie dodano nowego transferu.", "warning")
		          }
		      })
		      .error(function(err){
		        flash.show("Wystąpił błąd podczas dodawnia nowego transferu!", "error")
		      })
		},function(btn){});  
	}

})




app.directive('hcPie', function () {
  return {
    restrict: 'C',
    replace: true,
    scope: {
      items: '=',
      xseries: '=',
 
    },
    controller: function ($scope, $element, $attrs) {
     
    	 
    },
    template: '<div id="simChartContainer" style="margin: 0 auto;">not working</div>',
    link: function (scope, element, attrs) {
     	 

      	var newChart = function(){
	      return new Highcharts.Chart({
	        chart: {
	          renderTo: 'simChartContainer',
	          plotBackgroundColor: null,
	          plotBorderWidth: null,
	          plotShadow: false,

	         // width:  scope.width
	        },
	        title: {
	          text: 'Transfer kart SIM'
	        },
	        tooltip: {
	           pointFormat: '{series.name}: <b>{point.y}</b> GB',
	        },
	        xAxis: {
	            categories: scope.xseries,
	            title: {
                	text: 'Miesiąc'
            	}  
	        },
	        yAxis: {
	        	title: {
                	text: 'Transfer [GB]'
            	},
            	min :0 
	        },        
	        series: scope.items
	      });
	 	 }
     
        var chart = newChart()
        scope.$watch("items", function (newValue, oldvalue) {
      		newChart()
        }, true);       
    }
  }
});


/*

 title: false,            
            xAxis: {
                categories: kategorie
            },
            yAxis: {
                title: {
                    text: 'Transfer [GB]'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 'GB'
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                borderWidth: 0
            },
            series: seriesOptions
            /*series: [{
            	
  title: {
            text: 'Monthly Average Temperature',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: 'New York',
            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
        }, {
            name: 'Berlin',
            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
        }, {
            name: 'London',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
*/