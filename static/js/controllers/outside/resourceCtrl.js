app.controller("resourceCtrl", function($scope, $location, $filter, ngTableParams, commonData, Clients, Company, DevicesTypes, DevicesModels, DevicesStates, Devices, SubTransactionDevice){
	commonData.location = $location.path();
	commonData.broadcastItem()
	$scope.pageLoading = true

	$scope.selectedTab = "all";

/*  GET DATA  */
	///// CLIENTS
	$scope.clients = Clients.getClients()
	if(!Clients.getClients()){
        Clients.loadClientsData()
    }
    $scope.$on('clientsDataReady', function (event, data){
        Clients.clients = data
        $scope.clients = data
	});

    ///// COMPANY
	$scope.company = Company.getCompany()
	if(!$scope.company){
        Company.loadCompanData()
    }
    $scope.$on('companyDataReady', function (event, data){
        Company.company = data
        $scope.company = data
	});
 	
 	
    /////// SUBTRANSACTION DEVICES 
    /*
	$scope.subtransactionsdevice = SubTransactionDevice.getSubTransactionsDevice()
	if(!$scope.subtransactionsdevice){ 
	 	$scope.subtransactionsdevice = SubTransactionDevice.loadSubTransactionDeviceData();
	}  
	$scope.$on('subTransactionDeviceDataReady', function (event, data) {
		if($scope.devices && ($scope.devices.length >0) ){
		   console.log("available ===> subTransactionDeviceDataReady")
		   for(var i =0; i<$scope.devices.length;i++){
		        sub = $filter('filter')(data, {device_id: $scope.devices[i].id, returned: false}, true);
		        if(!$scope.devices[0].hasOwnProperty("available")){
			        if( sub.length >0){
			        	$scope.devices[i].available = false
			        }else{
			        	$scope.devices[i].available = true
			        }
			    }
		    }
		} 
		console.log(data)
		$scope.subtransactionsdevice = data;
		SubTransactionDevice.subtransactionsdevice = data;
		$scope.changeLoading() 
	});*/
 
    /////  TYPE
    $scope.devicetypes = DevicesTypes.getDeviceTypes()
	if(!$scope.devicetypes){
		$scope.devicetypes = DevicesTypes.loadDeviceTypesData();
	}  
	$scope.$on('deviceTypesDataReady', function (event, data) {
		if($scope.devices && ($scope.devices.length >0) && !$scope.devices[0].hasOwnProperty("type")){
		    for(var i =0; i<$scope.devices.length;i++){
		        type = $filter('filter')(data, {id: $scope.devices[i].type_id}, true);
		        if( type.length >0){
		        	$scope.devices[i].type = type[0]
		        	$scope.devices[i].type_name = type[0].name
		        }
		    }
		} 
		$scope.devicetypes = data;
		DevicesTypes.devicetypes = data;
		$scope.changeLoading()
	});

	///// MODELS
	$scope.devicemodels = DevicesModels.getDeviceModels()
	if(!$scope.devicemodels){
		$scope.devicemodels = DevicesModels.loadDeviceModelsData();
	}  
	$scope.$on('deviceModelsDataReady', function (event, data){
		if($scope.devices && ($scope.devices.length >0) && !$scope.devices[0].hasOwnProperty("model")){
		    for(var i =0; i<$scope.devices.length;i++){
		        model = $filter('filter')(data, {id: $scope.devices[i].model_id}, true);
		        if( model.length >0){
		        	$scope.devices[i].model = model[0]
		        	$scope.devices[i].model_name = model[0].name
		        }
		    }
		} 
		$scope.devicemodels = data;
		DevicesModels.devicemodels = data;
		$scope.changeLoading()
	});

    /////// STATES
    $scope.devicestates = DevicesStates.getDeviceState()
	if(!$scope.devicestates){
		 $scope.devicestates = DevicesStates.loadDeviceStateData();
	}  
	$scope.$on('deviceStatesDataReady', function (event, data) {
		if($scope.devices && ($scope.devices.length >0) && !$scope.devices[0].hasOwnProperty("state")){
		    for(var i =0; i<$scope.devices.length;i++){
		    	var state = $filter('filter')(data, {id: $scope.devices[i].state_id}, true);
		    	if( state.length>0){
		    		$scope.devices[i].state = state[0]
		        	$scope.devices[i].state_name = state[0].name
		    	}
		    }
		}
		$scope.devicestates = data;
		DevicesStates.devicestates = data;
		$scope.changeLoading()
	});

	///// DEVICES
    $scope.devices = Devices.getDevices()
    if(!$scope.devices){ 
		$scope.devices = Devices.loadDevicesData();
	} 
	$scope.$on('devicesDataReady', function (event, data) {
		if($scope.devicetypes || $scope.devicemodels || $scope.devicestates || $scope.company || $scope.clients ){
			for(var i = 0; i< data.length; i++){
				if($scope.devicetypes){
					data[i].type = $scope.getItem($scope.devicetypes, data[i].type_id)
					data[i].type_name = data[i].type.name
				}
				if($scope.devicemodels){
					data[i].model = $scope.getItem($scope.devicemodels, data[i].model_id)
					data[i].model_name = data[i].model.name
				}
				if($scope.devicestates){ 
					data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)
					data[i].state_name = data[i].state.name
				}
				if($scope.company){
					data[i].company = $scope.getItem($scope.company, data[i].company_id)
					data[i].company_name = data[i].company.name
				}
 

				if($scope.clients){
					holder = $filter('filter')($scope.clients, {id: data[i].holder_id}, true)[0];					

 					if(holder.user_id != null){
						data[i].available = true
					}else{
						if(holder.lastname == "LAB"){
							data[i].available = true
						}else{
							data[i].available = false
						}	
					}	 
				}
			
				if(!data[i].sn){data[i].sn=""} 
				if(!data[i].imei){data[i].imei=""}
				if(!data[i].mac){data[i].mac=""}

				if((data[i].owner_id.user_id == null) &&(parseInt(data[i].holder_id) == parseInt(data[i].owner_id))){
					data.splice(i,1); 
					i--;
				}
			}
		}


		if($scope.devicetypes){
			for(var i = 0; i<$scope.devicetypes.length;i++ ){
				var clie = $filter('filter')(data, {type_id: $scope.devicetypes[i].id}, true);
				$scope.devicetypes[i].len = clie.length;
			}
		}
		$scope.alldev = data.length; 
		$scope.devices = data;		
		 
		Devices.devices = data;
		$scope.changeLoading()	 	
	});
	 
	$scope.changeLoading = function(){	// sprawdź czy już można urkyć wizualizację ładowania 
		if($scope.devicestates && $scope.devicetypes && $scope.devicemodels && $scope.clients && $scope.company && $scope.devices /*&& $scope.subtransactionsdevice*/){
			$scope.pageLoading = false;
			$scope.initTable()
		}else{
			$scope.pageLoading = true;
		}
	}

	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id == id){
				return src[i]
			}
		}
	} 
	 
	$scope.getHolder = function(iid){ // zwraca element o określonym id
		cli = $filter('filter')($scope.clients, {id: iid}, true)[0];
		return cli
	}	 

/* ***** */

	/// type -1 => SIM   /// type 0 => both //  / type 1 => devs
	///  && showcolumn(value.type)
	$scope.columns = [{value: "cbs_id", visible: true, type: 0, name:"CBS Id", icon: "fa-key", col: "cbs_id"}, {value: "id", visible: false, type: 0, name:"Id", icon: "fa-key", col: "id"},
					  {value: "type", visible: false, type: 1, name: "Typ", icon: "fa-circle-thin", col: "type_name"}, {value: "model", visible: true, type: 1, name: "Model", icon: "fa-star-o", col: "model_name"},
					  {value: "company", visible: true, type: 0, name: "Firma", icon: "fa-suitcase", col: "company_name"}, {value: "sn", visible: true, type: 1, name: "SN", icon: "fa-barcode", col: "sn"},
					  {value: "imei", visible: true, type: 1, name: "IMEI", icon: "fa-bars", col: "imei"}, {value: "mac", visible: true, type: 1, name: "MAC", icon: "fa-database", col: "mac"},
					  {value: "description", visible: true, type: 0, name: "Opis", icon: "fa-file-text-o", col: "description"},
					  {value: "state", visible: true, type: 0, name: "Stan", icon: "fa-gavel", col: "state_name"},
					  {value: "available", visible: true, type: 0, name: "Dostępne", icon: "fa-flag-o", col: "available"}/* ,
					  {value: "holder_id", visible: true, type: 0, name: "Posiadacz", icon: "fa-flag-o", col: "holder_id"}  */
					  ]

	$scope.filter = {}

	$scope.selectedType = 0
	$scope.selectTyppe = function(type_id){
		$scope.selectedType = type_id;
		$scope.filter = {}
		if($scope.devices){
			$scope.tableParamsRes.reload();
		}
	}
 	

	var getData = function(){ 
		if($scope.selectedType == 0){
		 	return $scope.devices;
		 }else{
		 	return clie = $filter('filter')($scope.devices, {type_id: $scope.selectedType}, true);
		}
	}

	$scope.initTable = function(){ 
	    $scope.tableParamsRes = new ngTableParams({
	        page: 1,            // show first page
	        count: 50          // count per page
	    },{
	        total: getData().length,
	        getData: function($defer, params) { 
	            var filteredData = $filter('filter')( getData(), $scope.filter);  
	            var orderedData = params.sorting() ?
	                                $filter('orderBy')(filteredData, params.orderBy()) :
	                                filteredData;
	            params.total(orderedData.length);
	            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
	        }
	    }); 
	    $scope.tableParamsRes.settings().$scope = $scope;  
	}

	$scope.$watch("filter", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if($scope.devices){
			$scope.tableParamsRes.reload();
 		}
	}, true);

	$scope.changeLoading()
});

