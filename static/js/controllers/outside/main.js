var app = angular.module('CBS', ["ui.router", 'ngResource', 'ngTable', 'ui.bootstrap', 'ngAnimate']);

/** CLIENTS **/
app.provider('Clients', function () {
    "use strict";
    this.clients = null;
    this.$get = function ($http, $rootScope) {
        return {
            clients: this.clients,
            getClients: function () {
                return this.clients;
            },
            loadClientsData : function () {
                $http.get("getClients.py")
                    .success(function (data) {
                        this.clients = data;
                        $rootScope.$broadcast('clientsDataReady',  data);
                    });
            }
        };
    };
});

/** COMPANY **/
app.provider('Company', function () {
    "use strict";
    this.company = null;

    this.$get = function ($http, $rootScope) {
        return {
            company: this.company,
            getCompany: function () {
                return this.company;
            },
            loadCompanData : function () {
                $http.get("getCompany.py")
                    .success(function (data) {
                        this.company = data;
                        $rootScope.$broadcast('companyDataReady',  data);
                    });
            }
        };
    };
});

/** DEVICES **/
app.provider('Devices', function () {
    "use strict";
    this.devices = null;

    this.$get = function ($http, $rootScope) {
        return {
            devices: this.devices,
            getDevices: function () {
                return this.devices;
            },
            loadDevicesData : function () {
                $http.get("getDevices.py")
                    .success(function (data) {

                        this.devices = data;
                        $rootScope.$broadcast('devicesDataReady',  data);
                        $rootScope.$broadcast('dDataReady',  data);
                    });
            }
        };
    };
});

/** DEVICES TYPE **/
app.provider('DevicesTypes', function () {
    "use strict";
    this.devicetypes = null;

    this.$get = function ($http, $rootScope) {
        return {
            devicetypes: this.devicetypes,
            getDeviceTypes: function () {
                return this.devicetypes;
            },
            loadDeviceTypesData : function () {
                $http.get("getDevicesType.py")
                    .success(function (data) {
                        this.devicetypes = data;
                        $rootScope.$broadcast('deviceTypesDataReady',  data);
                    });
            }
        };
    };
});

/** DEVICES MODELS **/
app.provider('DevicesModels', function () {
    "use strict";
    this.devicemodels = null;

    this.$get = function ($http, $rootScope) {
        return {
            devicemodels: this.devicemodels,
            getDeviceModels: function () {
                return this.devicemodels;
            },
            loadDeviceModelsData : function () {
                $http.get("getDevicesModel.py")
                    .success(function (data) {
                        this.devicemodels = data;
                        $rootScope.$broadcast('deviceModelsDataReady',  data);
                    });
            }
        };
    };
});

/** DEVICES STATES **/
app.provider('DevicesStates', function () {
    "use strict";
    this.devicestates = null;

    this.$get = function ($http, $rootScope) {
        return {
            devicestates: this.devicestates,
            getDeviceState: function () {
                return this.devicestates;
            },
            loadDeviceStateData : function () {
                $http.get("getDevicesState.py")
                    .success(function (data) {
                        this.devicestates = data;
                        $rootScope.$broadcast('deviceStatesDataReady',  data);
                    });
            }
        };
    };
});


/** SUB TRANSACTION  DEVICE**/
app.provider('SubTransactionDevice', function () {
    "use strict";
    this.subtransactionsdevice = null;
    this.$get = function ($http, $rootScope) {
        return {
            subtransactionsdevice: this.subtransactionsdevice,
            getSubTransactionsDevice: function () {
                return this.subtransactionsdevice;
            },
            loadSubTransactionDeviceData : function () {
                $http.get("getSubTransactionDevice.py")
                    .success(function (data) {
                        this.subtransactionsdevice = data;
                        $rootScope.$broadcast('subTransactionDeviceDataReady',  data);
                    });
            }
        };
    };
});


/************** PRELOADING and CONFIGURATION  ********************/
/************** PRELOADING and CONFIGURATION  ********************/


app.config(function ($stateProvider, $urlRouterProvider) {
    "use strict";
    $urlRouterProvider.otherwise('resources');
    $stateProvider
        .state('res', {
            url : '/resources',
            templateUrl: 'resources.html',
            data: {
                active: 'resources'
            }
        });
            /*.state('spec', {
            url : '/specifications',
            templateUrl: 'specifications.html',
            data: {
              active: 'specifications'
            }
        })*/
});

app.factory('commonData', function ($rootScope, $location) {
    "use strict";
    var data = {};
    data.location = $location.path();

    data.broadcastItem = function () {
        $rootScope.$broadcast('commonDataBroadcast', data);
    };
    return data;
});

/************** CONTROLLERS  ********************/
/** MAIN CONTROLLER**/
app.controller("Main", function ($scope, commonData, Clients) {
    "use strict";
    // var scope = this; $rootScope, $location
    this.commonData = commonData;

    if (!Clients.getClients()) {
        Clients.loadClientsData();
    }
    $scope.$on('clientsDataReady', function (data) {
        Clients.clients = data;
    });

    $scope.Main = this;
    return $scope.Main;
});
