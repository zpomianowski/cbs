/** USER CONTROLLER **/
app.controller("UserCtrl", function ($scope, $http, flash, $filter, Clients, Users, $dialogs) {
    'use strict';
    $scope.users = Users.getUsers();
    $scope.clients = Clients.getClients();
    $scope.flash = flash;

  /** CLIENT EDIT FORM VALIDATION **/
    $scope.formValid = true;

    $scope.sortType = 'id';  // set the default sort type
    $scope.sortReverse = false; // set the default sort order
    $scope.searchUser = '';    // set the default search/filter term

    $scope.setFormValidation = function (value) {
      $scope.formValid = value;
    };

    $scope.addClientToUser = function () {
        angular.forEach($scope.users, function (value, key) {
            value.client = $filter('pairClientUser')($scope.clients, value.client_id);
        }, null);
    };

    $scope.users = Users.loadUsersData();

    $scope.$on('usersDataReady', function (event, data) {
        $scope.users = data;
        Users.users = data;
        if ($scope.clients) {
            $scope.addClientToUser();
        }
    });

    $scope.$on('clientsDataReady', function (event, data) {
      $scope.clients = data;
      Clients.clients = data;
      if($scope.users){
        $scope.addClientToUser()
      }
    });

    $scope.cleanSortOptions = function () {
        $scope.sortType = 'id';  // set the default sort type
        $scope.sortReverse = false; // set the default sort order
        $scope.searchUser = '';
        $scope.editId = -1;
    };

    /**  REMOVE USER**/
    $scope.removeUser = function (user) {
        var dlg = null;
        dlg = $dialogs.confirm('Usuń użytkownika', 'Czy chcesz usunąć użytkownika?');
        dlg.result.then(function (btn) {
            $http.post("hideRecord.py", {'id': user.id,
                                         'table': 'cbs_auth_user'})
                .success(function (data) {
                    user.visible = false;
                });
        }, function (btn) {} );
    };

  /**  EDIT USER**/
    $scope.editId = -1;
    $scope.setEditId = function (pid) {
        $scope.editId = pid;
    };

    $scope.selectedUser = null;
    $scope.copyUser = null;

    $scope.setEditingUser = function (user) {
        $scope.selectedUser = user;
        $scope.copyUser = angular.copy($scope.selectedUser);
    };

  /**  SAVE USER**/
    $scope.saveClient = function(user) {
        if ($scope.editId === user.id) {
            if ($scope.formValid) {
          /* UAKTUALNIJ UPRAWNIENIA*/
                if (!angular.equals(user.permission.id,$scope.copyUser.permission.id)) {
            data = {
              user_id : $scope.copyUser.id,
              group_id :$scope.copyUser.permission.id
            }
            $http.post("editRecord.py", {'id': user.member_id,
                                         'table': 'cbs_auth_membership',
                                         'data':data})
              .success(function(data) {
                user.permission.id = $scope.copyUser.permission.id;
                switch($scope.copyUser.permission.id){
                  case '1':
                    user.permission.role  = 'Administrator';
                    break;
                  case '2':
                    user.permission.role   = 'Manager';
                    break;
                  case '3':
                    user.permission.role   = 'Developer';
                    break;
                  case '4':
                    user.permission.role   = 'Updater';
                    break;
                  case '5':
                    user.permission.role   = 'Viewer';
                    break;
                }
                $scope.setEditId(-1);
                flash.show("Zaktualizowano uprawnienia użytkownika", "success")
              })
          }

          /* UAKTUALNIJ POZOSTAŁYCH WARTOŚCI*/
          if(!angular.equals(user.firstname,$scope.copyUser.firstname) | !angular.equals(user.lastname,$scope.copyUser.lastname) | !angular.equals(user.email,$scope.copyUser.email) | !angular.equals(user.client.phone,$scope.copyUser.client.phone)){
            var data = {
              first_name: $scope.copyUser.firstname,
              last_name: $scope.copyUser.lastname,
              email: $scope.copyUser.email
            }

            $http.post("editRecord.py", {'id': user.id,
                                         'table': 'cbs_auth_user',
                                         'data':data }) // edytuj użytkownika
              .success(function (data) {
                data = {
                  firstname :$scope.copyUser.firstname,
                  lastname :$scope.copyUser.lastname,
                  email :$scope.copyUser.email,
                  phone :$scope.copyUser.client.phone
                }

                $http.post("editRecord.py", {'id': user.client.id,
                                             'table': 'cbs_client',
                                             'data':data})
                  .success(function (data) {
                    if (data > 0) {
                      user.firstname = $scope.copyUser.firstname;
                      user.lastname = $scope.copyUser.lastname;
                      user.email = $scope.copyUser.email;
                      user.client.phone = $scope.copyUser.client.phone;
                      $scope.setEditId(-1);

                      user.client.firstname = $scope.copyUser.firstname;
                      user.client.lastname = $scope.copyUser.lastname;
                      user.client.email = $scope.copyUser.email;
                      $scope.setEditId(-1);
                      flash.show("Zaktualizowano dane klienta i użytkownika", "success")
                    }else{
                      flash.show("Nie zaktualizowano danych", "warning")
                    }
                  })
              })
          }
        }else{
          flash.show("Wprowadź poprawne dane", "info")
        }
      }else{}
    };
});
