app.controller("newSpecificationCtrl", function($scope, flash, $filter, $http, DevicesModels, Company, Specification, Datasheet) {
	$scope.flash = flash
	$scope.pageLoading = true;

	$scope.step = [{ class: 'active', id:1}, {class: '', id:2}, {class: '', id:3}, {class: '', id:4}, {class: '', id:5}]
	$scope.activStep = $scope.step[0]


 	$scope.changeStep = function(atr, type){ // prześcia pomiędzy zakładkami wizarda
		switch(type){
			case 'arrow':
				if(atr>0){
					var index = $scope.step.indexOf($scope.activStep) 
					if(index==0 && !$scope.firstStepValidation()){
						flash.show("Wprowadź poprawne dane", "info")
						return
					}
					 

                   /*
					if( index==1  && !$scope.selectedDevise){
						flash.show("Wybjerz przynajmniej jedno urządzenie ", "info")
						return
					}*/
 	
 					
					if( index==1){
						$scope.changedDatasheet = !angular.equals($scope.newDatasheet, $scope.oldDatasheet)	
					} 
					  
				
					/*
					if( index==1  && $filter('filter')($scope.devices, {$selected:true}).length ==0 && $filter('filter')($scope.sim, {$selected:true}).length ==0 ){
						flash.show("Wybjerz przynajmniej jedno urządzenie ", "info")
						return
					}*/
						$scope.activStep = $scope.step[index +atr]
						$scope.activStep.class = "active"
						angular.forEach($scope.step.slice(0, $scope.activStep.id-1), function(value, key) {
					           value.class="complete"
					    },  null); 
					    angular.forEach($scope.step.slice($scope.activStep.id, $scope.step.length), function(value, key) {
						           value.class=""
						},  null); 
					/*}else{
						flash.show("Wprowadź poprawne dane", "info")
					}*/
				}else{
					var index = $scope.step.indexOf($scope.activStep)
					$scope.activStep = $scope.step[index +atr]
					$scope.activStep.class = "active"

					angular.forEach($scope.step.slice(0, $scope.activStep.id-1), function(value, key) {
					        value.class="complete"
					},  null); 
					angular.forEach($scope.step.slice($scope.activStep.id, $scope.step.length), function(value, key) {
						value.class=""
					},  null); 
				}

				if($scope.step.indexOf($scope.activStep)===($scope.step.length-1)){
					$scope.addNewSpecification();
					//$timeout(update, 200);
				}
				break;
			case 'click':
				if($scope.step.indexOf($scope.activStep)!=($scope.step.length-1) && atr.class=='complete'){
					$scope.activStep = atr
					$scope.activStep.class = "active"
					angular.forEach($scope.step.slice(0, $scope.activStep.id-1), function(value, key) {
				           value.class="complete"
				    },  null); 
				    angular.forEach($scope.step.slice($scope.activStep.id, $scope.step.length), function(value, key) {
				           value.class=""
				    },  null); 	
				}
				break;
		}
	}

	$scope.company = Company.getCompany()
	$scope.devicemodels = DevicesModels.getDeviceModels()
	$scope.specifications = Specification.getSpecification()
	$scope.datasheets = Datasheet.getDatasheets()

/*GET DEVICES MODELS ****************/
	if(!$scope.devicemodels){
	 	$scope.devicemodels = DevicesModels.loadDeviceModelsData();
	}  
	$scope.$on('deviceModelsDataReady', function (event, data) {
		if($scope.specifications && $scope.specifications.length>0 && !$scope.specifications[0].hasOwnProperty("model")){
			for(var i =0; i<$scope.specifications.length;i++){
				$scope.specifications[i].model = $filter('filter')(data, {id:parseInt($scope.specifications.model_id)}, true)[0]	
			}
		}
		$scope.devicemodels = data;
		DevicesModels.devicemodels = data;
		$scope.changeLoading()
	});

/*GET COMPANY ****************/
  	if(!$scope.company){
	 	$scope.company = Company.loadCompanData();
	}  
	$scope.$on('companyDataReady', function (event, data) {
		if($scope.specifications && $scope.specifications.length>0 && !$scope.specifications[0].hasOwnProperty("company")){
			for(var i =0; i<$scope.specifications.length;i++){
				$scope.specifications[i].company = $filter('filter')(data, {id:parseInt($scope.specifications[i].company_id)}, true)[0]
			}
		}
		$scope.company = data;
		Company.company = data;
		$scope.changeLoading()
	});

/*GET SPECIFICATIONS ****************/
	if(!$scope.specifications){
	 	$scope.specifications = Specification.loadSpecificationsData();
	}  
	$scope.$on('specificationsDataReady', function (event, data) {
		if( $scope.devicemodels && $scope.company && ($scope.devicemodels.length>0) && ($scope.company.length>0)){
			for(var i =0; i<data.length;i++){
				data[i].company = $filter('filter')($scope.company, {id:parseInt(data[i].company_id)}, true)[0] 
				data[i].model = $filter('filter')($scope.devicemodels, {id:parseInt(data[i].model_id)}, true)[0]	
			}
		}
		$scope.specifications = data;
		Specification.specifications = data;
		$scope.changeLoading()
	});

//
	$scope.changeLoading = function(){	// sprawdź czy już można urkyć wizualizację ładowania 
		if($scope.company && $scope.devicemodels && $scope.specifications){
			$scope.pageLoading = false;
		} 			
	}

	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id == id){
				return src[i]
			}
		}
	} 
/*******   SETP 1*********/
	$scope.changeLoading()
	$scope.displayText = {}
	$scope.newSpecification = {}

	$scope.$watch("displayText.model", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if(newVal && (typeof newVal == "object")){
	    	$scope.displayText.model = newVal.name
	    	$scope.newSpecification.model = angular.copy(newVal);
 		}   
	}, true);

	$scope.$watch("displayText.company", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if(newVal && (typeof newVal == "object")){
	    	$scope.displayText.company = newVal.name
	    	$scope.newSpecification.company = angular.copy(newVal);
 		}   
	}, true);

	$scope.options = {
      height: 150,
      width: 500,
      lang:"pl-PL",
      disableDragAndDrop: true,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
    };

    $scope.init = function() {  };
	$scope.enter = function() {  };
	$scope.focus = function(e) {  };
	$scope.blur = function(e) { };
	$scope.paste = function() {  };
	$scope.change = function(contents, editable$) {};    
	$scope.keyup = function(e) { /*console.log('Key is released:', e.keyCode);*/  };
	$scope.keydown = function(e) { /*console.log('Key is pressed:', e.keyCode);*/ };
	$scope.imageUpload = function(files, editor, $editable) {};

    $scope.firstStepValidation = function(){
    	var goforwad = true;
    	var goforwadSys = true;
    	if(!$scope.newSpecification.hasOwnProperty("model")){
    		goforwad = false;
    		$scope.displayText.model=""
    		$scope.newSpecification.model = null
    	}else if(!angular.equals($scope.displayText.model, $scope.newSpecification.model.name)){
    		goforwad = false;
    		$scope.displayText.model=""
    		$scope.newSpecification.model = null
    	}
    	if(!$scope.newSpecification.hasOwnProperty("company")){
    		goforwad = false;
    		$scope.displayText.company=""
    		$scope.newSpecification.company = null
    	}else if(!angular.equals($scope.displayText.company, $scope.newSpecification.company.name)){
    		goforwad = false;
    		$scope.displayText.company=""
    		$scope.newSpecification.company = null
    	}
    	if(goforwad){
	    	if($scope.newSpecification.firmware){
	    		if(!$scope.newSpecification.operating_system && !$scope.newSpecification.system_version){
	    		}else{
					if(!$scope.newSpecification.operating_system){
						$scope.newSpecification.operating_system = ""
						goforwad = false; 	
					}
					if(!$scope.newSpecification.system_version){
						$scope.newSpecification.system_version = ""
						goforwad = false; 	
					}
	    		}
	    	}else{
	    		if(!$scope.newSpecification.hasOwnProperty("operating_system") || !$scope.newSpecification.operating_system){
					$scope.newSpecification.operating_system = ""
					goforwad = false; 	
				}
				if(!$scope.newSpecification.hasOwnProperty("system_version") || !$scope.newSpecification.system_version){
					$scope.newSpecification.system_version = ""
					goforwad = false; 	
				}	 
	    	}
    	}
    	$scope.existingSpecification = $scope.findSpecification($scope.newSpecification.model, $scope.newSpecification.company) 
    	
    	if($scope.existingSpecification && $scope.existingSpecification.datasheet_id){ 
    		$scope.changeOldDatasheet($scope.existingSpecification.datasheet_id)   		 	
    	}else{
    		$scope.oldDatasheet = null
    	} 
    	return goforwad
    }

/*******   SETP 2*********/
	$scope.selectoptions ={
		tgpp: ["Rel-13", "Rel-12", "Rel-11", "Rel-10", "Rel-9", "Rel-8", "Rel-7", "Rel-6", "Rel-5", "Rel-4", "Rel-99"],
		usagesetting: ["voice centric","data centric" ],
		voice_domain: ["CS Voice only", "IMS PS Voice only", "CS Voice preferred, IMS PS Voice as secondary", "IMS PS Voice preferred, CS Voice as secondary"],
		drx: ["short", "long", "short/long"],
		mimo: ["2x2", "3x3", "4x4", "8x8"],
		duplex: ["TDD", "FDD", "TDD/FDD"],
		cat_3g_downlink: ["1", "2", "3", ]
	}
	$scope.band_lte =[
		{title:"B1", selected:false}, {title:"B2", selected:false},
		{title:"B3", selected:false}, {title:"B4", selected:false},
		{title:"B5", selected:false}, {title:"B6", selected:false},
		{title:"B7", selected:false}, {title:"B8", selected:false},
		{title:"B9", selected:false}, {title:"B10", selected:false},
		{title:"B11", selected:false}, {title:"B12", selected:false},
		{title:"B13", selected:false}, {title:"B14", selected:false},

		{title:"B17", selected:false}, {title:"B18", selected:false},
		{title:"B19", selected:false}, {title:"B20", selected:false},
		{title:"B21", selected:false}, {title:"B22", selected:false},
		{title:"B23", selected:false}, {title:"B24", selected:false},
		{title:"B25", selected:false}, {title:"B26", selected:false},
		{title:"B27", selected:false}, {title:"B28", selected:false},
		{title:"B29", selected:false}, {title:"B30", selected:false},

		{title:"B31", selected:false},
		{title:"B33", selected:false}, {title:"B34", selected:false},
		{title:"B35", selected:false}, {title:"B36", selected:false},
		{title:"B37", selected:false}, {title:"B38", selected:false},
		{title:"B39", selected:false}, {title:"B40", selected:false},
		{title:"B41", selected:false}, {title:"B42", selected:false},
		{title:"B43", selected:false}, {title:"B44", selected:false}
	]
	$scope.band_3g =[
		{title:"B1", selected:false}, {title:"B2", selected:false},
		{title:"B3", selected:false}, {title:"B4", selected:false},
		{title:"B5", selected:false}, {title:"B6", selected:false},
		{title:"B7", selected:false}, {title:"B8", selected:false},
		{title:"B9", selected:false}, {title:"B10", selected:false},
		{title:"B11", selected:false}, {title:"B12", selected:false},
		{title:"B13", selected:false}, {title:"B14", selected:false},

		{title:"B19", selected:false}, {title:"B20", selected:false},
		{title:"B21", selected:false}, {title:"B22", selected:false},
		{title:"B25", selected:false}, {title:"B26", selected:false}
	]
	$scope.band_2g =[
		{title:"850", selected:false}, {title:"900", selected:false},
		{title:"1800", selected:false}, {title:"1900", selected:false}
	]
	$scope.band_wifi =[
		{title:"2,4", selected:false},
		{title:"5", selected:false}
	]
	$scope.standard_wifi =[
		{title:"a", selected:false}, {title:"b", selected:false},
		{title:"g", selected:false}, {title:"n", selected:false},
		{title:"ac", selected:false} 
	]
	$scope.rohc =[
		{title:"0x0001", selected:false}, {title:"0x0002", selected:false},
		{title:"0x0003", selected:false}, {title:"0x0004", selected:false},
		{title:"0x0006", selected:false},{title:"0x0101", selected:false},
		{title:"0x0102", selected:false},{title:"0x0103", selected:false},
		{title:"0x0104", selected:false}
	]
	 

	$scope.existingSpecification = null;
	$scope.oldDatasheet = null;
	$scope.newDatasheet = {};
	$scope.specificationsList = [];

	$scope.changedDatasheet = false;

	$scope.$watch("displayText.chipset_prvider", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if(newVal && (typeof newVal == "object")){
	    	$scope.displayText.chipset_prvider = newVal.name
	    	$scope.newDatasheet.chipset_prvider = angular.copy(newVal);
	    	$scope.newDatasheet.chipset_prvider_id = $scope.newDatasheet.chipset_prvider.id
 		}   
	}, true);


	$scope.$watch("band_lte", function(newVal, oldVal){ // zamień zaznaczone pasma na string
		if(!angular.equals(newVal, oldVal)){
			var temp =""
			for(var i =0; i < $scope.band_lte.length;i++ ){
				if($scope.band_lte[i].selected==true){
					temp += $scope.band_lte[i].title+" "
				}
			}
			$scope.newDatasheet.band_lte = temp.trim()
		}
	}, true);
	$scope.$watch("band_3g", function(newVal, oldVal){ // zamień zaznaczone pasma na string
		if(!angular.equals(newVal, oldVal)){
			var temp =""
			for(var i =0; i < $scope.band_3g.length;i++ ){
				if($scope.band_3g[i].selected==true){
					temp += $scope.band_3g[i].title+" "
				}
			}
			$scope.newDatasheet.band_3g = temp.trim()
		}
	}, true);
	$scope.$watch("band_2g", function(newVal, oldVal){ // zamień zaznaczone pasma na string
		if(!angular.equals(newVal, oldVal)){
			var temp =""
			for(var i =0; i < $scope.band_2g.length;i++ ){
				if($scope.band_2g[i].selected==true){
					temp += $scope.band_2g[i].title+" "
				}
			}
			$scope.newDatasheet.band_2g = temp.trim()
		}
	}, true);
	$scope.$watch("band_wifi", function(newVal, oldVal){ // zamień zaznaczone pasma na string
		if(!angular.equals(newVal, oldVal)){
			var temp =""
			for(var i =0; i < $scope.band_wifi.length;i++ ){
				if($scope.band_wifi[i].selected==true){
					temp += $scope.band_wifi[i].title+" "
				}
			}
			$scope.newDatasheet.band_wifi = temp.trim()
		}
	}, true);
	$scope.$watch("standard_wifi", function(newVal, oldVal){ // zamień zaznaczone pasma na string
		if(!angular.equals(newVal, oldVal)){
			var temp =""
			for(var i =0; i < $scope.standard_wifi.length;i++ ){
				if($scope.standard_wifi[i].selected==true){
					temp += $scope.standard_wifi[i].title+" "
				}
			}
			$scope.newDatasheet.standard_wifi = temp.trim()
		}
	}, true);
	$scope.$watch("rohc", function(newVal, oldVal){ // zamień zaznaczone pasma na string
		if(!angular.equals(newVal, oldVal)){
			var temp =""
			for(var i =0; i < $scope.rohc.length;i++ ){
				if($scope.rohc[i].selected==true){
					temp += $scope.rohc[i].title+" "
				}
			}
			$scope.newDatasheet.rohc = temp.trim()
		}
	}, true);
 

 	$scope.selectItemFromDatasheet = function(){
 		$scope.newDatasheet = angular.copy($scope.oldDatasheet)
 		if($scope.newDatasheet){
 			arraysToselect = ["band_lte", "band_3g", "band_2g", "band_wifi" ,"standard_wifi", "rohc"]
	 	 	for(var n =0; n<arraysToselect.length; n++){
	 	 		if(!$scope.newDatasheet.hasOwnProperty(arraysToselect[n])){
	 	 			continue;
	 	 		}
	 	 		if($scope.newDatasheet.hasOwnProperty(arraysToselect[n]) && $scope.newDatasheet[arraysToselect[n]]){
	 	 			var bands = $scope.newDatasheet[arraysToselect[n]].split(" ")
			 	 	for(var i =0; i< bands.length;i++){
			 			var item = $filter('filter')($scope[arraysToselect[n]], {title:bands[i]}, true)
			 			if(item.length>=1){
			 				item[0].selected = true 	
			 			} 
			 		}		
	 	 		}
		 	 	
	 	 	}	
 		}else{
 			arraysToselect = ["band_lte", "band_3g", "band_2g", "band_wifi" ,"standard_wifi", "rohc"]
	 	 	for(var n =0; n<arraysToselect.length; n++){
		 	 	for(var i =0; i< $scope[arraysToselect[n]].length;i++){
		 			$scope[arraysToselect[n]][i].selected = false 	
		 		}	
	 	 	}	
 		}	
 	}
	 
	$scope.findSpecification = function(model, company){
		$scope.specificationsList = []
		if(model && company){
			for(var i =0; i< $scope.specifications.length; i++){
				var k = $scope.specifications[i]
				if(angular.equals(k.model_id, model.id) && angular.equals(k.company_id, company.id)){
					$scope.specificationsList.push(k)
					//return k
				}
			}	
		}
		if($scope.specificationsList.length>0){
			return $scope.specificationsList[$scope.specificationsList.length-1]
		}else{
			return null
		}	
	}

	$scope.changeOldDatasheet = function(datasheet_id){
		$scope.oldDatasheet = null
    	for(var i=0; i<$scope.datasheets.length;i++){
    		if($scope.datasheets[i].id==parseInt(datasheet_id)){
				$scope.oldDatasheet = $scope.datasheets[i]
				$scope.selectItemFromDatasheet()
				$scope.displayText.chipset_prvider = $scope.getItem($scope.company, $scope.datasheets[i].chipset_prvider_id )
    		}
    	}	
    	if(!$scope.oldDatasheet){
	    	$http.post("getDatsheet/"+datasheet_id.toString()) 
	        	.success(function(data) {
					if(data.chipset_prvider_id){
						data.chipset_prvider_id = parseInt(data.chipset_prvider_id)
						var provider = $filter('filter')($scope.company, {id:parseInt(data.chipset_prvider_id)}, true)	
						if(provider.length>=1){
							data.chipset_prvider = provider[0]
							$scope.displayText.chipset_prvider = provider[0]
						}
					}
					$scope.oldDatasheet = data
	        		$scope.selectItemFromDatasheet() 
	        		$scope.displayText.chipset_prvider = $scope.getItem($scope.company, data.chipset_prvider_id )
	        		//$scope.datasheets.push(data)
	        		Datasheet.datasheets.push(data)
	        	})
	        	.error(function(err){
	    			flash.show("Wystąpił błąd podczas pobierania karty katalogowej!", "error")
	    		})	
    	}  
	}	

	$scope.$watch("selectDifferentDatasheet", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
		arraysToselect = ["band_lte", "band_3g", "band_2g", "band_wifi" ,"standard_wifi", "rohc"]
	 	 	for(var n =0; n<arraysToselect.length; n++){
		 	 	for(var i =0; i< $scope[arraysToselect[n]].length;i++){
		 	 	 
		 			$scope[arraysToselect[n]][i].selected = false 	
		 		}	
	 	 	}	
		if($scope.existingSpecification){
			$scope.changeOldDatasheet(newVal)	 
		}	
	}, true);

/*******   SETP 3*********/
	$scope.throughputs = [];
	$scope.newthroughput = {};
	$scope.addNewThroughputs = function(){	 
		if($scope.newthroughput.hasOwnProperty("category") && ($scope.newthroughput.hasOwnProperty("uplink") || $scope.newthroughput.hasOwnProperty("downlink"))){
			$scope.throughputs.push($scope.newthroughput);	
		}
		$scope.newthroughput = {}			
	}


	$scope.addThroughputsToDB = function(specid){
		angular.forEach($scope.throughputs, function(value, key) {
 			value.adddate = $filter('date')(new Date(), 'yyyy-MM-dd')
 			value.specification_id = specid
 			$http.post("addRecord.py", {'table': 'cbs_throughput', 'data':value})
	        	.success(function(data) {
	        		value.id = parseInt(data);
	        		console.log(value)
	    		})
	    		.error(function(err){
	    			flash.show("Wystąpił błąd podczas dodawania nowej karty katalogowej!", "error")
	    		}) 	 
		}, false);

	}

/*******   SETP 4*********/
	$scope.addNewSpecification = function(){
 		var newSpec = {
 			model_id : $scope.newSpecification.model.id,
 			company_id: $scope.newSpecification.company.id,
 			adddate: $filter('date')(new Date(), 'yyyy-MM-dd')
 		}	
 		
 		angular.forEach($scope.newSpecification, function(value, key) {
 			if((key !="model") && (key !="company")){
 				newSpec[key] =value;
 			}
			
		}, false);


 		var datasheet = angular.copy($scope.newDatasheet)
 		delete  datasheet.chipset_prvider

 		if(!$scope.oldDatasheet || ($scope.oldDatasheet && $scope.changedDatasheet && ($scope.datasheetActivity == "new" ))){ // brak wcześniejszej karty katalogowej
 			if(datasheet.hasOwnProperty("id")){
 				delete datasheet.id
 			}
 			$http.post("addRecord.py", {'table': 'cbs_datasheet', 'data':datasheet})
	        	.success(function(data) {
	        		datasheet.id = parseInt(data);
	        		datasheet.chipset_prvider = $scope.newDatasheet.chipset_prvider
	        		datasheet.chipset_prvider = angular.copy($scope.newDatasheet.chipset_prvider)

	        		$scope.datasheets.push(datasheet);
	        		newSpec.datasheet_id = datasheet.id;
	        		$http.post("addRecord.py", {'table': 'cbs_specification', 'data':newSpec})
	        			.success(function(data) {
	        				newSpec.id = parseInt(data);
	        				newSpec.model =  $scope.newSpecification.model
	        				newSpec.company =  $scope.newSpecification.company
	        				$scope.addThroughputsToDB(newSpec.id);

		        			if($scope.specifications){
		        				$scope.specifications.push(newSpec)
		        			}
	        				flash.show("Dodano nową specyfikcaję!", "success")
	        			})
			    		.error(function(err){
			    			flash.show("Wystąpił błąd podczas dodawania nowej specyfikacji!", "error")
			    		}) 	 	

	    		})
	    		.error(function(err){
	    			flash.show("Wystąpił błąd podczas dodawania nowej karty katalogowej!", "error")
	    		}) 	 
			
 		}else{			
 			if($scope.changedDatasheet && ($scope.datasheetActivity == "update" )){
 				var index = $scope.datasheets.indexOf($scope.newDatasheet)
 				$scope.datasheets.slice(index,1)
 				$http.post("editRecord.py", {'table': 'cbs_datasheet', 'data':datasheet, 'id': datasheet.id})
	        		.success(function(data) {
		        		datasheet.chipset_prvider = $scope.newDatasheet.chipset_prvider
		        		
		        		$scope.datasheets.push(datasheet);

		        		newSpec.datasheet_id = datasheet.id;
		 				$http.post("addRecord.py", {'table': 'cbs_specification', 'data':newSpec})
				        	.success(function(data) {
				        		newSpec.id = parseInt(data);
				        		newSpec.model =  $scope.newSpecification.model
			        			newSpec.company =  $scope.newSpecification.company
				        		$scope.addThroughputsToDB(newSpec.id);

				        		if($scope.specifications){
				        			$scope.specifications.push(newSpec)
				        		}
				        		flash.show("Dodano nową specyfikcaję!", "success")
				        	})
						    .error(function(err){
						    	flash.show("Wystąpił błąd podczas dodawania nowej specyfikacji!", "error")
						    })	

		    		})
		    		.error(function(err){
		    			flash.show("Wystąpił błąd podczas dodawania nowej karty katalogowej!", "error")
		    		}) 
 			}else{
 				newSpec.datasheet_id = datasheet.id;
 				$http.post("addRecord.py", {'table': 'cbs_specification', 'data':newSpec})
		        	.success(function(data) {
		        		newSpec.id = parseInt(data);
		        		newSpec.model =  $scope.newSpecification.model
	        			newSpec.company =  $scope.newSpecification.company
		        		$scope.addThroughputsToDB(newSpec.id);

		        		if($scope.specifications){
		        			$scope.specifications.push(newSpec)
		        		}
		        		flash.show("Dodano nową specyfikcaję!", "success")
		        	})
				    .error(function(err){
				    	flash.show("Wystąpił błąd podczas dodawania nowej specyfikacji!", "error")
				    })
			}
 		}  
 		

	}

	//datasheetActivity
})


app.directive('dynamic', function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
});
 
 