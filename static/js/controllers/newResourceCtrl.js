//'use strict';

app.controller("newResourceCtrl", function($scope,$rootScope, $http, flash, $filter, $dialogs, Company, DevicesTypes, DevicesModels, DevicesStates, Clients, Devices, SIM, Session, XLSXReaderService) {
	$scope.flash = flash
	$scope.pageLoading = false;
	$scope.newRes = {}
	$scope.showNewRes = false;
	
	$scope.issim = false;
  	

	$scope.displaytext ={
		model: "",
		company: "",
		owner: "LAB LAB",
		holder: "LAB LAB"
	}

	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id == id){
				return src[i]
			}
		}
	} 

	$scope.devicetypes = DevicesTypes.getDeviceTypes()
	$scope.devicemodels = DevicesModels.getDeviceModels()
	$scope.devicestates = DevicesStates.getDeviceState()
	$scope.company = Company.getCompany()
	$scope.clients = Clients.getClients()

	$scope.devices = Devices.getDevices()

	$scope.sim = SIM.getSim()

	/*GET CLIENTS ****************/ 
		if(!$scope.clients){ 
		 	$scope.clients = Clients.loadClientsData();
		}else{
			var index = Session.user.LAB
			$scope.newRes.holder = $scope.getItem($scope.clients, index)
			$scope.newRes.owner = $scope.getItem($scope.clients, index)
		} 
		$scope.$on('clientsDataReady', function (event, data) {
			var index = Session.user.LAB
			$scope.newRes.holder = $scope.getItem(data, index)
			$scope.newRes.owner = $scope.getItem(data, index)
			$scope.clients = data;
			Clients.clients = data;
			$scope.changeLoading()
		});

	/*GET DEVICES TYPES ****************/
	  	if(!$scope.devicetypes){
		 	$scope.devicetypes = DevicesTypes.loadDeviceTypesData();
		}  
		$scope.$on('deviceTypesDataReady', function (event, data) {
			$scope.devicetypes = data;
			DevicesTypes.devicetypes = data;
			$scope.changeLoading()
		});

	/*GET DEVICES MODELS ****************/
	  	if(!$scope.devicemodels){
		 	$scope.devicemodels = DevicesModels.loadDeviceModelsData();
		}  
		$scope.$on('deviceModelsDataReady', function (event, data) {
			$scope.devicemodels = data;
			DevicesModels.devicemodels = data;
			$scope.changeLoading()
		});

	/*GET DEVICES STATES ****************/
	  	if(!$scope.devicestates){
		 	$scope.devicestates = DevicesStates.loadDeviceStateData();
		}else{
			$scope.newRes.state  = $scope.getItem($scope.devicestates, 1)
		}
		$scope.$on('deviceStatesDataReady', function (event, data) {
			$scope.devicestates = data;
			DevicesStates.devicestates = data;
			$scope.newRes.state  = $scope.getItem(data, 1)
			$scope.changeLoading()
		});

	/*GET COMPANY ****************/
	  	if(!$scope.company){
		 	$scope.company = Company.loadCompanData();
		}  
		$scope.$on('companyDataReady', function (event, data) {
			$scope.company = data;
			Company.company = data;
			$scope.changeLoading()
		});

	$scope.changeLoading = function(){	// sprawdź czy już można urkyć wizualizację ładowania 
		if($scope.devicestates && $scope.devicetypes && $scope.devicemodels && $scope.clients && $scope.company){
			$scope.pageLoading = false;
		}else{
			$scope.pageLoading = true;
		}
	}
	$scope.changeLoading()
	 
	$scope.$watch("displaytext.model", function(newVal, oldVal){
		if(newVal && (typeof newVal == "object")){
			$scope.displaytext.model = newVal.name
 			$scope.newRes.model = angular.copy(newVal);;	
		}
	}, true);

	$scope.$watch("displaytext.company", function(newVal, oldVal){
		if(newVal && (typeof newVal == "object")){
			$scope.displaytext.company = newVal.name
 			$scope.newRes.company = angular.copy(newVal);;	
		}
	}, true);
 
	$scope.$watch("displaytext.owner", function(newVal, oldVal){
		if(newVal && (typeof newVal == "object")){
			$scope.displaytext.owner = newVal.firstname+' '+newVal.lastname
 			$scope.newRes.owner = angular.copy(newVal);;	
		}
	}, true);

	$scope.$watch("displaytext.holder", function(newVal, oldVal){
		if(newVal && (typeof newVal == "object")){
			$scope.displaytext.holder = newVal.firstname+' '+newVal.lastname
 			$scope.newRes.holder = angular.copy(newVal);;	
		}
	}, true);
 
 	$scope.updateSelectedType = function(type_id){
 		if(type_id >0){
 			$scope.newRes.type = $scope.getItem($scope.devicetypes, type_id)
 			delete $scope.newRes.pin
 			delete $scope.newRes.pin2
 			delete $scope.newRes.puk
 			delete $scope.newRes.puk2
 			delete $scope.newRes.msisdn
 			delete $scope.newRes.imsi
 			delete $scope.newRes.iccid
 		}else{					   //////// SIM
 			delete $scope.newRes.type
 			delete $scope.newRes.sn
 			delete $scope.newRes.imei
 			delete $scope.newRes.mac
 			delete $scope.newRes.iccid
 		}	
 	}


 	$scope.sendEmail = function(dev, sim){
	      $scope.sendingEmail = true
	      var devidlist = "0_"
	      if(dev.length>0){
		       for(var i =0; i<dev.length;i++ ){
		          devidlist += dev[i].id+"_"
		      }	
	      }
	      devidlist = devidlist.substring(0, devidlist.length - 1); 
	     
	      var simidlist  = "0_"
	      if(sim.length>0){
		      for(var i =0; i<sim.length;i++ ){
		          simidlist += sim[i].id+"_" 
		      }	
	      }
	      simidlist = simidlist.substring(0, simidlist.length - 1); 

	      $http.post("sendEmail.py", {"toemail": Session.user.email, "msgtype":"newresource" , "dev":devidlist, "sim":simidlist})
	        .success(function(data){
	            $scope.sendingEmail = false
	            if(data.toString()=="True"){
	             //flash.show("Wysłano powiadomienie.", "success")
	            }else{
	               flash.show("Nie wysłano powiadomnienia !", "info")
	            }
	        })
	        .error(function(err){
	          flash.show("Wystąpił błąd podczas wysyłania emaila!", "error")
	        })
    } 

	$scope.add = function(){
		var goForward = true
		if($scope.newRes.holder){
			if(!(angular.equals($scope.newRes.holder.firstname+" "+$scope.newRes.holder.lastname,  $scope.displaytext.holder))){
				$scope.displaytext.holder= ""
				goForward = false
			}
		}else{
			$scope.displaytext.holder= ""
			$scope.newRes.holder = ""
			goForward = false
		}

		if($scope.newRes.owner){
			if(!(angular.equals($scope.newRes.owner.firstname+" "+$scope.newRes.owner.lastname,  $scope.displaytext.owner))){
				$scope.displaytext.owner= ""
				goForward = false
			}
		}else{
			$scope.displaytext.owner= ""
			$scope.newRes.owner = ""
			goForward = false
		}

		if($scope.newRes.type_id > 0){ // DEV
			if($scope.newRes.model){
				if(!(angular.equals($scope.newRes.model.name,  $scope.displaytext.model))){  // object. name === display text.model
					$scope.displaytext.model= ""
					goForward = false
				}
			}else{
				$scope.displaytext.model= ""
				$scope.newRes.model = ""
				goForward = false
			}
			if($scope.newRes.company){
				if(!(angular.equals($scope.newRes.company.name,  $scope.displaytext.company))){
					$scope.displaytext.company= ""
					goForward = false
				}
			}else{
				$scope.displaytext.company= ""
				$scope.newRes.company = ""
				goForward = false
			}
			
			if(!$scope.newRes.type){
				$scope.newRes.type= ""
				goForward = false
			}
			if(!$scope.newRes.state){
				$scope.newRes.state= ""
				goForward = false
			}

			if(!goForward){
				$scope.flash.show("Wprowadź poprawnie wszystie wymagane dane.", "info")
				return
			}

			if(!$scope.newRes.sn && !$scope.newRes.mac && !$scope.newRes.imei){
				$scope.flash.show("Wypełnij przynajmniej jedno z pól: SN, IMEI, MAC.", "worning")
				return  
			}

			var newData ={
			    model_id : $scope.newRes.model.id,
			    company_id : $scope.newRes.company.id,
			    type_id : $scope.newRes.type.id,
			    state_id : $scope.newRes.state.id,
			    sn : $scope.newRes.sn,
			    imei : $scope.newRes.imei,
			    mac : $scope.newRes.mac,
			    holder_id : $scope.newRes.holder.id,
			    owner_id : $scope.newRes.owner.id,
			    description : $scope.newRes.description,
			    visible: true
			  }
			var table = "cbs_device"	
		}else{	// SIM

			if($scope.newRes.company){
				if(!(angular.equals($scope.newRes.company.name,  $scope.displaytext.company))){
					$scope.displaytext.company= ""
					goForward = false
				}
			}else{
				$scope.displaytext.company= ""
				$scope.newRes.company = ""
				goForward = false
			}
			if(!$scope.newRes.state){
				$scope.newRes.state= ""
				goForward = false
			}
			if(!goForward){
				$scope.flash.show("Wprowadź poprawnie wszystie wymagane dane.", "info")
				return
			}
			if(!$scope.newRes.imsi && !$scope.newRes.iccid && !$scope.newRes.msisdn){
				$scope.flash.show("Wypełnij przynajmniej jedno z pól: MSISDN, IMSI, ICCID.", "worning")
				return  
			}
		  	var newData ={
			    company_id : $scope.newRes.company.id,
			    state_id : $scope.newRes.state.id,
			    iccid : $scope.newRes.iccid,
			    imsi : $scope.newRes.imsi,
			    msisdn : $scope.newRes.msisdn,
			    description : $scope.newRes.description,
			    holder_id : $scope.newRes.holder.id,
			    owner_id : $scope.newRes.owner.id,
			    pin: $scope.newRes.pin,
			    pin2: $scope.newRes.pin2,
			    puk: $scope.newRes.puk,
			    puk2: $scope.newRes.puk2,
			    visible: true
			}
			var table = "cbs_sim"	
		}

		var dlg = null;
	    dlg = $dialogs.confirm('Potwierdzenie','Dodać nowe urządenie?');
	    dlg.result.then(function(btn){
		    $http.post("addRecord.py", { 'table': table, 'data':newData})
		        .success(function(data){
		          if(data){
		          	id = parseInt(data.split("-")[0])
		          	cbs_id = parseInt(data.split("-")[1])
		          }
		          if(id > 0){
		            flash.show("Dodano nowe urządenie.", "success")
		            newData.id = id
		            newData.cbs_id = cbs_id
		            var temp = []
			        temp.push(newData)
		            if($scope.devices){
			            newData.company = $scope.newRes.company
			            newData.state = $scope.newRes.state
			            newData.holder = $scope.newRes.holder
			            newData.owner = $scope.newRes.owner
			           
			            if($scope.newRes.type_id > 0){
			            	newData.type = $scope.newRes.type
			            	newData.model = $scope.newRes.model
			            	$scope.sendEmail(temp, [])
			            }else{
			            	$scope.sendEmail([], temp)
			            }
			            $scope.devices.push(newData) 
		          	}else{
						if($scope.newRes.type_id > 0){
				        	$scope.sendEmail(temp, [])
				        }else{
				            $scope.sendEmail([], temp)
				        }
		          	}
		          	
		           
		          }else{
		            flash.show("Nie dodano nowego urządenia.", "warning")
		          }
		      })
		      .error(function(err){
		        flash.show("Wystąpił błąd podczas dodawnia nowego urządenia!", "error")
		      })
		},function(btn){}); 
  	};

    $scope.options = {
      height: 200,
      //width: 500,
      lang:"pl-PL",
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
    };



// FILE /////////////////////////////////////////////////////

    $scope.showPreview = false;
    $scope.showJSONPreview = true;
    $scope.json_string = "";
    $scope.file = {};
    $scope.newfilename =""

    $scope.progress = 0;
       
    $scope.parseCsvToJson = function(csv){
    	var file = []
    	var header = csv.split('\n')[0].split(";");
    	if( header[header.length-1].indexOf("\r")>0){
    		header[header.length-1] = header[header.length-1].substring(0, header[header.length-1].length - 1);
    	}
    	var items = []

    	for(var i=1; i<csv.split('\n').length; i++ ){
    		var line = csv.split('\n')[i]
    		if(line ==""){
    			continue
    		}
    		var item = {}
    		
    		for(var n=0 ;n<header.length; n++ ){
    			item[header[n]] = line.split(";")[n]
    			if(item[header[n]].indexOf("\r")>0){
    				item[header[n]] = item[header[n]].substring(0, item[header[n]].length - 1);
    			}
    		} 
    		items.push(item)
    	}
    	return items
    }

        $scope.fileChanged = function(files){	
    	if(files.length>0){
    		$scope.excelFile = files[0];	
			$scope.newfilename = files[0].name.toString()
	        if(angular.equals($scope.excelFile.type.toString(),"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
	        	$scope.sheets = [];
	        	$scope.isProcessing = true;
	        	XLSXReaderService.readFile($scope.excelFile, $scope.showPreview, $scope.showJSONPreview).then(function(xlsxData) {
			        $scope.sheets = xlsxData.sheets;
			        var keys = [];
					for(var k in $scope.sheets) keys.push(k);
			        if(keys.length==0){
			        	flash.show("Niewłaściwy format pliku.", "error")
			        	return
			        }		        
			        $scope.file= $scope.sheets[keys[0]] 
			        $scope.isProcessing = false;
	        	});
	        }else{
	        	if(angular.equals($scope.excelFile.type.toString(),"application/vnd.ms-excel")){
	        		var reader = new FileReader();		
	        		reader.onload = function(e) {
					    $scope.$apply(function() {
					        $scope.csvFile = reader.result;
					        $scope.file = $scope.parseCsvToJson($scope.csvFile)
					    });
					};
					reader.readAsText($scope.excelFile);
	 	        }else{
	 	        	$scope.newfilename =""
	 	        	flash.show("Niewłaściwy typ pliku.", "error")
	 	        	return
		        }

	        }
    	}else{
    		flash.show("Wybierz plik.", "info")	
    	}	 
    }
//

	/** GET ITEM BY NAME **/
	$scope.getItemByName = function(src, name){ // zwraca element o określonej nazwie
	    for(var i = 0; i<src.length;i++ ){
	       	if(src[i].name == name){
	          return src[i]
	        }
	    }
	    return ""
	} 
	/** GET ITEM BY ID **/
	$scope.getItemById = function(src, id){ // zwraca element o określonym id
	    for(var i = 0; i<src.length;i++ ){
	      if(src[i].id == id){
	        return src[i]
	      }
	    }
	  } 

	/* CREATE NEW DEV || SIM */
	$scope.items = [];

	/**
	* Sprawdza czy w source znajdują się elementy z propertys, jak tak to 
	* znajduje objekt o tej samej nazwie i przypisuje go do zwracanej zminennej
	* propertys - objekty z polami po których przeszukujemy
	* source - tam szukamy kluczy z propertys
	*/
	$scope.addpropertiesObject = function(propertys, source){
		var item  = {}
	    angular.forEach(propertys, function(value, key) {
	      if(source[key]){
	        var temp = $scope.getItemByName(value, source[key])
	        if(temp){
	          item[key] = temp
	          item[key+"_id"] = temp.id
	        }
	      }  
	    }, '');
	    return item
	}
	/**
	* Sprawdza czy w source znajdują się elementy z propertys, jak tak to 
	* dodaje wartość pola z source do item
	* propertys - objekty z polami po których przeszukujemy
	* source - tam szukamy kluczy z propertys
	*/
	$scope.addpropertiesString = function(propertys, source){
	    var item  = {}
	    for(var i =0; i < propertys.length;i++){
	      if(source[propertys[i]]){
	        item[propertys[i]] = source[propertys[i]]
	      }
	    }
	    
	    return item
	}


	$scope.createnewitems = function(){
		$scope.progress = 0
		$scope.showprogress = false

		$scope.showNewRes = false;
		//$scope.items = [];
		while($scope.items.length > 0) {
		    $scope.items.pop();
		}
	    var forbothObj = {"state": $scope.devicestates, "company":$scope.company}
	    var fordevObj = {"type": $scope.devicetypes, "model":$scope.devicemodels}
	    var fordevStr = ["sn","imei","mac","description"]

	    var forSimStr = ["iccid","msisdn","imsi","pin","pin2","puk","puk2","description"]
        if(!$scope.file.length){
	    	return
	    }
	    if($scope.file[0].hasOwnProperty('msisdn') || $scope.file[0].hasOwnProperty('iccid') || $scope.file[0].hasOwnProperty('imsi')){
    		$scope.issim = true;
   		}else{
   			$scope.issim = false;
   		}
	    for(var i = 0; i < $scope.file.length;i++){
	      var item = {}
	      item = $scope.addpropertiesObject(forbothObj, $scope.file[i])
	      item.holder = $scope.getItemById($scope.clients, Session.user.LAB)
	      item.holder_id = Session.user.LAB
	      item.owner = $scope.getItemById($scope.clients, Session.user.LAB)
	      item.owner_id = Session.user.LAB
		  item.visible = true
	      if($scope.issim){
	        angular.extend(item, $scope.addpropertiesString(forSimStr, $scope.file[i]));
	      }else{
	        angular.extend(item, $scope.addpropertiesObject(fordevObj, $scope.file[i]));
	        angular.extend(item, $scope.addpropertiesString(fordevStr, $scope.file[i]));
	        if((item.hasOwnProperty('type') && item.hasOwnProperty('model') && item.hasOwnProperty('state') && item.hasOwnProperty('company')) && (item.hasOwnProperty('sn') || item.hasOwnProperty('imei') ||item.hasOwnProperty('mac')) ){
	        	item.ok = true
	        	item.$selected = true
	        }else{
	        	item.ok = false
	        	item.$selected = false
	        }
	      }
	      $scope.items.push(item)
	    }
	    $scope.showNewRes = true; 
	}  


	$scope.changeAllSelection = function(selectall){
		for(var i = 0; i < $scope.items.length;i++){
			if($scope.items[i].ok){
				$scope.items[i].$selected = angular.copy(!selectall)	
			}
			
		}
	}
 

	$scope.checkProperties = function(item){
		switch($scope.issim){
			case false:
				if((item.hasOwnProperty('type') && item.hasOwnProperty('model') && item.hasOwnProperty('state') && item.hasOwnProperty('company')) && ( item.hasOwnProperty('sn') || item.hasOwnProperty('imei') ||item.hasOwnProperty('mac')) ){
			        item.ok = true
			        item.$selected = true
				}else{
				    item.ok = false
				    item.$selected = false
				}
				break;
			case true:
				if(( item.hasOwnProperty('state') && item.hasOwnProperty('company')) && (item.hasOwnProperty('iccid') || item.hasOwnProperty('msisdn') ||item.hasOwnProperty('imsi')) ){
			        item.ok = true
			        item.$selected = true
				}else{
				    item.ok = false
				    item.$selected = false
				}
				break;
		}
	 	
	}

	$scope.editType = {}
	$scope.selectEditType= function(item){
		$scope.editType = item
		
	}

	$scope.editState = {}
	$scope.selectEditState= function(item){
		$scope.editState = item
	}


	$scope.displaytextNR={
		model: "",
		company: "",
		owner: "LAB LAB",
		holder: "LAB LAB"
	}

	/* edit model for new resources*/
		$scope.editModel = {}
		$scope.selectEditModel= function(item){
			$scope.editModel = item	
			if(item.model){
				$scope.displaytextNR.model = item.model.name
			}
		}

		$scope.$watch("displaytextNR.model", function(newVal, oldVal){
			if(newVal && (typeof newVal == "object")){
				$scope.displaytextNR.model = newVal.name;
	 			$scope.editModel.model = angular.copy(newVal);
	 			$scope.editModel.model_id = angular.copy($scope.editModel.model.id);
	 			$scope.checkProperties($scope.editModel)
		        $scope.editModel = {}
			}
		}, true);

	/* edit company for new resources*/
		$scope.editCompany = {}
		$scope.selectEditCompany= function(item){
			$scope.editCompany = item	
			if(item.company){
				$scope.displaytextNR.company = item.company.name
			}
		}

		$scope.$watch("displaytextNR.company", function(newVal, oldVal){
			if(newVal && (typeof newVal == "object")){
				$scope.displaytextNR.company = newVal.name;
	 			$scope.editCompany.company = angular.copy(newVal);
	 			$scope.editCompany.company_id = angular.copy($scope.editCompany.company.id);
	 			$scope.checkProperties($scope.editCompany)
		        $scope.editCompany = {}
			}
		}, true);

	/* edit owner for new resources*/
		$scope.editOwner = {}
		$scope.selectEditOwner= function(item){
			$scope.editOwner = item	
			if(item.owner){
				$scope.displaytextNR.owner = item.owner.firstname+' '+item.owner.lastname;
			}
		}

		$scope.$watch("displaytextNR.owner", function(newVal, oldVal){
			if(newVal && (typeof newVal == "object")){
				$scope.displaytextNR.owner = newVal.firstname+' '+newVal.lastname;
	 			$scope.editOwner.owner = angular.copy(newVal);
	 			$scope.editOwner.owner_id = angular.copy($scope.editOwner.owner.id);
	 			$scope.checkProperties($scope.editOwner)
		        $scope.editOwner = {}
			} 
		}, true);

	/* edit holder for new resources*/
		$scope.editHolder = {}
		$scope.selectEditHolder= function(item){
			$scope.editHolder = item	
			if(item.holder){
				$scope.displaytextNR.holder = item.holder.firstname+' '+item.holder.lastname;
			}
		}

		$scope.$watch("displaytextNR.holder", function(newVal, oldVal){
			if(newVal && (typeof newVal == "object")){
				$scope.displaytextNR.holder = newVal.firstname+' '+newVal.lastname;
	 			$scope.editHolder.holder = angular.copy(newVal);
	 			$scope.editHolder.owner_id = angular.copy($scope.editHolder.holder.id);
	 			
	 			$scope.checkProperties($scope.editHolder)
		        $scope.editHolder = {}
			} 
		}, true);

 
	$scope.progress = 0
	$scope.showprogress = false

	function update(numb) {
  		var n = 1
  		if(numb >0){
  			n =  numb
  		}		 
  		if($scope.progress!=100){
  			$scope.progress += 100/n
  		}
  	}

	$scope.addDevFromFile= function(){
		$scope.itemForEmail =[]
		$scope.showprogress = true
		var items = $filter('filter')($scope.items, {ok:true, $selected:true} )
		var numdev = items.length
 
		for(var i =0; i<items.length;i++){	

			$scope.addSingleDev(angular.copy(items[i]), numdev)
 			
			var index = $scope.items.indexOf(items[i])
			$scope.items[index].$selected = false
			$scope.items[index].visible = false

        	delete items[i].ok
			delete items[i].$selected
		}	 
	}


	$scope.addSingleDev= function(item,  numdev){
		if($scope.issim){
			var newData ={
				company_id : item.company.id,
				state_id :item.state.id,
				iccid : item.iccid,
				imsi : item.imsi,
				msisdn : item.msisdn,
				description : function(){if(item.description){ return item.description}else{return ""}}(),
				holder_id : item.holder.id,
				owner_id : item.owner.id,
				pin: item.pin,
				pin2: item.pin2,
				puk: item.puk,
				puk2: item.puk2,
				visible: item.visible
			}
			var table = "cbs_sim"	
		}else{
			var newData ={
				model_id : item.model.id,
				company_id : item.company.id,
				type_id : item.type.id,
				state_id : item.state.id,
				sn : item.sn,
				imei :item.imei,
				mac : item.mac,
				holder_id : item.holder.id,
				owner_id : item.owner.id,
				description : function(){if(item.description){ return item.description}else{return ""}}(),
				visible: item.visible
			}
			var table = "cbs_device"		
		}

		$http.post("addRecord.py", {'table': table, 'data':newData})
        	.success(function(data){
        		if(data){
		          id = parseInt(data.split("-")[0])
		          cbs_id = parseInt(data.split("-")[1])
		         }
		         
        		item.id = id;
        		item.cbs_id = cbs_id;
        		item.created_by = Session.user.id;
        		
				$scope.itemForEmail.push(item)
         		if($scope.issim){
        		 	if($scope.sim){
        		 		$scope.sim.push(item)
        		 	}
        		}else{
        		 	if($scope.devices){
        		 		$scope.devices.push(item)
        		 	}      		 	
        	 	}
        	 	update(numdev) 
        	 	if($scope.progress ==100){
        	 		flash.show("Dodano nowe urządenia.", "success")
        	 		var items = $filter('filter')($scope.items, {ok:true, $selected:true} )
        	 		if(!$scope.issim){
				        $scope.sendEmail($scope.itemForEmail, [])
				    }else{
				        $scope.sendEmail([], $scope.itemForEmail)
				    } 
				    $scope.itemForEmail=[]

				   
        	 	}

        	})
	        .error(function(err){
	    		flash.show("Wystąpił błąd podczas dodawania nowego zasobu!", "error")
	    	})		
	}


 
})

 /*
app.directive('dynamic', function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
});*/
