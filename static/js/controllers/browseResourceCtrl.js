app.controller("browseResourceCtrl", function($scope,$rootScope, $modal, flash, $q, $filter, $http, $timeout, Devices, Company, DevicesTypes, DevicesModels, DevicesStates, SIM,Clients, ngTableParams, Session) {
	$scope.flash = flash
	$scope.session = Session;
	$scope.pageLoading = false;
	$scope.selectedTab = "all";

	$scope.devices = Devices.getDevices()
	$scope.devicetypes = DevicesTypes.getDeviceTypes()
	$scope.devicemodels = DevicesModels.getDeviceModels()
	$scope.devicestates = DevicesStates.getDeviceState()
	$scope.company = Company.getCompany()
	$scope.sim = SIM.getSim()

	$scope.clients = Clients.getClients();

	if($scope.devices){
		for(var i =0;i<$scope.devices.length;i++ ){
			$scope.devices[i].$selected = false;
		}
	}


	if($scope.devices && $scope.devicetypes){
		for(var i = 0; i<$scope.devicetypes.length;i++ ){
			var clie = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.client_id)});
 			var lab = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.LAB) });
			$scope.devicetypes[i].len = clie.concat(lab).length;
		}
		var clie = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.client_id)});
	 	var lab = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.LAB) });
	 	$scope.alldev = clie.concat(lab).length;
	}

	if($scope.sim){
		var clie = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.client_id)});
 		var lab = $filter('filter')($scope.sim, { holder_id: parseInt($scope.session.user.LAB) });
 		$scope.allsim = clie.concat(lab).length;
	}

	/*GET CLIENTS ****************/
		if(!$scope.clients){
		 	$scope.clients = Clients.loadClientsData();
		}
		$scope.$on('clientsDataReady', function (event, data) {
			if($scope.devices && !$scope.devices[0].owner){
		      for(var i =0; i<$scope.devices.length;i++){
		        $scope.devices[i].holder = $scope.getItem(data,  $scope.devices[i].holder_id)
		        $scope.devices[i].owner = $scope.getItem(data,  $scope.devices[i].owner_id)
		      }
		    }
		    if($scope.sim && !$scope.sim[0].owner){
		      for(var i =0; i<$scope.sim.length;i++){
		        $scope.sim[i].holder = $scope.getItem(data,  $scope.sim[i].holder_id)
		        $scope.sim[i].owner = $scope.getItem(data,  $scope.sim[i].owner_id)
		      }
		    }
			$scope.clients = data;
			Clients.clients = data;
			$scope.changeLoading()
		});

	/*GET DEVICES ****************/
	    if(!$scope.devices){
		 	$scope.devices = Devices.loadDevicesData();
		}
	    $scope.$on('devicesDataReady', function (event, data) {
			if($scope.devicetypes || $scope.devicemodels || $scope.devicestates || $scope.company || $scope.clients){
				for(var i = 0; i<data.length;i++ ){
					if($scope.devicetypes){ data[i].type = $scope.getItem($scope.devicetypes, data[i].type_id) }
					if($scope.devicemodels){data[i].model = $scope.getItem($scope.devicemodels, data[i].model_id)}
					if($scope.devicestates){data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)}
					if($scope.company){data[i].company = $scope.getItem($scope.company, data[i].company_id)}
					if($scope.clients){data[i].holder = $scope.getItem($scope.clients, data[i].holder_id)}
					if($scope.clients){data[i].owner = $scope.getItem($scope.clients, data[i].owner_id)}
					if(!data[i].sn){data[i].sn=""}				// jak był null to chowałe je przy filtrowniu
					if(!data[i].imei){data[i].imei=""}
					if(!data[i].mac){data[i].mac=""}
				}
				/*if($scope.devicetypes){
					for(var i = 0; i<$scope.devicetypes.length;i++ ){
						var clie = $filter('filter')(data, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.client_id)});
		 				var lab = $filter('filter')(data, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.LAB) });
						$scope.devicetypes[i].len = clie.concat(lab).length;
					}
				}
				var clie = $filter('filter')(data, {holder_id: parseInt($scope.session.user.client_id)});
		 		var lab = $filter('filter')(data, {holder_id: parseInt($scope.session.user.LAB) });
		 		$scope.alldev = clie.concat(lab).length; */

			}
			//holderDevices
			$scope.devices = data ;
			//if($scope.devicetypes){
			//	$scope.countDev()
			//}

			Devices.devices = data;
			$scope.changeLoading()


		});

	/*GET DEVICES TYPES ****************/
	  	if(!$scope.devicetypes){
		 	$scope.devicetypes = DevicesTypes.loadDeviceTypesData();
		}
		$scope.$on('deviceTypesDataReady', function (event, data) {
			if($scope.devices && !$scope.devices[0].type){
		      for(var i =0; i<$scope.devices.length;i++){
		        $scope.devices[i].type = $scope.getItem(data,  $scope.devices[i].type_id)
		      }
		    }
		    /*if($scope.devices){
				for(var i = 0; i<data.length;i++ ){
					var clie = $filter('filter')($scope.devices, {type_id: data[i].id, holder_id: parseInt($scope.session.user.client_id)});
		 			var lab = $filter('filter')($scope.devices, {type_id: data[i].id, holder_id: parseInt($scope.session.user.LAB) });
					data[i].len = clie.concat(lab).length;
				}
			}*/

			$scope.devicetypes = data;
			if($scope.devices){
				$scope.countDev()
			}
			DevicesTypes.devicetypes = data;
			$scope.changeLoading()
		});

	/*GET DEVICES MODELS ****************/
	  	if(!$scope.devicemodels){
		 	$scope.devicemodels = DevicesModels.loadDeviceModelsData();
		}
		$scope.$on('deviceModelsDataReady', function (event, data) {
			if($scope.devices && !$scope.devices[0].model){
		      for(var i =0; i<$scope.devices.length;i++){
		        $scope.devices[i].model = $scope.getItem(data,  $scope.devices[i].model_id)
		      }
		    }
			$scope.devicemodels = data;
			DevicesModels.devicemodels = data;
			$scope.changeLoading()
		});

	/*GET DEVICES STATES ****************/
	  	if(!$scope.devicestates){
		 	$scope.devicestates = DevicesStates.loadDeviceStateData();
		}
		$scope.$on('deviceStatesDataReady', function (event, data) {
			if($scope.devices && !$scope.devices[0].state){
		      for(var i =0; i<$scope.devices.length;i++){
		        $scope.devices[i].state = $scope.getItem(data,  $scope.devices[i].state_id)
		      }
		    }
		    if($scope.sim && !$scope.sim[0].state){
		      for(var i =0; i<$scope.sim.length;i++){
		        $scope.sim[i].state = $scope.getItem(data,  $scope.sim[i].state_id)
		      }
		    }
			$scope.devicestates = data;
			DevicesStates.devicestates = data;
			$scope.changeLoading()
		});

	/*GET COMPANY ****************/
	  	if(!$scope.company){
		 	$scope.company = Company.loadCompanData();
		}
		$scope.$on('companyDataReady', function (event, data) {
			if($scope.devices && !$scope.devices[0].company){
		      for(var i =0; i<$scope.devices.length;i++){
		        $scope.devices[i].company = $scope.getItem(data,  $scope.devices[i].company_id)
		      }
		    }
		    if($scope.sim && !$scope.sim[0].company){
		      for(var i =0; i<$scope.sim.length;i++){
		        $scope.sim[i].company = $scope.getItem(data,  $scope.sim[i].company_id)
		      }
		    }
			$scope.company = data;
			Company.company = data;
			$scope.changeLoading()
		});

	/*GET SIM ****************/
		if(!$scope.sim){
		 	$scope.sim = SIM.loadSimData();
		}
		$scope.$on('simsDataReady', function (event, data) {
			if($scope.company || $scope.devicestates || $scope.clients){
				for(var i = 0; i<data.length;i++ ){
					if($scope.devicestates){ data[i].state = $scope.getItem($scope.devicestates, data[i].state_id)}
					if($scope.company){data[i].company = $scope.getItem($scope.company, data[i].company_id)}
					if($scope.clients){data[i].holder = $scope.getItem($scope.clients, data[i].holder_id)}
					if($scope.clients){data[i].owner = $scope.getItem($scope.clients, data[i].owner_id)}
					if(!data[i].pin){data[i].pin=""}
					if(!data[i].pin2){data[i].pin2=""}
					if(!data[i].puk){data[i].puk=""}
					if(!data[i].puk2){data[i].puk2=""}
					if(!data[i].imsi){data[i].imsi=""}
					if(!data[i].iccid){data[i].iccid=""}
					if(!data[i].msisdn){data[i].msisdn=""}
				}
			}
		 	$scope.allsim = data.length
			$scope.sim = data;
			SIM.sim = data;
			$scope.changeLoading()
		});
	//

	$scope.changeLoading = function(){	// sprawdź czy już można urkyć wizualizację ładowania
		if($scope.devicestates && $scope.devicetypes && $scope.devicemodels && $scope.clients && $scope.company && $scope.devices && $scope.sim){
			$scope.pageLoading = false;
			$scope.initTable()
		}else{
			$scope.pageLoading = true;
		}
	}


	$scope.getItem = function(src, id){ // zwraca element o określonym id
		for(var i = 0; i<src.length;i++ ){
			if(src[i].id == id){
				return src[i]
			}
		}
	}

	$scope.filter = {
    }

	$scope.selectedType = 0
	$scope.selectTyppe = function(type_id){
		$scope.selectedType = type_id;
		if($scope.devices){
			$scope.tableParamsRes.reload();
		}
	}

 	$scope.changeTab = function(tab){
		$scope.selectedTab = tab;
		if($scope.devices){
			$scope.tableParamsRes.reload();
		}
	}


	$scope.addedBy = function(userid){
		if( (typeof userid == "undefined") || (userid == null)){
			return ""
		}
		var users = $filter('filter')($scope.clients, {user_id: userid.toString()},true);
		if(users && (users.length>0)){
			return users[0].firstname+" "+users[0].lastname
		}else{
			return ""
		}
	}


	$scope.columns =   // widoczność kolumn w tabeli
			{cbs_id:true,
			 id: false,
			 type: false,
			 model: true,
			 company: true,
			 sn: true,
			 imei: true,
			 mac:true,
			 pin:false,
			 pin2:false,
			 puk:false,
			 puk2:false,
			 msisdn:true,
			 imsi:true,
			 iccid:true,
			 description:false,
			 holder:true,
			 owner:true,
			 state:true,
			 created:false,
			 created_by:false};

	$scope.countDev = function(){
		if($scope.devices && $scope.devicetypes && $scope.sim){
			switch($scope.selectedTab){
				case 'all':
					for(var i = 0; i<$scope.devicetypes.length;i++ ){
						var clie = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id});
 						$scope.devicetypes[i].len = clie.length;
					}
			 		$scope.alldev = $scope.devices.length;
			 		$scope.allsim = $scope.sim.length
					break;
				case 'holder':
					for(var i = 0; i<$scope.devicetypes.length;i++ ){
						var clie = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.client_id)}, true);
			 			var lab = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, holder_id: parseInt($scope.session.user.LAB) }, true);
						$scope.devicetypes[i].len = clie.concat(lab).length;
					}
					var clie = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.client_id)}, true);
			 		var lab = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.LAB) }, true);
			 		$scope.alldev = clie.concat(lab).length;

			 		var cliesim = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.client_id)}, true);
			 		var labsim = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.LAB) }, true);
			 		$scope.allsim = cliesim.concat(labsim).length
					break;
				case 'owner':
					for(var i = 0; i<$scope.devicetypes.length;i++ ){
						var clie = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, owner_id: parseInt($scope.session.user.client_id)}, true);
			 			/*var lab = $filter('filter')($scope.devices, {type_id: $scope.devicetypes[i].id, owner_id: parseInt($scope.session.user.LAB) });*/
						$scope.devicetypes[i].len = clie.length;//.concat(lab).
					}
					var clie = $filter('filter')($scope.devices, {owner_id: parseInt($scope.session.user.client_id)}, true);
			 		/*var lab = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.LAB) });*/
			 		$scope.alldev = clie.length ///.concat(lab).length;

			 		var cliesim = $filter('filter')($scope.sim, {owner_id: parseInt($scope.session.user.client_id)}, true);
		 			//var labsim = $filter('filter')($scope.sim, {owner_id: parseInt($scope.session.user.LAB) });
		 			$scope.allsim = cliesim.length //concat(labsim).length
					//console.log($scope.session.user.client_id)
					break;
			}
		}
	}

	var getData = function(){ // przełączanie danych tabeli
		if($scope.devicetypes){
			$scope.countDev()
		}
		switch($scope.selectedTab){
			case 'all':
				if($scope.selectedType ==0){
		 			return $scope.devices;
		 		}else if($scope.selectedType == -1){  //sim
		 			$scope.allsim = $scope.sim.length
		 			return $scope.sim
		 		}else{
		 			var clie = $filter('filter')($scope.devices, {type_id: $scope.selectedType}, true);
		 			return  clie;
		 		}
				break;
			case 'holder':
				if($scope.selectedType ==0){
	 				var clie = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.client_id)}, true);
	 				var lab = $filter('filter')($scope.devices, {holder_id: parseInt($scope.session.user.LAB) }, true);
		 			return clie.concat(lab);
		 		}else if($scope.selectedType == -1){  //sim
		 			var clie = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.client_id)}, true);
		 			var lab = $filter('filter')($scope.sim, {holder_id: parseInt($scope.session.user.LAB) }, true);
		 			return clie.concat(lab);
		 		}else{
		 			var clie = $filter('filter')($scope.devices, {type_id: $scope.selectedType, holder_id: parseInt($scope.session.user.client_id)}, true);
		 			var lab = $filter('filter')($scope.devices, {type_id: $scope.selectedType, holder_id: parseInt($scope.session.user.LAB) }, true);
		 			return  clie.concat(lab);
		 		}
		 		break;
			case 'owner':
				if($scope.selectedType ==0){
	 				var own = $filter('filter')($scope.devices, {owner_id: parseInt($scope.session.user.client_id)}, true);
	 				/*var lab = $filter('filter')($scope.devices, {owner_id: parseInt($scope.session.user.LAB) });*/
		 			return own//.concat(lab);
		 		}else if($scope.selectedType == -1){  //sim
		 			var own = $filter('filter')($scope.sim, {owner_id: parseInt($scope.session.user.client_id)}, true);
		 			/*var lab = $filter('filter')($scope.sim, {owner_id: parseInt($scope.session.user.LAB) });*/
		 			$scope.allsim = own.concat(lab).length
		 			return own.concat(lab);
		 		}else{
		 			var own = $filter('filter')($scope.devices, {type_id: $scope.selectedType, owner_id: parseInt($scope.session.user.client_id)}, true);
		 			/*var lab = $filter('filter')($scope.devices, {type_id: $scope.selectedType, owner_id: parseInt($scope.session.user.LAB) });*/
		 			return  own//.concat(lab);
		 		}
		 		break;
		}
    }

    $scope.initTable = function(){
	    $scope.tableParamsRes = new ngTableParams({
	        page: 1,            // show first page
	        count: 50          // count per page
	    }, {
	        total: getData().length,//function () { return getData().length; }, // length of data
	        getData: function($defer, params) {
	           // var filteredData = getData();
	            var filteredData = $filter('filter')( getData(), $scope.filter);
	            var orderedData = params.sorting() ?
	                                $filter('orderBy')(filteredData, params.orderBy()) :
	                                filteredData;
	            params.total(orderedData.length);
	            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
	        }//,
	        //$scope: { $data: {} }
	    });
	    $scope.tableParamsRes.settings().$scope = $scope;
	}

	function updateTable(){
  		$scope.tableParamsRes.reload()
  	}


  	$scope.$on('reloadBrowseResTable', function (event, data) {
  		$scope.tableParamsRes.reload()
  	})



	if($scope.devices && $scope.clients && $scope.sim){ // gdy dane nie wymagają ładowania to od razu wyświetl tbelę
  		$scope.initTable()
	}

	$scope.$watch("filter", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania
 		if($scope.devices && $scope.sim ){
 			$timeout(updateTable, 1);
			//$scope.tableParamsRes.reload();
 		}
	}, true);
/*

	$scope.$watch("devices", function(newVal, oldVal){ // odświeża tabelę podczas filtrowania

 		if($scope.tableParamsRes){
 			$scope.tableParamsRes.reload()
 		}
	}, true);
 */
	$scope.changeLoading()
})
